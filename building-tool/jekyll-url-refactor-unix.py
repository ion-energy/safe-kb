from os import listdir
from os import walk
from os.path import isfile, join
from os import path
from os import makedirs
from os import remove
import os
import sys
import re
import fileinput
import shutil
import zipfile

rootfolderpath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sourcepath = os.path.join(rootfolderpath,"_site")
prodpath = os.path.join(rootfolderpath,"_site_production")
archivepath = os.path.join(rootfolderpath,"_site_production_archive.zip")

#print(rootfolderpath)
#print(sourcepath)

allfiles = []
alldirs = []
htmlfiles =[]

rootfolder = "_site_production"
asseturl = "/doc/ion_knowledge_base/_site_production"
baseurl = "/core/doc"
count = 0

def createProdFolder(folderpath):
    if os.path.isdir(prodpath):
        shutil.rmtree(prodpath)
        print("Old production folder deleted \n")
    if not os.path.exists(prodpath):
        print("New production folder created at {} \n".format(prodpath))
        makedirs(prodpath)

def copytree(src, dst, symlinks=False, ignore=None):
    for item in listdir(src):
        #print(item)
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

def get_allfile(rootpath):
    for dirpath, dirnames, filenames in walk(rootpath):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            allfiles.append(filepath)
        for dirname in dirnames:
            get_allfile(os.path.join(dirpath, dirname))
        break

def isolate_ext(filelist, extension):
    for files in filelist:
        if os.name is "nt":
            pathlist = files.split("/")
        else:
            pathlist = files.split("/")

        if ("assets" in pathlist and files.endswith(extension)) or not files.endswith(extension) :
            continue              
        else:
            #print(files)
            htmlfiles.append(files)

def inplace_change(filename,old_string):
    global count
    with open(filename, encoding='utf8') as f:
        s = f.read()
        if old_string not in s:
            return

    with open(filename,'w', encoding='utf8') as f:
        stringiteration = re.finditer(old_string,s)
        span=7
        offset = 0

        for i in stringiteration:
            whileCount = 0
            while True :
                if s[i.end()+offset+whileCount] == '"':
                    break
                else:
                    whileCount = whileCount + 1
            fullUrl = s[i.end()+offset:i.end()+whileCount+offset]
            fullUrlList = fullUrl.split("/")
            prependedS=''            
            #print(fullUrl)
            if fullUrl:
                if fullUrlList[0] != "https:" and fullUrlList[0] != "http:" and fullUrl[0] != "#" and fullUrl != "javascript:void(0)":
                    if "assets" in fullUrlList or "dist" in fullUrlList:
                        prependedS = asseturl 
                    else:
                        prependedS = baseurl
                    
                    if fullUrl[0] != "/":
                        prependedS += rootpath
            
            s = s[:i.end() + offset] + prependedS + s[i.end() + offset:]
            offset += len(prependedS)
            count+=1
            #print("modified: {} path is: {} ".format(count,filename))
               
        f.write(s)

createProdFolder(prodpath)
copytree(sourcepath,prodpath)
get_allfile(prodpath)
isolate_ext(allfiles,".html")
isolate_ext(allfiles,".js")

for files in htmlfiles:
    if os.name is "nt":
        pathlist = files.split("/")
    else:
        pathlist = files.split("/")
    rootpath = ""
    rootfound = 0
    
    for index,i in zip(range(len(pathlist)-1),pathlist):
        
        if rootfound == 1:
            rootpath += i + "/"
        if i == rootfolder:
            rootfound = 1      

    rootpath = "/" +rootpath[:-1]+"/"

    inplace_change(files,"src=\"")
    inplace_change(files,"href=\"")
    inplace_change(files,"url: \"")

print("Creating archive... \n")
if os.path.exists(archivepath):
    remove(archivepath)
print(prodpath)
shutil.make_archive("_site_production_archive",'zip', prodpath)
print("All done ! \n")
