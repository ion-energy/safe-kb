const Papa = require('papaparse')
const fs = require('fs')
const basePath = './csvTables/'
const path = require('path')
const fse = require('fs-extra')

const filePathes = fs.readdirSync('./csvTables/')

fse.emptyDirSync('./jsonTables')

filePathes.forEach(filePath => {
    const csvString = fs.readFileSync(path.resolve('./csvTables/' + filePath), "utf-8")
    const json = Papa.parse(csvString, { header: true })
    const jsonString = JSON.stringify(json.data)

    const fileNameArray = filePath.split('.')
    const fileName = fileNameArray[0]

    fs.writeFileSync(path.resolve('../static/tables/json/' + fileName + '.json'), jsonString, 'utf-8')

})




