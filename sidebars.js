module.exports = {
  ctSidebar: [
    'ct/overview',
    {
      type: "category",
      label: 'Mechanical',
      items: [
        'ct/mechanical/connector-pin-description',
        'ct/mechanical/mounting-guidelines',
        'ct/mechanical/typical-wire-harness',],
      collapsed: false
    }
  ],
  fsbBSidebar: [
    'fsb-b/fsb-b-an'
  ],
  guidesSidebar: [
    'guides/getting-started',
    'guides/help',
    'guides/onboarding',
    'guides/welcome',

  ],
  ltSidebar: [
    'lt/overview',
    {
      type: "category",
      label: 'FS-LT',
      items: [
        'lt/fs-lt/product-overview',
        'lt/fs-lt/mounting-guidelines',
        'lt/fs-lt/connectors-and-pins',
        'lt/fs-lt/lt-harness-guide',
        'lt/fs-lt/specifications',],
      collapsed: false
    },
    {
      type: "category",
      label: 'FSB-PR-I',
      items: [
        'lt/fsb-pr-i/product-overview',
        'lt/fsb-pr-i/mounting-guidelines',
        'lt/fsb-pr-i/connectors-and-pins',
        'lt/fsb-pr-i/specifications',],
      collapsed: false
    },
    {
      type: "category",
      label: 'Integration guide',
      items: [
        'lt/integration-guide/overview',
        {
          type: "category",
          label: 'FS-LT with FSB-B',
          items: [
            'lt/integration-guide/fs-lt-with-fsb-b/integration-bom',
            'lt/integration-guide/fs-lt-with-fsb-b/wiring-guide',],
          collapsed: true
        },
        {
          type: "category",
          label: 'FS-LT with FSB-PR-I',
          items: [
            'lt/integration-guide/fs-lt-with-fsb-pr-i/integration-bom',
            'lt/integration-guide/fs-lt-with-fsb-pr-i/wiring-guide'
          ],
          collapsed: true
        },
      ],
      collapsed: false
    },
    {
      type: "category",
      label: 'Mechanical',
      items: [
        'lt/mechanical/connector-pin-description',
        'lt/mechanical/typical-wire-harness',],
      collapsed: false
    },
  ],
  xtSidebar: [
    'xt/overview',
    {
      type: "category",
      label: 'BMU',
      items: [
        {
          type: "category",
          label: 'XT-BMU-V1',
          items: [
            'xt/bmu/bmu-v-1/connectors-and-pins'
          ],
          collapsed: true
        },
        {
          type: "category",
          label: 'XT-BMU-OTS',
          items: [
            'xt/bmu/xt-bmu-ots/bmu-harness-guide',
            'xt/bmu/xt-bmu-ots/connectors-and-pins',
            'xt/bmu/xt-bmu-ots/interfaces',
            'xt/bmu/xt-bmu-ots/mounting-guidelines',
            'xt/bmu/xt-bmu-ots/product-overview',
            'xt/bmu/xt-bmu-ots/specifications',
          ],
          collapsed: true
        },
      ],
      collapsed: false
    },
    {
      type: "category",
      label: 'Integration guide',
      items: [
        'xt/integration-guide/overview',
        'xt/integration-guide/integration-bom',
        'xt/integration-guide/wiring-guide'],
      collapsed: false
    },
    {
      type: "category",
      label: 'SMU',
      items: [
        {
          type: "category",
          label: 'XT-SMU-F',
          items: [
            'xt/smu/xt-smu-f/product-overview',
            'xt/smu/xt-smu-f/mounting-guidelines',
            'xt/smu/xt-smu-f/connectors-and-pins',
            'xt/smu/xt-smu-f/specifications',],
          collapsed: false
        }
      ],
      collapsed: false
    },
  ],
};


