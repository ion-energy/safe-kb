
import React, { useState, useEffect } from 'react'
import { Helmet } from "react-helmet"
import ExecutionEnvironment from '@docusaurus/ExecutionEnvironment';

const characteristicsTable = require('/static/tables/json/characteristics-table.json')
const featuresTable = require('/static/tables/json/features-table.json')
const ConnectorsTable = require('/static/tables/json/connectors-table.json')
const pinsTable = require('/static/tables/json/pins-table.json')
const blocksTable = require('/static/tables/json/blocks-table.json')



const csvTable = (props) => {

    const tableType = props.tableType
    const tableFilter = props.tableFilter
    const tableDesign = props.tableDesign
    const tableCategory = props.tableCategory
    let processedData

    function GetData(tableType) {
        let data
        switch (tableType) {
            case "characteristics":
                data = JSON.parse(JSON.stringify(characteristicsTable))
                break;
            case "features":
                data = featuresTable
                break;
            case "features-overview":
                data = featuresTable
                break;
            case "connectors-designation":
                data = ConnectorsTable
                break;
            case 'connectors-implementation':
                data = ConnectorsTable
                break;
            case 'connectors-condensed':
                data = ConnectorsTable
                break;
            case "pins":
                data = pinsTable
                break;
            case "blocks":
                data = blocksTable
                break;
            default:
                break;
        }

        return data;
    }

    function processData(tableFilter, tableDesign, tableCategory, tableType) {


        const tableData = GetData(tableType)

        const characteristicsCol = ["Symbol", "Parameter", "Conditions", "Min.", "Typ.", "Max.", "Unit", "Variant"]
        const featureCol = ["Name", "Description", "Value", "Variant"]
        const featureOverviewCol = ["Category", "Name", "Description", "Value", "Variant"]
        const connectorDesignationCol = ["Des.", "Name", "Description", "Pins", "Variant"]
        const connectorImplementationCol = ["Des.", "Name", "MPN", "Mating MPN", "Crimp MPN", "AWG", "Variant"]
        const connectorCondensedCol = ["Pins", "MPN", "Mating MPN", "Crimp MPN", "AWG", "Description"]
        const pinCol = ["#", "Label", "Type", "Description"]
        const blockCol = ["Name", "Type", "Label", "Description", "Parent"]

        if (!tableCategory) {
            tableCategory = "none"
        }
        if (!tableDesign) {
            tableDesign = "none"
        }
        if (!tableFilter) {
            tableFilter = "none"
        }

        let colChecker = characteristicsCol

        switch (tableType) {
            case 'characteristics':
                colChecker = characteristicsCol
                break;
            case 'features':
                colChecker = featureCol
                break;
            case 'features-overview':
                colChecker = featureOverviewCol
                break;
            case 'connectors-designation':
                colChecker = connectorDesignationCol
                break;
            case 'connectors-implementation':
                colChecker = connectorImplementationCol
                break;
            case 'connectors-condensed':
                colChecker = connectorCondensedCol
                break;
            case 'pins':
                colChecker = pinCol
                break;
            case 'blocks':
                colChecker = blockCol
                break;
            default:
        }
        let filteredData = JSON.parse(JSON.stringify(tableData))

        if (tableDesign != 'none') {

            filteredData = filteredData.filter(value => value.Design.includes(tableDesign))
        }

        if (tableCategory != 'none') {


            filteredData = filteredData.filter(value => value.Category.includes(tableCategory)

            )
        }

        if (tableFilter != 'none') {
            let tableFilterArray = tableFilter.split(',')
            const filteredHeader = tableFilterArray[1]
            const filteredValue = tableFilterArray[2]

            if (tableFilterArray.includes('not')) {
                filteredData = filteredData.filter(value => !value[filteredHeader].includes(filteredValue))
            }
            else {
                const filteredHeader = tableFilterArray[0]
                const filteredValue = tableFilterArray[1]
                filteredData = filteredData.filter(value => value[filteredHeader].includes(filteredValue))
            }
        }
        for (const data of filteredData) {

            for (const [key, value] of Object.entries(data)) {
                if (!colChecker.includes(key)) {
                    delete data[key]
                }
                else {
                    if (value == '') {
                        data[key] = 'N/A'
                    }
                }
            }
        }
        return filteredData
    }

    const data = processData(tableFilter, tableDesign, tableCategory, tableType)
    if (data) {
        processedData = data
    }

    if (processedData.length > 0) {
        return (


            <div className={props.tableType.toLowerCase()}>
                <table>
                    <thead>
                        <tr key='tr' >
                            {
                                Object.keys(processedData[0]).map((key, i) => {
                                    const className = 'col-' + key.toLowerCase()
                                    return <th className={className} key={key + i} >{key}</th>
                                })
                            }
                        </tr>
                    </thead>
                    <tbody>

                        {processedData.map((element, i) => {
                            return (
                                <tr key={element + i} >
                                    {Object.entries(element).map((entry, i) => {
                                        return <td key={entry[1] + i}> {entry[1]} </td>
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <Helmet>
                    <script>MathJax.typeset()</script>
                </Helmet>
            </div>
        )
    }
    else {
        console.log('noData')
        return (

            <p>no data</p>
        )
    }
}



export default csvTable


