const math = require('remark-math')
const katex = require('rehype-katex')
const path = require('path')
require('dotenv').config()
console.log(process.env.BASEURL)
const fetch = require("node-fetch");
module.exports = {
  clientModules: [require.resolve('./mathjax-updater/updater.ts')],
  scripts: [
    {
      src: process.env.BASEURL + 'js/mathjax.conf.js'
    },
    {
      src: "https://code.jquery.com/jquery-3.5.1.min.js",
    },
    {
      src: "https://polyfill.io/v3/polyfill.min.js?features=es6",
    },
    {
      id: "MathJax-script",
      src: "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js",
    },
    {
      src: process.env.BASEURL + 'js/mathjax-helper.js'
    },
  ],
  stylesheets: [
    'https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.css',
  ],
  title: 'Edison Knowledge base',
  tagline: 'Official documentation',
  url: 'https://ion-energy.github.io',
  baseUrl: "/",
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'ion-energy', // Usually your GitHub org/user name.
  projectName: 'edison-kb', // Usually your repo name.

  themeConfig: {
    algolia: {
      appId: 'YORKKOZH8C',
      apiKey: '7fcc95af60a5e967917989e624690056',
      indexName: 'ionkbv2',
      contextualSearch: false,
    },
    navbar: {
      title: 'ION knowledge base',
      logo: {

        alt: 'Ion energy logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/ct/overview',
          label: 'CT',
          position: 'left'
        },
        {
          to: 'docs/lt/overview',
          label: 'LT',
          position: 'left'
        },
        {
          to: 'docs/xt/overview',
          label: 'XT',
          position: 'left'
        },
        {
          to: 'docs/guides/getting-started',
          label: 'Guides',
          position: 'left'
        },

        {
          href: 'https://github.com/facebook/docusaurus',
          label: 'Bitbucket',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',

      copyright: `Copyright © ${new Date().getFullYear()} ION Knowledge base, Ion Energy Inc. Built with Docusaurus.`,
    },
  },

  presets: [
    [
      '@docusaurus/preset-classic',
      {
        sitemap: {},
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://bitbucket.org/freemens/ionkb-v2/src/master/',
          remarkPlugins: [math],
          rehypePlugins: [[katex, { strict: false }]],
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
