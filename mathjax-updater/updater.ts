import ExecutionEnvironment from '@docusaurus/ExecutionEnvironment';

export function onRouteUpdate({ location }: { location: Location }) {

  $(document).ready(function () {
    console.log("executed onRouteUpdate")
    console.log(MathJax.typeset())
  })


}

if (ExecutionEnvironment.canUseDOM) {
  $(document).ready(function () {
    console.log("executed in client module example")
    console.log(MathJax.typeset())
  })
}