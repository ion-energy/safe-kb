
MathJax = {
    startup: {
        ready() {
            var CHTMLmath = MathJax._.output.chtml.Wrappers.math.CHTMLmath;
            CHTMLmath.styles['mjx-container[jax="CHTML"][display="true"]'].margin = '0';
            MathJax.startup.defaultReady();
        }
    },
    tex: {
        inlineMath: [['$', '$'], ['\\(', '\\)']]
    },
    svg: {
        fontCache: 'global'
    },
};

console.log('mathJaxConf loaded')
