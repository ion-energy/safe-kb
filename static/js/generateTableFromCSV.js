var url = "";
var characteristicsTable
var featuresTable
var connectorsTable
var pinTable
var blockTable

console.log("generateTableFromCSV Loaded")

document.addEventListener("DOMContentLoaded", generateTables)

function generateTables() {
    $.ajax({
        type: "GET",
        url: "../tables/blocks-table.csv",
        dataType: "text",
        success: function (response) {
            console.log(response)
            fileData = $.csv.toArrays(response);
            fileData.forEach(function (row, index) {
                blockTable = fileData
            })
        },
        async: false
    })

    $.ajax({
        type: "GET",
        url: "../tables/pins-table.csv",
        dataType: "text",
        success: function (response) {
            fileData = $.csv.toArrays(response);
            fileData.forEach(function (row, index) {
                if (index == 0) {
                    designCol = row.indexOf('Design')
                }
                else {
                    // Changes string format of design to Array and remove any spaces. "FS-LT-10S ,FS-LT-25S" ---> ["FS-LT-10S","FS-LT-25S"]
                    row[designCol] = row[designCol].replace(/ /g, '')
                    row[designCol] = row[designCol].split(',')

                }
                pinTable = fileData
            })
        },
        async: false
    })

    $.ajax({
        type: "GET",
        url: "../tables/connectors-table.csv",
        dataType: "text",
        success: function (response) {
            fileData = $.csv.toArrays(response);
            fileData.forEach(function (row, index) {
                if (index == 0) {
                    designCol = row.indexOf('Design')
                }
                else {
                    // Changes string format of design to Array and remove any spaces. "FS-LT-10S ,FS-LT-25S" ---> ["FS-LT-10S","FS-LT-25S"]
                    row[designCol] = row[designCol].replace(" ", "")
                }
                connectorsTable = fileData
            })
        },
        async: false
    })

    $.ajax({
        type: "GET",
        url: "../tables/characteristics-table.csv",
        dataType: "text",
        success: function (response) {
            fileData = $.csv.toArrays(response);
            fileData.forEach(function (row, index) {
                if (index == 0) {
                    designCol = row.indexOf('Design')
                }
                else {
                    // Changes string format of design to Array and remove any spaces. "FS-LT-10S ,FS-LT-25S" ---> ["FS-LT-10S","FS-LT-25S"]
                    row[designCol] = row[designCol].replace(" ", "")
                }
                characteristicsTable = fileData
            })
        },
        async: false
    })

    $.ajax({
        type: "GET",
        url: "../tables/features-table.csv",
        dataType: "text",
        success: function (response) {
            fileData = $.csv.toArrays(response);
            fileData.forEach(function (row, index) {
                if (index == 0) {
                    designCol = row.indexOf('Design')
                }
                else {
                    // Changes string format of design to Array and remove any spaces. "FS-LT-10S ,FS-LT-25S" ---> ["FS-LT-10S","FS-LT-25S"]
                    row[designCol] = row[designCol].replace(" ", "")
                }
                featuresTable = fileData
            })
        },
        async: false
    })

    console.log($('div[tabletype]'))
    $('div[tabletype]').each(function (index, table) {
        console.log(table)
        var data

        var filterString = $(table).attr("tablefilter")
        var type = $(table).attr("tabletype")
        var design = $(table).attr("tabledesign")
        var debug = $(table).attr("isdebug")
        var category = $(table).attr("tablecategory")
        if (debug == null) {
            debug = 0
        }
        if (category == null) {
            category = "none"
        }
        if (filterString == null) {
            filter = []

        }
        else {

            var filter = filterString.split(",")
        }
        if (design == null) {
            design = ""
        }

        switch (type) {
            case "characteristics":
                data = characteristicsTable
                break;
            case "features":
                data = featuresTable
                break;
            case "features-overview":
                data = featuresTable
                break;
            case "connectors-designation":
                data = connectorsTable
                break;
            case 'connectors-implementation':
                data = connectorsTable
                break;
            case 'connectors-condensed':
                data = connectorsTable
                break;
            case "pins":
                data = pinTable
                break;
            case "blocks":
                data = blockTable
                break;
            default:
                break;
        }


        generateHtmlTable(data, $(table), category, type, design, debug, filter)
    });

}
function generateHtmlTable(data, element, category, type, design, debug, filter) {

    const characteristicsCol = ["Symbol", "Parameter", "Conditions", "Min.", "Typ.", "Max.", "Unit", "Variant"]
    const featureCol = ["Name", "Description", "Value", "Variant"]
    const featureOverviewCol = ["Category", "Name", "Description", "Value", "Variant"]
    const connectorDesignationCol = ["Des.", "Name", "Description", "Pins", "Variant"]
    const connectorImplementationCol = ["Des.", "Name", "MPN", "Mating MPN", "Crimp MPN", "AWG", "Variant"]
    const connectorCondensedCol = ["Pins", "MPN", "Mating MPN", "Crimp MPN", "AWG", "Description"]
    const pinCol = ["#", "Label", "Type", "Description"]
    const blockCol = ["Name", "Type", "Label", "Description", "Parent"]


    var designString = design.replace(/ /g, "")
    var designArray = designString.split(",")

    var html = '<table className="' + type + ' table table-hover table-striped table-sm">';
    var colChecker
    var colToGenerateIndex = []
    var filterCol
    var filterValue
    var filterColIndex
    var filterValue
    var filterIsSet
    var hasNewEntry = true
    var designFound

    switch (type) {
        case 'characteristics':
            colChecker = characteristicsCol
            break;
        case 'features':
            colChecker = featureCol
            break;
        case 'features-overview':
            colChecker = featureOverviewCol
            break;
        case 'connectors-designation':
            colChecker = connectorDesignationCol
            break;
        case 'connectors-implementation':
            colChecker = connectorImplementationCol
            break;
        case 'connectors-condensed':
            colChecker = connectorCondensedCol
            break;
        case 'pins':
            colChecker = pinCol
            break;
        case 'blocks':
            colChecker = blockCol
            break;
        default:
    }

    if (typeof (data[0]) === 'undefined' && typeof (colChecker) === 'undefined') {
        return null;
    } else {

        // Check if filter is set. 
        if (filter.length > 0) {
            // Assign column and value filter and set the filter flag to true

            if (filter[0] == "not") {
                negativeFilter = true
                filterCol = filter[1]
                filterValue = filter[2]
                filterIsSet = true
            }
            else {
                negativeFilter = false
                filterCol = filter[0]
                filterValue = filter[1]
                filterIsSet = true

            }
        }
        else {
            filterIsSet = false
            negativeFilter = false
        }


        $.each(data, function (index, row) {




            //bind header
            if (index == 0) {
                html += '<thead className="thead-light">';
                html += '<tr>';
                designIndex = row.indexOf('Design')
                variantIndex = row.indexOf('Variant')

                // Register column index of the filter
                filterColIndex = row.indexOf(filterCol)
                $.each(row, function (index, colData) {

                    if (colChecker.includes(row[index])) {
                        colToGenerateIndex.push(index)
                        html += '<th className="fit">';
                        html += colData;
                        html += '</th>';

                    }
                    else {
                    }
                });
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
            } else {
                html += '<tr>';
                $.each(row, function (index, colData) {

                    categoryIndex = row.indexOf(category)

                    try {
                        if (row[designIndex].includes(design)) {
                            designFound = true
                        }
                        else {
                            designFound = false
                        }
                    }
                    catch {
                        designFound = false
                    }

                    if (colToGenerateIndex.includes(index) && (row[categoryIndex] == category || category === "none") && (designFound || design === "")) {


                        if (row[filterColIndex] == filterValue && filterIsSet) {
                            if (!negativeFilter) {
                                if (index == variantIndex) {
                                    html += '<td className="fit">';
                                    if (row[variantIndex].includes(designString)) {

                                        variantArray = row[variantIndex].split(',')
                                        for (const variant of variantArray) {
                                            if (variant.includes(designString)) {
                                                var displayVariant = variant.replace(designString + '-', "")
                                                html += '<div class = tooltip>' + displayVariant + ' <span className="tooltiptext">' + variant + '</span></div> ';
                                            }
                                            else {

                                            }
                                        }


                                    }
                                    else if (row[variantIndex] == "") {
                                        // html += '<td className="fit"></td>';
                                    }
                                    html += '</td>';
                                }

                                else {
                                    html += '<td className="fit">';
                                    html += colData;
                                    html += '</td>';
                                    hasNewEntry = true
                                }
                            }

                        }
                        else if (!filterIsSet || (filterIsSet && negativeFilter)) {

                            if (index == variantIndex) {

                                html += '<td className="fit">';
                                if (row[variantIndex].includes(designString)) {
                                    variantArray = row[variantIndex].split(',')
                                    for (const variant of variantArray) {
                                        if (variant.includes(designString)) {
                                            var displayVariant = variant.replace(designString + '-', "")
                                            html += '<div class = tooltip>' + displayVariant + ' <span className="tooltiptext">' + variant + '</span></div> ';
                                        }
                                        else {

                                        }
                                    }


                                }
                                else if (row[variantIndex] == "") {
                                    // html += '<td className="fit"></td>';
                                }
                                html += '</td>';
                            }
                            else {

                                html += '<td className="fit">';
                                html += colData;
                                html += '</td>';
                                hasNewEntry = true
                            }

                        }
                        else {
                            hasNewEntry = false
                        }

                    }
                    else {
                        hasNewEntry = false
                    }
                });
                console.log("ITERATION ")
                html += '</tr>';



            }
        });
        html += '</tbody>';
        html += '</table>';
        if (parseInt(debug)) (
            designArray.forEach(function (design, index) {
                element.append('<span className="badge badge-pill badge-success">' + design + '</span> ')
            })

        )
        if (parseInt(debug)) {
            element.append('<span className="badge badge-pill badge-danger">' + category + '</span> ')
        }
        element.append(html);
        MathJax.typeset()
    }


    //Cleanup empty TR tags generated when value did not match filter
    $('tr').each(function () {
        if ($(this).text() === '') {
            $(this).remove();
        }
    });


}	