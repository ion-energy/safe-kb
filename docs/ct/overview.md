---
layout: docs
group: sales
title: FS-CT Overview
description: An overview of FS-CT specifications & features.
release_prediction: Q2
---

![ct-overview](./assets/overview/fs-ct-front.png)

FS-CT is a single board Battery Management System designed for low Voltage battery systems. It comes with 5S and 10S variants suitable for two wheeler automotive and small storage applications.

## Application

- Light electric and hybrid vehicles, motorcycles, scooters, 3 wheelers.
- Small Industrial and home storage.
  
## Features

- Configurable single board compact architecture.
- Support multiple battery chemistries.
- State of Charge (SOC) and State of Health (SOH) estimations based on advanced algorithms.
- Watchdogs and self-diagnostics safety systems.
- Isolated CAN bus interface for charger control and system interfacing.
- Each board manages from 3 to 10 cells in series.
- Integrated PDU with 40A continuous current and over-current protection.
- Detection and protection for over voltage and under-voltage errors.
- Real-time monitoring and logging.
- 500µA supply current in power saving mode.
- Embedded passive balancing up to 150mA per cell.
- 1 onboard temperature sensors and 2 thermistors (NTC) inputs for external sensing

## General specifications

| Parameters                     | Description                                    |
| ------------------------------ | ---------------------------------------------- |
| Battery voltage                | 7.5 VDC – 42 VDC                               |
| Cell configuration (per board) | 3-5 cells (FS-CT-5S)                           |
|                                | 6-10 cells (FS-CT-10S)                         |
| Capacity                       | 40Ah max recommended                           |
| Balancing current per cell     | 150 mA @ 4.2V                                  |
| Max cell voltage               | 4.2V                                           |
| Current consumption on battery | 15mA typ in normal mode                        |
|                                | 500µA typ in sleep mode                        |
| Temperature sensors            | 1 on the board + 2 external sensors (10kΩ NTC) |
| Communication                  | CAN bus for system integration                 |
|                                | 4 Led for SOC                                  |
| Temperature                    | -40°C to 85°C                                  |
| Dimensions                     | 75 x 60 x 15 mm                                |
| Weight                         | 35 g                                           |

## Typical Application

<img src="./assets/overview/fs-ct-application-diagram.png" width="100%"></img>