---
layout: docs

group: FS-CT
title: Typical FS-CT Wiring Harness
description: Instructions to interface electrically FS-CT with a battery pack.
---

The Wire Harness connects the Battery Management System (BMS) FS-CT to various applications for batteries with less than or equal to a 10S combination for each board.

## Items in the kit

The items in the kit are split into four as below:

Table 1: Items in the Kit.

|         Harness Name          |  Qty  |
| :---------------------------: | :---: |
|     Cell Voltage Sensing      |   1   |
| Ignition, CAN bus and SoC Display |   1   |
|        NTC Connections        |   1   |
|          Custom GPIO          |   1   |

[COMMENT]: #  (Add images of the kit once available.)

## Installation

To understand the installation, the wiring diagram must be studied. The steps to assemble the harness are referenced from the wiring diagram.

### Wiring diagram

The below wiring diagram represents the interconnection between the BMS board and the rest of the battery pack.

![wire-harness](./assets/wire-harness/ct-harness.png)

Wiring Diagram for FS-CT BMS Board

:::info
This image is for representative purposes only. Please consult ION before finalising the wiring harness for your application.
:::

Table 2: Wiring Diagram Reference

|               Item Name               | Reference |  Qty  |
| :-----------------------------------: | :-------: | :---: |
|     Cell Voltage Sensing harness      |    H1     |   1   |
|        NTC Connections harness        |    H2     |   1   |
| Ignition, CAN bus and SoC Display harness |    H3     |   1   |
|          Custom GPIO harness          |    H4     |   1   |
|  Ignition, CAN bus and Display connector  |    J1     |   1   |
|           Cell connections            |    J2     |   1   |
|      Firmware FLashing connector      |    J3     |   1   |
|            GPIO connector             |    J4     |   1   |
|            NTC connections            |    J5     |   1   |
|         Battery + connection          |    J6     |   1   |
|           Pack - connection           |    J7     |   1   |
|         Battery - connection          |    J8     |   1   |

:::info
Since the cell voltage sensing harness (H1) will power up the board, we advise that it be connected as the last step in your setup to prevent unwanted BMS errors.
:::

### Assembly instructions

1. Connect the NTC harness **H2** to FS-CT BMS Connector **J5** and the lug termination to be connected to the intended location on the cell modules.
2. Connect the GPIO harness **H4** to FS-CT BMS connector **J4** and the other free end of the cable to the application.
3. Connect the Ignition, CAN bus and SoC harness **H3** to FS-CT BMS connector **J1** and the other free end of the cable to the application.
4. Connect the Cell Voltage Sensing harness **H1** to FS-CT BMS Connector **J2** and the lug termination to be connected to the intended location on the cell modules.
5. Connect the BAT+ **J6**, BAT- **J8** and PACK- **J7** connections using M3 bolts into the provided holes on the board.

:::info
The firmware flashing connector (J3) is used only to flash an updated firmware to the BMS and must be used only when consulted by us.
:::

## Connection verification

Verify the following to make sure that the harness is assembled properly:

1. ION Lens shows no errors after connection
2. All the connectors are locked properly in position
3. There is no strain on the cables while bending and routing
4. The shunt switches are in position and configured properly