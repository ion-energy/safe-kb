---
layout: docs
title: Mounting Guidelines
group: documentation
description: This document highlights the method by which the FS-CT BMS board should be mounted in the customer application


---

The objective of this document is to provide the user/customer of the ION Energy FS-CT BMS board with an overview of the BMS installation guidelines including the environmental conditions it is most suitable to operate in.

## Handling

All ION Energy BMS boards are shipped in cartons containing either 5, 20 or 50 boards based on the size of the order. All the boards are shipped individually wrapped in antistatic bags which are further encapsulated in foam sheets. When handling the boards, please ensure the following measures are taken:

<img src="./assets/mounting-guidelines/antistatic-cover.png" width="60%"></img>

- Ensure that all personnel receive education and training on ESD preventive measures in general.
- The operator/handler of the board must wear a grounded antistatic wrist-strap in contact with their skin when handling the boards outside the antistatic bags.
- The boards are unboxed on an Electro-Static Discharge(ESD) mat to prevent any unwanted static current from damaging the board.

<img src="./assets/mounting-guidelines/esd-mat.png" width="60%"></img>

## Storage

- The BMS boards must be stored at temperature and humidity levels that do not exceed [specified operating conditions](#Environmental-conditions) that are shown below.
- If the BMS is going to be stored for longer periods of time, it is ION Energy's recommendation to store the boards at temperatures below $25°C$ and humidity levels below **70% RH**.

### Environmental conditions

Standard operating conditions which will not reduce the system lifetime are temperature: **$15-35 °C$**, Hmidity: **25-75% RH** and air pressure **860-1060 mBar**.

## BMS dimensions

The ION Energy BMS is a highly configurable system that can manage multiple lithium ion chemistries and packages. The Mechanical Dimensions of the board are shown below:

| Parameter | Units | Value                      |
| --------- | ----- | -------------------------- |
| Dimension | *mm*  | *75 x 60 x 9.2(L x W x H)* |
| Weight    | *g*   | *<35*                      |

![FS_CT_2D_Diagram](./assets/mounting-guidelines/ct-2d.png)

## Mounting

<img src="./assets/mounting-guidelines/ct-mount.jpg"></img>

The board provides four Ø3.3mm holes symmetric mounting holes that accept **M3** screw **(metric)** or **5-40** **UNC** and **4-48** **UNF** screws **(imperial)**.

![Mounting Hole Locations](./assets/mounting-guidelines/mounting-holes.jpg)

The 4 screws should be fastened in a specific order as illustrated in the sequence column of the following table.

|         | Torque($Nm$)                        | Sequence |
| ------- | ----------------------------------- | -------- |
| Initial | 0.3*[Recommended Torque](#torquing) | 1-2-3-4  |
| Final   | [Recommended Torque](#torquing)     | 4-3-2-1  |

The two Ø5mm holes indicated in the figure are to be used to connect the board to the battery pack and power the board. The same can also be used to mount the board if proper insulation is provided. If these holes are used to mount the board, these holes should be fastened only after the main four mounts are fastened.

### Screws

Ensure the board is mounted and torqued in a manner that is perpendicular to the board.

We recommend the following screw types for effective mounting of the BMS (*for example only*):

- [M3xL Pan Head Philips Drive SEMS Screw](https://int.rsdelivers.com/product/rs-pro/din-125a-bs-4183/rs-pro-m3-x-6mm-zinc-plated-steel-pan-head-sems/0278821) - SEMS screws have washers fixed on the screw shank thus preventing the possibility of washers falling into your application.
- [M5xL Pan Head Philips Drive SEMS Screw](https://int.rsdelivers.com/product/rs-pro/din-6797-bs-4183/rs-pro-m5-x-10mm-zinc-plated-steel-pan-head-sems/0279234) - SEMS screws have washers fixed on the screw shank thus preventing the possibility of washers falling into your application.

<div className="alert alert-primary" role="alert">
<span>:information_source:</span>
The length (L) and material of the screws is dependant on your application and can be chosen as per your requirement.
</div>

#### Torquing

We recommend a torquing of **$1 N/m^2$** for the M3 screws.

The board is FR4 4 layers PCB. The FR4 material can withstand the recommended torque for M3 screws ($1 N/m^2$) without any failures. The optional use of the M5 screw to mount board also would include the usage of wire lugs to make connection with the board. Therefore, the recommended torque for M5 screw is increased to **$1.8 N/m^2$**, whereas the value is susceptible to change depending on the application.

It is ION's recommendation that **[pneumatic](https://www.ingersollrandproducts.com/en-in/power-tools/products/air-and-electric-screwdrivers/1-series-inline.html#tab-2-594ad5ed-66ad-495c-96ca-0e907ebf737f)/[electric torque drivers](https://www.ingersollrandproducts.com/en-in/power-tools/products/air-and-electric-screwdrivers/brushless-electric.html)** are used so as to carefully control the torque applied to each screw. **Magnetic** drive bits are recommended to ensure that the screws do not fall into your application.

<div className="alert alert-warning" role="alert">
<span>:warning:</span>
Failure to follow these guidelines can result in stress cracking of the surface mounted components and/or the board itself
</div>

### Clearances

#### Printed circuit board

The board has components populated on both sides of the board which means it is important to ensure that the board has enough clearance from the component it is being mounted to, be it a sheet metal plate or a plastic enclosure box.

<div className="alert alert-info" role="alert">
<span>:information_source:</span>
A tolerance stack up analysis is recommended to ensure this is taken care of at the design level.
</div>

As the BMS dimensions above show, the tallest component on the top side of the BMS board is 2.80 mm in height. However, depending on the EMS the board is manufactured at, there is a rare possibility of through hole component pins being extended beyond 2.80 mm which is why we recommend a minimum of 6 mm clearance from the mounting location.

The clearance can be achieved by using the following methods:

- For **Sheet Metal**
  - Use [Self Clinch Standoffs](https://int.rsdelivers.com/product/rs-pro/16475/rs-pro-steel-zinc-plated-self-clinching-standoff/0827451)

- For **Plastics**
  - Design bosses with [inserts](https://int.rsdelivers.com/product/rs-pro/14591/rs-pro-m3-brass-threaded-insert-diameter-4mm/0278534) that raise the board at least 6 mm from the next plastic surface.

An example of the same is shown below:
![CT Clearances](./assets/mounting-guidelines/ct-mounting.jpg)

As observed in the image above, sufficient clearance has been provided in the case of sheet metal enclosure to ensure that no mating screws or parts can cause a short on the board.

#### Wire harness

The FS-CT BMU board has four different connectors mounted on it. They allow the user to [wire their system](typical-wire-harness) to the BMS by the means of a wire harness. The connectors on the BMS are shown below:

![CT Connectors](./assets/mounting-guidelines/connectors.jpg)

- [Cell Voltage Sensing Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/SM14B-GHS-TB-LF-SN/455-1576-1-ND/807844) - 14 Pin Connector
- [Temperature Sensor NTC Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/SM04B-GHS-TB-LF-SN/455-1566-2-ND/807788) - 4 Pin Connector
- [Ignition, CAN bus, Soc Display Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/SM14B-GHS-TB-LF-SN/455-1576-1-ND/807844) - 14 Pin Connector
- [GPIO Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/SM06B-GHS-TB-LF-SN/455-1568-2-ND/807790) - 6 Pin Connector

All the above connectors are mated with wire to board to connectors containing **[26 AWG](http://www.farnell.com/datasheets/1855485.pdf)** cables. It is important to leave sufficient clearance around the board to ensure the wiring can be bent easily without stressing the wire crimps or the connectors on the board.

<div className="alert alert-info" role="alert">
<span>:information_source:</span>

The wire harness required for most applications using FS-CT requires standard multi-core wires which need 8 times the overall wire diameter as bend clearance.

</div>

## Assembly instructions

Below are the steps and warnings that need to be added to the BMS manual as a part of assembly instruction.

1. The BMS board is designed to be mounted with four fasteners at the [indicated locations](#mounting) in the figure.
2. Fasteners must be tightened in the [denoted fashion](#mounting) to enable alignment and to reduce stress on the components while assembling.
3. Consider the space between the nearest component and the type of [fastener](#screws) being used. Kindly follow the Standard instructions to remain compliant.
4. Care must be taken to not to damage the conformal coating on the board surface.

<div className="alert alert-info" role="alert">
<span>:information_source:</span>

Mounting of the boards in the horizontal orientation is preferred.

</div>

### Assembly warnings

1. Use of fasteners greater than M3 will result in board damage.
2. Ensure sufficient torque setting. Over torque might crack the SMD components near to the fastening points. So, care must be taken.

<div className="alert alert-info" role="alert">
<span>:information_source:</span>

Mounting of the boards in the horizontal orientation is preferred.

</div>

## Assembly verification

1. The installation must be verified before configuring and powering up the board. Follow the instructions below.
2. Verify that all the fasteners are assembled.
3. Verify that the fasteners are not under-torqued or over - torqued.
4. Verify the gap between the tallest component and interaction point in the assembly.
5. Verify whether all the signal connectors are completely snap locked.
6. Make sure that the cable routing in close proximity to the components in the board is avoided.

## Support

For any questions or deviations, please contact [us](support@ionenergy.co) before troubleshooting.
