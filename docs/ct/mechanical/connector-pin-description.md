---
layout: docs

group: FS-CT
title: Typical FS-CT Connectors and Pin Description
description: Information about the pin description to interface the wire harness with the application.
---

The connectors on the Battery Management System (BMS) FS-CT connects the wire harness from the board to various applications for batteries with less than or equal to a 10S combination for each board.

## FS-CT harness description

The FS-CT board has 4 connectors on it. The harnesses are as following:

- Cell voltage sensing harness (Connector J1)
- NTC harness (Connector J2)
- Ignition, CAN bus and SoC Display Harness (Connector J3)
- Custom General Purpose Input/Output (GPIO) (Connector J4)

The harness information can be found [here](typical-wire-harness).

## Pin description

This section covers the connectors and their pin assignements. The numbering for each connector is illustrated below.

### Cell voltage sensing connector (Connector J1)

There are 13 Open-ended cables that need to be connected to the busbars or cells depending on the usage of lugs or soldering as a method of interconnection.

![Schematic](./assets/connector-pin-description/vltg.png)

No wires to be populated in the blacked out positions. Use **[26 AWG wires](http://www.farnell.com/datasheets/1855485.pdf)** or equivalent.

Table 3: Cell Voltage Sensing (Connector J1)

| Pin number | Type |     Description      |
|:----------:|:----:|:--------------------:|
|     1      | C10  | Cell Voltage Sensing |
|     2      | C10  | Cell Voltage Sensing |
|     3      | C09  | Cell Voltage Sensing |
|     4      | C08  | Cell Voltage Sensing |
|     5      | C07  | Cell Voltage Sensing |
|     6      | C06  | Cell Voltage Sensing |
|     7      | C05  | Cell Voltage Sensing |
|     8      | C04  | Cell Voltage Sensing |
|     9      | C03  | Cell Voltage Sensing |
|     10     | C02  | Cell Voltage Sensing |
|     11     | C01  | Cell Voltage Sensing |
|     12     |  C0  | Cell Voltage Sensing |
|     13     |  C0  | Cell Voltage Sensing |
|     14     |  NC  |    No Connection     |

### NTC connector (Connector J2)

There are 4 cables that need to be attached in a pair using Focusens NTC Resistor to the busbars or cells depending on the application. Use **[26 AWG wires](http://www.farnell.com/datasheets/1855485.pdf)** or equivalent.

![Schematic](./assets/connector-pin-description/ntc.png)

Table 4: NTC Harness (Connector J2)

| Pin number | Type |     Description     |
|:----------:|:----:|:-------------------:|
|     1      | TS2  | Temperature Sensing |
|     2      | VC5x |       Supply        |
|     3      | TS1  | Temperature Sensing |
|     4      | GND  |       Ground        |

### Ignition, CAN bus and SoC Display connector (Connector J3)

There are 14 open-ended cables out of which, 4 are used for Ignition, 6 are used for SoC Display and 4 are used for CAN bus interface. Use **[26 AWG wires](http://www.farnell.com/datasheets/1855485.pdf)** or equivalent.

![Schematic](./assets/connector-pin-description/conn3.png)

As shown in the figure below, this connector is connected to the charger, ignition key and the display SoC LEDs.

Table 5: Ignition, CAN bus and SoC Display Harness (Connector J3)

| Pin number |   Type   |    Description    |
|:----------:|:--------:|:-----------------:|
|      1     | Ignition |    Display out    |
|      2     | Ignition |    Keyin Return   |
|      3     |   KEYIN  |     Keyin Out     |
|      4     |  Supply  |       Supply      |
|      5     |  SoC LED |      SoC LED      |
|      6     |  SoC LED |      SoC LED      |
|      7     |  SoC LED |      SoC LED      |
|      8     |  SoC LED |      SoC LED      |
|      9     |  SoC LED |        GND        |
|     10     |  SoC LED |      GND CAN bus      |
|     11     |   Comm   |    SMBC [Core]    |
|     12     |   Comm   |    SMBD [Core]    |
|     13     |  Supply  |       5V CAN bus      |
|     14     |  Supply  | 5V external loads |

### Custom general purpose input/output (GPIO) connector (Connector J4)

There are 6 open-ended cables for GPIO harness.

![Schematic](./assets/connector-pin-description/conn4.png)

As shown in the figure below, the connection to the charger detection is made to this connector using GPIO harness. Use **[26 AWG wires](http://www.farnell.com/datasheets/1855485.pdf)** or equivalent.

Table 6: Custom General Purpose Input/Output (GPIO) (Connector J4)

| Pin number |    Type    |   Description   |
|:----------:|:----------:|:---------------:|
|      1     |  Presence  | Presence Return |
|      2     |  Presence  |   Presence Out  |
|      3     |    GPIO    |   Custom GPIO   |
|      4     |    GPIO    |   Custom GPIO   |
|      5     | Open Drain |    Open Drain   |
|      6     | Open Drain |    Open Drain   |

:::info
  Please ensure all the connections are made in a proper fashion.
To verify this, ensure there was a click sound after the connection was made.
:::