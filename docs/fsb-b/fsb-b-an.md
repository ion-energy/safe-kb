---
layout: docs
group: fsb-b
title: FSB-B Short circuit protection
description: User guide to include additional snubber capacitance for use with FSB-B
---

## Application Note: FSB-B Capacitive Snubber Reinforcement for Enhanced Short Circuit Protection

### Physical Model of a Battery with FSB-B

![physical model](./assets/battery_physical_model.svg)

The above figure depicts a lumped parameter model that contains the parastic elements on a typical battery pack. The cells are stacked in the form of series and parallel connections, interconnected via busbars. These busbars have a parasitic inductance and resistance that is normally non-negligible, but is of high importance when it comes to the dynamics of an event of short-circuit.

The battery can be broken down into 2 sections viz.

- Cells stacked together by the busbar. Contains the busbar resistance and inductance.
- Entire EMA of the Battery pack, containing the wiring inductance from the busbar to the FSB-B, and to the external contacts.

### During Event of Short Circuit

In case an event of short circuit happens at the battery terminals, there is a huge current spike that the battery experiences. This event energizes all the parasitic elements (busbar and wiring inductances) within the battery pack.

![short circuit](./assets/short_circuit.svg)

This accumulated energy will find a way to collapse as soon as there is an attempt to open the FETs of the FSB-B. When the FSB-B is opened, it causes a huge $dI/dt$ thus causing a high flyback voltage across each inductor within the battery pack.  
The quantum of this flyback voltage is a function of the inductance and $dI/dt$. This is additive to the battery pack voltage and is seen by:

- Power Path FETs of the FSB-B
- Power Supply of the FSB-B

This instantaneous energy is high enough to *cause permanent damage* to the FSB-B if it is not arrested correctly.  

For the wiring inductance of the BATT+ line (L+ Wiring), there is a freewheeling path that gets activated when the short circuit opens and dissipates the energy in the wiring inductance.

For the busbar and wiring inductance of the BATT- line (L- Wiring), there is no provision to arrest this additive voltage and dissipate this energy. This is also a subject to the busbar design and the battery pack design that governs these parameters and is variable.

![short circuit freewheel](./assets/short_circuit_snubbed.svg)

### Introduction of Snubber Capacitance in the network

In order to absorb this additional energy, a capacitive tank needs to be introduced within the battery back. This capacitance needs to be connected at the point on the battery pack where the FSB-B's supply connection is made on the busbar. This point is to be treated as the star-connection location in the battery pack. The busbar, BATT+ connection to the FSB-B and the +ve terminal of the capacitor is to be starred via a low resistance and low inductance path.  

![short circuit snubbed](./assets/short_circuit_snubbed.svg)

### Guidelines

- Estimate the internal impedance of the battery pack and consider realistic busbar and contact resistance to determine the resistance during a short-circuit.
- Estimate the peak short circuit current $I_{SC} = V_{PACK(max)}/R_{PACK}$
- Estimate or measure the busbar inductance.
- The maximum energy stored in this inductance can be estimated as $E_{max}=0.5.L_{busbar}.I_{SC}^2$
- The snubber capacitance for compensating the effect of this inductance can be estimated by $C_{snubber}=\frac {2.E_{max}} {(V_{CAP(max)} - V_{PACK(max)})^2}$
- The star connection between the positive side of the busbar and that of BATT- on the FSB-B should be as per the wiring recommendation shown above.
- For a short circuit energy of ~ 9.5 Joules (~ 17 μH cumululative inductance @ 1.05 kA of short circuit current), the recommended value of this snubber capacitance is 470 μF/250V. Recommended MPN: 380LX471M250J042
