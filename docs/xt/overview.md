---
layout: docs

group: xt
title: FS-XT Overview
description: An overview of FS-XT specifications & features.
---

![xt-overview](./assets/overview/fs-xt-01.jpg)

## Introduction

FS-XT is a distributed Battery Management system designed for High Voltage battery systems that are built using multiple smaller battery modules connected in series or parallel. The system consists of a Master unit and can be connected up to 40 Slave units. Each Slave unit can monitor from 6 to 18 cells in series.

The slave units communicate with Master over the state of the art isoSPI protocol without the need of a processor. The Master unit runs advanced SOC and SOH algorithms to provide accurate system information. It supports 2 current sensing channels, real time clock circuitry, configurable input and output ports, temperature sensing channels and can drive up to 8 external relays. It also comes with premium safety features suitable for automotive and storage applications like support for insulation monitoring device, system interlock monitoring, contactor weld detection and system communication with isolated CAN bus.

## Application

- High-Power Electric and hybrid commercial vehicles and buses
- Construction Equipment
- Industrial Energy Storage Systems (ESS)
- Backup battery systems

## Features

### Safety

- Isolation Rating of 1kV(min) at system level.
- Contactor Weld Detection.
- System Interlock Monitoring.
- Support for Insulation Monitoring Device.

### Performance

- Smart Passive Balancing Algorithm with a max. balancing Current of 420mA
- Operational Temperature range -40°C to 85°C
- Accurate State of Charge (SOC) and State of Health (SOH) estimation for different cell chemistries
- Capable of being used as a building block of a bigger Power Network of multiple such BMS Systems connected in series or parallel
- Easily Customizable features like additional temperature measurements, current sensor, higher balancing current, heater, etc.
- Sleep Mode Current of less than 10uA per Slave Board
  
### Configuration and Data Management

- Stackable architecture based on Master which is system monitoring unit (SMU) and slave which is battery monitoring system (BMU) configuration.
- Bluetooth 4.1 BLE monitoring capabilities with real time Data Visualization on Companion Mobile App
- High resistance to EMI and increased accuracy.
- Up to 20 years of data history on an 8GB micro SD card
- Compatible with Desktop based CAN bus based tools for system configuration, real time data visualization
- Compatible with Edison Analytics for advanced Data Visualization and Analytics

## BMS System Specifications

| Parameters                   | Description                                          |
| ---------------------------- | ---------------------------------------------------- |
| System voltage               | 13 – 900 VDC                                         |
| Supported Pack Configuration | Pre PDU Serialization                                |
|                              | Post PDU Serialization                               |
|                              | Pre PDU Paralleling                                  |
|                              | Post PDU Paralleling                                 |
| Supported Communication      | 2 CAN2.0 A/B isolated CAN bus for system integration |
| Supported CAN bus speeds     | 125K, 250K, 500K, 1M bits/sec                        |
| High system Isolation level  | 1KVDC for isoSPI                                     |
|                              | 2.5KDCV for CAN bus                                  |
|                              | 3KVDC for voltage sensing lines                      |
| Usability                    | CAN bus based configuration settings                 |
|                              | Real time data monitoring                            |
|                              | Data download and analysis                           |
| Temperature range            | -40°C to 85°C                                        |

## Master Specifications

| Parameters                                            | Description                                     |
| ----------------------------------------------------- | ----------------------------------------------- |
| Input Voltage                                         | 9 VDC to 60 VDC                                 |
| Number of slaves supported per communication channel  | 1 to 10                                         |
| Number of communication channels available per Master | 4                                               |
| Number of cells in series for system                  | 6-180                                           |
| High voltage measurement channels                     | 4 isolated channels                             |
| High voltage measurement range                        | 0-900V                                          |
| Accuracy of high voltage measurement                  | +-1VDC                                          |
| Sleep Mode Current                                    | 1mA                                             |
| Current sensors                                       | Supports 2 external hall effect current sensor  |
| Range of current measurement input                    | 0-5VDC                                          |
| Temperature sensors                                   | 4 external temperature sensor (NTC) connections |
| System safety monitoring features                     | Supports insulation monitoring                  |
|                                                       | Supports PWM based interlock loop               |
| Contactor safety feature                              | 2 contactor weld monitoring channels            |
| General Purpose output                                | 8 channels for driving contactors               |
|                                                       | 2 high side open drain outputs                  |
|                                                       | 2 low side open drain outputs                   |
| Dimensions                                            | 152 X 152 X 15 mm                               |

## Slave Specifications

| Parameters                           | Description                                    |
| ------------------------------------ | ---------------------------------------------- |
| Cell voltage range                   | 1 VDC to 5 VDC                                 |
| Cell configuration (per Slave board) | 6 - 18 Cells in series                         |
| Balancing current per cell           | 420mA @ 4.2V                                   |
| Current consumption on board         | 15mA typically in normal mode                  |
|                                      | 5µA typically in sleep mode                    |
| Communication with Master            | Iso-SPI                                        |
| Temperature sensors                  | 1 on the board + 3 external sensors (10kΩ NTC) |
| Dimensions                           | 100 X 75 X 12 mm                               |

## Typical application diagram

<img src="./assets/overview/fs-xt_Application_diagram-01.png" width="100%"></img>
