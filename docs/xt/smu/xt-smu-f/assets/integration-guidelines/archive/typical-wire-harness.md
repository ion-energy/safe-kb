---
layout: docs

group: SMU
title: SMU Wire harness
description : Wire Harness FS-XT - SMU <> BMU
---

## Overview

The Wire Harness connects the Battery Management System (BMS) FS-XT boards SMU to BMU for applications involving heavy-duty loads and high voltage applications.

## Items in the Kit

The items are split into two kits as below:

   1. FS-XT SMU Kit
   2. FS-XT BMU Kit

### SMU Kit

| Harness                            | Quantity |
|-----------------------------------|:--------:|
| BMU Iso. SPI Bus                   | 1-4      |
| Current sensor                     | 1        |
| Coil connections and supply        | 1        |
| Board supply                       | 1        |
| GPIO and CAN                       | 1        |
| Isolation monitoring device (IMD)  | 1        |
| High voltage interlock loop (HVIL) | 1        |
| DC bus voltage monitoring          | 1        |

### BMU Kit

| Harness           | Quantity | Notes                                   |
|-------------------|----------|-----------------------------------------|
| Cell Connections  | 1        |                                         |
| NTC Connection    | 1        |                                         |
| Auxiliary         | 1        | Pin description change with application |
| Second Protection | 1        | Optional (dependant on application)     |
| S-Pin             | 1        | Optional (dependant on application)     |

The number of sets of these cables needed is dependent on the application for BMU Iso. SPI bus Harness and the Cell Connections Harness on the BMU Board.

## Safety Information

Read these instructions carefully and look at the equipment to become familiar with it before trying to install, operate, service or maintain it. The following safety messages may appear throughout this manual or on the equipment to warn of potential hazards or to call attention to information that clarifies or simplifies the procedure.

### Note

Electrical equipment should only be installed, operated, serviced, and maintained by qualified personnel. A qualified person is one who has skills and knowledge related to the construction, installation, and operation of electrical equipment and has received safety training to recognize and avoid the hazards
involved.

## Safety Instructions

- Cover the batteries with an insulating blanket before installing the harnesses.
- Wear safety glasses, rubber gloves, and rubber boots.
- Use double-insulated tools.
- Do not short-circuit the battery terminals; a short circuit could cause the batteries to explode.
- Do not lay tools or metal parts on top of the batteries or near the cable lugs.
- Use only cables supplied by ION Energy unless otherwise Specified.

[COMMENT]: # (Rachit should review this section in order to provide insights on safety requierment for HV. Also in some region of the world requiere some level of habilitation to operate on HV battery, we should tell which one. in france : **H0**V HTA certification are requiered)

## Harness General Information

### Wiring Diagram

The below wiring diagram represents the interconnection between SMU and BMU board (one board is shown). The figure also shows the connection of the Insulation Monitoring Device (IMD) connection with the SMU Board.

![Notice](./assets/typical-wire-harness/WBD.jpg)

| Harness name          | Reference | Qty |
|-----------------------|-----------|-----|
| BMU Iso. SPI Bus      | **H1**    | 4   |
| Current sensor        | **H2**    | 1   |
| Coil connection       | **H3**    | 1   |
| Board and coil supply | **H4**    | 2   |
| GPIO and CAN          | **H5**    | 2   |
| IMD                   | **H6**    | 1   |
| HVIL                  | **H7**    | 1   |
| Voltage Monitoring    | **H8**    | 6   |
| Cell connections      | **H9**    | 1   |
| NTC connection        | **H10**   | 1   |
| Second Protection     | **H11**   | 1   |
| Auxiliary             | **H12**   | 1   |
| S-Pin                 | **H13**   | 1   |

### Connector information

#### BMU Iso. SPI bus

The **H1** Harness makes the connection between the SMU and the BMU boards. Furthermore, it also connects the BMU boards to more BMU boards creating a bus. The bus harness uses two position Molex Duraclik connectors.

The recommended number of BMU boards in a bus is 8 boards.

The pin-description for the isoSPI harness is given in the following table:

| Signal Name | Type         | Description                |
|-------------|--------------|----------------------------|
| isoSPI IP   | Differential | 1 kV Galvanically Isolated |
| isoSPI IM   | Differential | 1 kV Galvanically Isolated |

#### Current Sensor

The **H2** Harness makes the connection between the SMU board and the Hall Effect Sensor.

This harness uses a 8 position Molex Duraclik connector on the SMU board. Out of the 8 pins, 5 pins are used and 3 are left empty.

The pin-description for the Current sensor harness is given in the following table:

| Signal Name | Type         | Description                     |
|-------------|--------------|---------------------------------|
| GND         | Ground       | Hall effect Sensor Ground       |
| NC          |              | No Connection                   |
| INSENSE2    | Analog Input | Shielded Connection             |
| 5V Supply   | Power Supply | Hall Effect Sensor Power Supply |
| GND         | Ground       | Hall effect Sensor Ground       |
| NC          |              | No Connection                   |
| INSENSE1    | Analog Input | Shielded Connection             |
| NC          |              | No Connection                   |

#### Coil connection

The **H3** Harness makes the connection between the SMU board and the Contactors.

This harness uses a 12 position Molex Duraclik connector on the SMU board. Out of the 12 pins, 8 pins are used and 4 are left empty.

The pin-description for the Coil connection harness is given in the following table:

Note: All the (-) connections are made possible by connecting the respective pin to VCOIL (-) in the **H4** harness.

| Signal Name | Type   | Description             |
|-------------|--------|-------------------------|
| PCHG+       | Output | PRECHARGE Contactor (+) |
| PCHG-       | Output | PRECHARGE Contactor (-) |
| (DSG)+      | Output | Discharge (+)           |
| (DSG)-      | Output | Discharge (-)           |
| (CHG)+      | Output | Charge (+)              |
| (CHG)-      | Output | Charge (-)              |
| Fan (+)     | Output | Fan (+)                 |
| Fan (-)     | Output | Fan (-)                 |
| EMG (+)     | Output | EMG LIGHT Contactor (+) |
| EMG (-)     | Output | EMG LIGHT Contactor (-) |
| AUX (+)     | Output | Auxiliary (+)           |
| AUX (-)     | Output | Auxiliary (-)           |

#### Board and Coil supply

The **H4** Harness provides the required supply to the SMU board and the coils connected to the board via **H3** harness.

This harness uses two connectors. The coil supply connector is a two position Molex Microfit connector.

The SMU board is provided the supply using a three position Molex Duraclik connector.

The pin-description for the Coil supply harness is given in the following table:

| Signal Name | Type        | Description   |
|-------------|-------------|---------------|
| VCOIL+      | Coil Supply | Coil Supply + |
| VCOIL-      | Coil Supply | Coil Supply - |

The pin-description for the Board supply harness is given in the following table:

| Signal Name | Type   | Description      |
|-------------|--------|------------------|
| GND         | Ground | BMS board ground |
| NC          |        | No Connection    |
| VSUP+       | Supply | BMS board Supply |

#### GPIO and CAN bus

The **H5** Harness provides the connections General Purpose Input/Output and the CAN connections between the application and the SMU board.

The GPIO connection uses a twelve position Molex Duraclik connector. The CAN bus connector is a four position Molex Duraclik connector.

The pin-description for the GPIO harness is given in the following table:

| Signal Name | Type       | Description  |
|-------------|------------|--------------|
| GPIO1       | I/O        | Input/Output |
| GPIO2       | I/O        | Input/Output |
| GPIO3       | I/O        | Input/Output |
| GPIO4       | I/O        | Input/Output |
| GPIO5       | I/O        | Input/Output |
| GPIO6       | I/O        | Input/Output |
| GPIO7       | I/O        | Input/Output |
| GPIO8       | I/O        | Input/Output |
| GPIO9       | I/O        | Input/Output |
| GPIO10      | I/O        | Input/Output |
| OD1         | Open Drain | Open Drain 1 |
| OD2         | Open Drain | Open Drain 2 |

The pin-description for the CAN harness is given in the following table:

| Signal Name | Type         | Description                  |
|-------------|--------------|------------------------------|
| CANH        | Differential | 5V Isolated + ESD Supression |
| CANL        | Differential | 5V Isolated + ESD Supression |
| RTERM       | NA           |                              |
| GNDCAN      | Ground       | CAN Ground                   |

#### IMD

The **H6** Harness provides the connection between the Insulation Monitoring Device to the SMU board for safety.

The IMD harness makes the connection possible using a four position Molex Duraclik connector.

The pin-description for the IMD harness is given in the following table:

| Signal Name | Type   | Description         |
|-------------|--------|---------------------|
| IMD Supply  | Supply | 12 V supply to IMD. |
| HST 1       | Input  | Warning Signal      |
| HST 2       | Input  | Error Signal        |
| IMD GND     | Ground | IMD Ground.         |

#### HVIL

The **H7** Harness provides the connection for the Hardware Interlocking to insure all the connectors are connected for safety and ensures tight connectivity.

The HVIL harness uses a two position Molex Duraclik connector for HVIL IN connections. The HVIL harness also uses a three position Molex Duraclik connector for HVIL OUT connections.

The pin-description for the HVIL IN harness is given in the following table:

| Signal Name | Type   | Description               |
|-------------|--------|---------------------------|
| HVIL_RTN    | Return | Hardware Interlock Return |
| HVIL_IN     | Input  | Hardware Interlock Input  |

The pin-description for the HVIL OUT harness is given in the following table:

| Signal Name | Type   | Description               |
|-------------|--------|---------------------------|
| HVIL_SUP    | Supply | Hardware Interlock Supply |
| HVIL_OUT    | Output | Hardware Interlock Output |
| HVIL_ORTN   | Return | Hardware Interlock Return |

#### Voltage Monitoring + Weld Detection

The **H8** Harness provides the connection for the Hardware Interlocking to insure all the connectors are connected for safety and ensures tight connectivity.

This harness uses 6, two position Molex Microfit connectors for voltage monitoring and weld detection. It uses four single ended voltage monitoring connections and two grounds.

The pin-description for the all the connectors in this harness is given in the following table:

| Signal Name | Type         | Description       |
|-------------|--------------|-------------------|
| VMON1       | Single Ended | Voltage Monitor 1 |
| VMON2       | Single Ended | Voltage Monitor 2 |
| GND ISO1    | Return       |                   |
| VMON3       | Single Ended | Voltage Monitor 3 |
| VMON4       | Single Ended | Voltage Monitor 4 |
| GND ISO2    | Return       |                   |

#### BMU

[COMMENT]: # (The pin description for all the connections to the BMU board varies from application to application.)

The **H9** Harness provides the connection between the cells and the BMU board for voltage sensing of the cells.

This harness uses a fourteen position Molex Duraclik connector. An additional six position Molex Duraclik connector for 18S battery packs and more.

The **H10** Harness provides the connection between the cells and the BMU board for temperature sensing of the cells.

This harness uses a six position Molex Duraclik connector.

The **H11**, **H12** and **H13** Harnesses are used as optional harnesses.

The **H11** harness uses a two position Molex Duraclik connector for the second protection harness. This harness provides additional safety to the BMU board and can be used for extremely high voltage applications.

The **H12** harness uses a ten position Molex Duraclik connector for auxiliary connections. This connection is totally application dependant.

The **H13** harness uses a twenty position Wurth WR-MM FRC connector to make the S-PIN connections possible. This connector uses a flat ribbon cable for balancing of any external board.

## Installation

### Assembly Instructions

Consider the following instructions before initiating the assembly

#### Preparation

1. All the Board mounted connectors are featured with a Secure lock (Snap lock)
2. Ensure Proper routing of the cables across the boards to prevent any signal interferences
3. Ensure that there is no strain on the cables due to an immediate change in routing direction
4. Plan for sufficient bend radius for the wire harness to prevent strains
5. Make sure that all the cables are snap locked
6. NTC Harness carry Ring lugs for mounting to the cell modules, sufficient care must be taken to assemble them on the cell modules.

#### Harness Connection

1. Connect the NTC Harness to BMU board at **H10** Harness Connector and the lug termination to be connected to the intended location on the cell modules
2. Connect the GPIO and CAN bus Harness connectors to SMU **H5** Harness connector and the other free end of the cable at the application.
3. Connect the SMU board with various application circuit at the GPIO harness using **H5** harness connector.
4. Connect the BMU to other BMU boards using the ISO-SPI harness at the **H1** harness connectors.
5. Connect the SMU to the contactors using the **H3** harness connectors, please ensure the coil supply harness is not connected.
6. Connect the SMU board to the Hall effect Sensor using the **H2** harness.
7. Connect the HVIL harness to the SMU board using **H7** harness connectors.
8. Connect the Voltage Monitoring connections from the contactors at the **H8** harness connectors.
9. Connect the IMD board to the contactors. Also, connect the SMU to the IMD board using the **H6** harness connector.
10. Connect the Auxiliary application connections to the BMU boards using **H12** harness connectors.
11. Connect the application specific connections like the external board balancing and the second protection harness to the BMU board as shown in the figure.
12. Connect the Cell sensing harness to the BMU boards using the **H9** harness connectors.
13. In the end, connect the Coil and the Board supply on the SMU board using the **H4** harness connectors.

![Notice](./assets/typical-wire-harness/connector.jpg)

## Verification

Verify the following to make sure that the harness is assembled properly

1. ION Lens shows no error after connection
2. All the connectors are locked properly in position
3. There is no strain on the cable while bending
4. The shunt switches are in position and configured properly
