---
layout: docs

group: FS-XT
title: FS-XT SMU Connectors and Pin Description
description: Connector description for interfacing of SMU wire harness.
---

This section describes the connectors present on the FS-XT SMU.

## Overview

<img src="./assets/xt-smu-connectors/smu-connector-overview.png" width="100%"></img>

## Connectors on the FS-XT SMU

The FS-XT-SMU has 24 connectors present on it. They are:

| Connector |              Description               |
| --------- | :------------------------------------: |
| J1        |       Voltage Monitor Channel 1        |
| J2        |        Voltage Monitor Ground 1        |
| J3        |       Voltage Monitor Channel 2        |
| J4        |       Voltage Monitor Channel 3        |
| J5        |        Voltage Monitor Ground 2        |
| J6        |       Voltage Monitor Channel 4        |
| J7        |            isoSPI Channel 1            |
| J8        |            isoSPI Channel 2            |
| J9        |            isoSPI Channel 3            |
| J10       |            isoSPI Channel 4            |
| J11       |           SD Card Interface            |
| J12       |             CAN Channel 1              |
| J15       |             Interlock Out              |
| J16       |         Input/Output Interface         |
| J17       | Insulation Monitoring Device Interface |
| J18       |     Contactor Interface Channel 1      |
| J19       |           Board Power Supply           |
| J20       |              Interlock In              |
| J21       |       Current Sensors Interface        |
| J22       |      Contactor Coil Power Supply       |
| J24       |             CAN Channel 2              |
| J25       |     Contactor Interface Channel 2      |
| J26       |   Temperature Measurement Interface    |
| J27       |      Open Drain Output Interface       |

## Pin Description

This section explains the pins on each of the connectors.

### J1 - Voltage Monitor Channel 1

This connector is used as a voltage measurement channel for weld check & precharge voltage check. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |    Description    |
| ---- | :---------------: |
| 1    | Voltage Monitor 1 |
| 2    |   No Connection   |

### J2 - Voltage Monitor Ground 1

This connector is used as a ground reference for voltage measurement channels 1 & 2. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |      Description      |
| ---- | :-------------------: |
| 1    | Voltage Monitor GND 1 |
| 2    |     No Connection     |

### J3 - Voltage Monitor Channel 2

This connector is used as a voltage measurement channel for weld check & precharge voltage check. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |    Description    |
| ---- | :---------------: |
| 1    | Voltage Monitor 2 |
| 2    |   No Connection   |

### J4 - Voltage Monitor Channel 3

This connector is used as a voltage measurement channel for weld check & precharge voltage check. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |    Description    |
| ---- | :---------------: |
| 1    | Voltage Monitor 3 |
| 2    |   No Connection   |

### J5 - Voltage Monitor Ground 2

This connector is used as a ground reference for voltage measurement channels 3 & 4. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |      Description      |
| ---- | :-------------------: |
| 1    | Voltage Monitor GND 2 |
| 2    |     No Connection     |

### J6 - Voltage Monitor Channel 4

This connector is used as a voltage measurement channel for weld check & precharge voltage check. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |    Description    |
| ---- | :---------------: |
| 1    | Voltage Monitor 4 |
| 2    |   No Connection   |

### J7 - isoSPI Channel 1

This connector is used for ios-SPI communication with FS-XT BMUs (BMS Slave). It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin.png" width="100%"></img>

| Pins |  Description   |
| ---- | :------------: |
| 1    | isoSPI Line IP |
| 2    | isoSPI Line IM |

### J8 - isoSPI Channel 2

This connector is used for ios-SPI communication with FS-XT BMUs (BMS Slave). It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin.png" width="100%"></img>

| Pins |  Description   |
| ---- | :------------: |
| 1    | isoSPI Line IP |
| 2    | isoSPI Line IM |

### J9 - isoSPI Channel 3

This connector is used for ios-SPI communication with FS-XT BMUs (BMS Slave). It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin.png" width="100%"></img>

| Pins |  Description   |
| ---- | :------------: |
| 1    | isoSPI Line IP |
| 2    | isoSPI Line IM |

### J10 - isoSPI Channel 4

This connector is used for ios-SPI communication with FS-XT BMUs (BMS Slave). It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin.png" width="100%"></img>

| Pins |  Description   |
| ---- | :------------: |
| 1    | isoSPI Line IP |
| 2    | isoSPI Line IM |

### J11 - SD Card interface

This connector is used for mounting SD card on the SMU.

### J12 - CAN Channel 1

This connector is used for isolated CAN bus Communication over a CAN Bus. It consists of 4 pins.

<img src="./assets/xt-smu-connectors/4-pin.png" width="100%"></img>

| Pins |     Description      |
| ---- | :------------------: |
| 1    |       CAN High       |
| 2    |       CAN Low        |
| 3    | Termination Resistor |
| 4    |       CAN GND        |

### J15 - Interlock Out

This connector is used for checking the interlock line. It consists of 3 pins.

<img src="./assets/xt-smu-connectors/3-pin.png" width="100%"></img>

| Pins | Description  |
| ---- | :----------: |
| 1    |  HVIL Input  |
| 2    |   HVIL OUT   |
| 3    | HVIL OUT RTN |

### J16 - Input/Output Interface

This connector is used for connecting to the General Purpose Input Output (GPIO) pins. It has 15 pins.

<img src="./assets/xt-smu-connectors/15-pin.png" width="100%"></img>

| Pins |      Description      |
| ---- | :-------------------: |
| 1    | Low Side Open drain 1 |
| 2    | Low Side Open drain 2 |
| 3    |          GND          |
| 4    |     No Connection     |
| 5    |         GPIO          |
| 6    |         GPIO          |
| 7    |         GPIO          |
| 8    |       Ignition        |
| 9    |         GPIO          |
| 10   |         GPIO          |
| 11   |       GPIO RTN        |
| 12   |       GPIO RTN        |
| 13   |       GPIO RTN        |
| 14   |       GPIO RTN        |
| 15   |       GPIO RTN        |

### J17 - Insulation Monitoring Device Interface

This connector is used for interfacing the SMU with an Insulation Monitoring Device (IMD). It consists of 4 pins.

<img src="./assets/xt-smu-connectors/4-pin.png" width="90%"></img>

| Pins | Description |
| ---- | :---------: |
| 1    |     GND     |
| 2    |    Fault    |
| 3    |    Data     |
| 4    |   Supply    |

### J18 - Contactor Interface Channel 1

This connector is used for interfacing the SMU with Contactors, Fan & Heater. It consists of 12 pins.

<img src="./assets/xt-smu-connectors/12-pin.png" width="100%"></img>

| Pins |        Description        |
| ---- | :-----------------------: |
| 1    |   Precharge Contactor +   |
| 2    |  Precharge Contactor GND  |
| 3    |   Discharge Contactor +   |
| 4    |  Discharge Contactor GND  |
| 5    |    Charge Contactor +     |
| 6    |   Charge Contactor GND    |
| 7    |       Fan Control +       |
| 8    |      Fan Control GND      |
| 9    |     Heater Control +      |
| 10   |    Heater Control GND     |
| 11   |  Auxiliary Contactor 1 +  |
| 12   | Auxiliary Contactor 1 GND |

### J19 - Board Power Supply

This connector is used for powering the SMU. It consists of 3 pins.

<img src="./assets/xt-smu-connectors/3-pin.png" width="100%"></img>

| Pins |   Description   |
| ---- | :-------------: |
| 1    |   Supply GND    |
| 2    |  No Connection  |
| 3    | Supply Positive |

### J20 - Interlock In

This connector is used for checking the interlock line. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin.png" width="70%"></img>

| Pins | Description |
| ---- | :---------: |
| 1    | HVIL Input  |
| 2    |  HVIL OUT   |

### J21 - Current Sensors Interface

This connector is used for interfacing the SMU with current sensors. It consists of 8 pins.

<img src="./assets/xt-smu-connectors/8-pin.png" width="90%"></img>

| Pins |         Description          |
| ---- | :--------------------------: |
| 1    |     Current Sensor 2 GND     |
| 2    |  Current Sensor 2 Reference  |
| 3    |    Current Sensor 2 Input    |
| 4    | Current Sensor 2 Supply (5V) |
| 5    |     Current Sensor 1 GND     |
| 6    |  Current Sensor 1 Reference  |
| 7    |    Current Sensor 1 Input    |
| 8    | Current Sensor 1 Supply (5V) |

### J22 - Contactor Coil Power Supply

This connector is used for powering of coils of the contactors that the SMU drives. It consists of 2 pins.

<img src="./assets/xt-smu-connectors/2-pin-black.png" width="100%"></img>

| Pins |   Description   |
| ---- | :-------------: |
| 1    | Supply Positive |
| 2    |   Supply GND    |

### J24 - CAN bus Channel 2

This connector is used for isolated CAN bus Communication over a CAN Bus. It consists of 4 pins.

<img src="./assets/xt-smu-connectors/4-pin.png" width="70%"></img>

| Pins |     Description      |
| ---- | :------------------: |
| 1    |       CAN High       |
| 2    |       CAN Low        |
| 3    | Termination Resistor |
| 4    |       CAN GND        |

### J25 - Contactor Interface Channel 2

This connector is used for interfacing the SMU with auxiliary Contactors. It consists of 4 pins.

<img src="./assets/xt-smu-connectors/4-pin.png" width="70%"></img>

| Pins |        Description        |
| ---- | :-----------------------: |
| 1    |  Auxiliary Contactor 2 +  |
| 2    | Auxilary Contactor 2 GND  |
| 3    |  Auxiliary Contactor 3 +  |
| 4    | Auxilairy Contactor 3 GND |

### J26 - Temperature Measurement Interface

This connector is used for interfacing the SMU with external temperature sensors. It consists of 8 pins.

<img src="./assets/xt-smu-connectors/8-pin.png" width="100%"></img>

| Pins |       Description        |
| ---- | :----------------------: |
| 1    | Temperature Sensor 4 GND |
| 2    |  Temperature Sensor 4+   |
| 3    | Temperature Sensor 4 GND |
| 4    |  Temperature Sensor 3+   |
| 5    | Temperature Sensor 4 GND |
| 6    |  Temperature Sensor 2 +  |
| 7    | Temperature Sensor 4 GND |
| 8    |  Temperature Sensor 1 +  |

### J27 - Open Drain Output Interface

This connector consists of high side open drain outputs. It has 3 pins.

<img src="./assets/xt-smu-connectors/3-pin.png" width="100%"></img>

| Pins |          Description          |
| ---- | :---------------------------: |
| 1    |              GND              |
| 2    | High Side Open drain Output 1 |
| 3    | High side open drain Output 2 |
