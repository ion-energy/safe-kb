---
layout: docs

group: XT
title: Overview 
description: Overview of the integration guide and its different sections.
---

## Introduction

This section is a guide to integrating the FS-XT BMS with the battery pack. It aims to provide the steps recommended for the hardware installation of the BMS. Furthermore, it provides information about the bill of materials required for integration, system-level integration diagram, wiring guides, connectors and pins, connection order recommendations, NTC placements, etc.

## Breakdown of the process

The integration process is divided into the following sections.

### Integration BOM

This section of the guide lists all the components necessary for the integration of the BMS. As these components are from multiple vendors, the BOM helps in procurement planning. This section also helps in familiarizing with the components required for integration. 

Click [here](/docs/safe/xt/integration-guide/integration-bom) to navigate to this section.

### Wiring guide

This section of the guide gives detailed guidance on the system level wiring. Refer to this section for wiring and powering the BMS. For making the integration easy, the ION's sample harness kit comes with labels on each wire for easy reference.

 Click [here](/docs/safe/xt/integration-guide/wiring-guide) to navigate to this section.

This section discusses the following topics.

 - System-level integration diagram
 - Wiring guide for BMU
 - Wiring guide for SMU
  