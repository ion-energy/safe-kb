---
layout: docs

group: xt
title: Wiring guide
description: Wiring guide for the FS-XT platform
---

## System-level integration diagram

The FS-XT system-level integration diagram is shown below.


[FS-XT-integration-wd]:assets/FS-XT-integration-wd.png
![FS-XT-integration-wd][FS-XT-integration-wd]
[<i className="fas fa-search-plus"></i>][FS-XT-integration-wd]

:::info
This is an example integration diagram and is for references only.
:::

## Wiring guides

### FS-XT BMU

The details of all the connectors of the BMU can be found [here](/docs/safe/xt/bmu/xt-bmu-ots/connectors-and-pins)

Once you are familiar with the connectors and pins of the BMU, refer to the section below for connection order.

#### Cell connections

Connect the voltage sensing wires to the battery as per the number of cells per BMU in your system. The connection details can be found below.

##### Cell connections for 12S BMU

Here is the harness guide for connecting the cells to a 12S BMU. 12S BMU uses the J4 connector for cell connections. You can connect any battery of 6S (6 cells in series) to 12S (12 cells in series) to a 12S BMU.

- [6S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#6s-battery-cell-harness-with-12s-bmu)

- [7S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#7s-battery-cell-harness-with-12s-bmu)

- [8S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#8s-battery-cell-harness-with-12s-bmu)

- [9S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#9s-battery-cell-harness-with-12s-bmu)

- [10S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#10s-battery-cell-harness-with-12s-bmu)

- [11S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#11s-battery-cell-harness-with-12s-bmu)

- [12S battery cell harness with 12S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#12s-battery-cell-harness-with-12s-bmu)

##### Cell connections for 18S BMU

Here is the harness guide for connecting the cells to an 18S BMU. 18S BMU uses J2 & J4 for cell connections. You can connect any battery of 8S (8 cells in series) to 18S (18 cells in series) to an 18S BMU.

- [8S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#8s-battery-cell-harness-with-18s-bmu)

- [9S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#9s-battery-cell-harness-with-18s-bmu)

- [10S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#10s-battery-cell-harness-with-18s-bmu)

- [11S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#11s-battery-cell-harness-with-18s-bmu)

- [12S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#12s-battery-cell-harness-with-18s-bmu)

- [13S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#13s-battery-cell-harness-with-18s-bmu)

- [14S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#14s-battery-cell-harness-with-18s-bmu)

- [15S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#15s-battery-cell-harness-with-18s-bmu)

- [16S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#16s-battery-cell-harness-with-18s-bmu)

- [17S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#17s-battery-cell-harness-with-18s-bmu)

- [18S battery cell harness with 18S BMU](/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide#18s-battery-cell-harness-with-18s-bmu)

:::warning
  The cell connector should not be connected to the BMU at this point.
:::

#### NTC connections

The NTCs should be placed inside the battery pack to monitor cell temperature. The details of the placement can be found below.

##### Recommendation of placement of NTCs

The optimal way to place the NTC’s on the battery pack is to perform thermal analysis, identify the hotspots, and place the NTC’s on the hotspots.

If the battery pack temperature is uniformly distributed, the best practice is to cover the maximum volume of the pack. On this basis, the following are the recommended placements.

1. For BMS consisting of 3 NTC’s, the recommended arrangement is triangular, with 2 NTC’s on one side and the other one has to be placed on the opposite side as shown in the figure.

<img src="./assets/3_NTC.jpg" width="450" height="180"></img>

1. For BMS consisting of 5 NTC’s, the recommended arrangement is  rhomboidal, with 3 NTC’s on one side and the other two have to be placed on the opposite side as shown in the figure.

<img src="./assets/5_NTC.jpg" width="550" height="200"></img>

After the placement, connect the NTCs to the BMU.

#### isoSPI connections

Connect the communication lines of the BMU to the next BMU.

#### Powering up the BMU

Connect the cell connector to the BMU only after verifying the connections. For verification, measure the voltage on each pin of the cell connector. The voltage between any adjacent two pins should be equal to the corresponding cell voltage to which those pins are connected.

### FS-XT SMU

The details of all the connectors of the SMU can be found [here](/docs/safe/xt/smu/xt-smu-f/connectors-and-pins).

Once you are familiar with the connectors and pins of the SMU, refer to the section below for connection order.

#### Current sensor interface

Connect the current sensor to the SMU. If your system requires only one current sensor use the I_sense2 pin of the SMU.

#### Power distribution channels

Connect the contactors to the SMU as per your PDU architecture. If your system uses only 1 contactor, please use the discharge channel of the SMU. More details about the PDU architecture can be found [here](/docs/fusion/operations/setup/pdu-control-modes).

#### HVIL

Wire the High Voltage Interlock Loop as per your system. The HVIL works on 12 V so all the switches used in the loop should be rated for 12 V.

#### IMD

Connect the Insulation Monitoring Device as per the wiring details of the IMD.

#### High voltage sensing lines

Connect the HV sensing lines to the contactors based on the requirements. These channels can be used for weld check or voltage based precharging.

#### isoSPI channel

Connect the isoSPI communication lines from the BMU. If the cell stacks are connected in series, it is suggested that the BMU connected to the cell stack with the lowest potential inside the battery (the cell stack which has the Battery negative) is connected to the SMU.

:::info
Each isoSPI channel of the SMU can monitor a maximum of 12 BMUs.
:::

#### CAN bus

The FS-XT has two isolated CAN communication channels (connectors J12 and J24). To enable CAN bus communication connect the CAN 1 connector (J12) to the SMU.

 For adding the termination resistance to the CAN bus, short the pin **CAN TERM** to the pin **CAN L** on the CAN bus connectors.

In order to suppress the noise on the CAN bus, shield the CAN H and CAN L lines and connect the pin **CAN GND** to the shielding.

#### Power Supply

  - **Board power supply** - Connect the SMU to the power supply for powering up the BMS. The input range is from 9 V to 60 V.
  - **Power distribution channel power supply** - Connect the power supply for the contactors. The supply voltage should be equal to the voltage required by the contactors to operate. The input voltage range is 12 V to 72 V.
  - **Ignition button** - Connect the wires for the ignition(the ignition switch and GND) to the power supply. The input voltage range is 3.3 V to 12 V.

As per the system, the power supplies used for the above mentioned can be the same or different.

:::info
  For all the power supplies, 12 V can be selected as the common voltage. If the selected contactor coil operates on 12 V then, a single supply with appropriate power rating can be used for powering up the BMS, contactors, and for giving the ignition signal.
:::

:::warning
  The powering up of the SMU should be done after all other connections have been completed.
:::
