---
layout: docs

group: xt
title: Integration BOM
description: Guide on all the essential components before starting the integration of the BMS.
---

## Description

This section lists the components required to integrate the FS-XT BMS with the battery. Please have these components present with you, before starting the integration. Additionally, other components could be needed as per your application.

:::info
Before starting the integration, ensure that all individual components are thoroughly tested and are in working condition.
:::

## List of components

| Component                            | Quantity           | Description                                                 |
|--------------------------------------|--------------------|-------------------------------------------------------------|
| FS-XT-SMU                            | 1                  | Provided by ION                                             |
| FS-XT-BMU                            | As per requirement | Provided by ION                                             |
| HK-FS-XT-SMU                         | 1                  | Provided by ION                                             |
| HK-FS-XT-BMU                         | 1 per BMU          | Provided by ION                                             |
| Hall-effect Current sensor           | 1-2                  | To be procured as per the applications' specifications. The number of current sensors used depends on the current requirements of the system. Refer to the Hall effect current sensor's [selection guide](./assets/Hall%20Effect%20Current%20Sensor%20.pdf)     |
| Connector/harness for Current Sensor | 1                  | To be procured as per current sensor                        |
| Bi-directional Contactors            | 3                  | To be procured for pre-charge, discharge, and charge. Refer to the Contactor's [selection guide](./assets/High%20Voltage%20Contactors.pdf)         |
| Connector for contactor coils        | 3                  | To be procured as per contactor                             |
| Precharge Resistor                   | 1                  | To be procured as per the applications' specifications. Refer to the Precharge Resistor's [selection guide](./assets/Precharge%20Resistor.pdf)     |
| Fuse/MCB                             | As per requirement | To be procured as per the applications' specifications (recommended). Refer to the Fuse's [selection guide](./assets/Fuse.pdf)     |
| Power supply                         | 1-3                | DC power source for powering SMU, ignition, and contactors' coils. The number of power supplies depends on the system. |
| Android phone or Tab                 | 1                  | To be procured as per the applications' specifications     |
| IMD ISOMETER® IR155-3203/IR155-3204  | 1                  | To be procured as per the applications' specifications (optional). Refer to the IMD Isometer's [selection guide](./assets/Insulation%20Monitoring%20Device.pdf)     |
| Connector for IMD (optional)         | 1                  | To be procured as per IMD                                   |
| PCAN-USB Adapter                     | 1                  | Provided by ION                                             |
| Test Load                            | 1                  | To be procured as per the applications' specifications     |
| Charger                              | 1                  | To be procured as per the applications' specifications     |

:::warning
The selection guide document is only for a reference purpose, and the user can choose any vendor/manufacturer's component for their application. But the selected component must follow the selection criteria, as mentioned in the selection guide for each of the components in order to make it compatible with the ION BMS.
:::

:::info
The number of power supplies required and their wattage depends on the rating of the contactor's actuation current and actuation voltage.

For example, if the contactor's actuating voltage is 12V, then a common power supply can be used for ignition, BMS power supply and, Power distribution channels-power supply. If the contactor's actuating voltage is 48V, different power supplies - 12V supply for ignition,BMS power supply; 48V supply for the power ditribution channels are to be used. Kindly refer to <a href="/docs/safe/xt/smu/xt-smu-f/specifications#power-supply">FS-XT specifications</a> for information on voltage limits.
:::
