---
layout: docs
title: Mounting guidelines
group: Mechanical
description: This document highlights the method by which the FS-XT BMU board should be mounted in the customer application


---

The objective of this document is to provide the user/customer of the ION Energy FS-XT BMU board with an overview of the BMS installation guidelines including the environmental conditions it is most suitable to operate in.

## Handling

All ION Energy BMS boards are shipped in cartons containing either 5, 20 or 50 boards based on the size of the order. All the boards are shipped individually wrapped in antistatic bags which are further encapsulated in foam sheets. When handling the boards, please ensure the following measures are taken:

<img src="./assets/mounting-guidelines/antistatic-cover.png" width="60%"></img>

- Ensure that all personnel receive education and training on ESD preventive measures in general.
- The operator/handler of the board must wear a grounded antistatic wrist-strap in contact with their skin when handling the boards outside the antistatic bags.
- The boards are unboxed on an Electro-Static Discharge(ESD) mat to prevent any unwanted static current from damaging the board.

<img src="./assets/mounting-guidelines/esd-mat.png" width="60%"></img>

## Storage

- The BMS boards must be stored at temperature and humidity levels that do not exceed [specified operating conditions](#Environmental-conditions) that are shown below.
- If the BMS is going to be stored for longer periods of time, it is ION Energy's recommendation to store the boards at temperatures below $25°C$ and humidity levels below **70% RH**.

### Environmental conditions

| Parameters        | Units   | Standard operating conditions |
| ----------------- | ------- | ----------------------------- |
| Temperature       | $°C$    | $15-35$                       |
| Relative Humidity | $RH$(%) | $25-75$                       |
| Pressure          | $mBar$  | $860-1060$                    |

## BMS Dimensions

The ION Energy BMS is a highly configurable system that can manage multiple lithium ion chemistries and packages. The Mechanical Dimensions of the board are shown below:

| Parameter | Units | Value                       |
| --------- | ----- | --------------------------- |
| Dimension | $mm$  | 100 x 80 x 12.24(L x W x H) |
| Weight    | $g$   | <150                        |

![FS_XT_BMU_2D_Diagram](./assets/mounting-guidelines/bmu-2d.jpg)

## Mounting

<img src="./assets/mounting-guidelines/bmu-render.jpg"></img>

The board provides four Ø3.3mm holes symmetric mounting holes that accept **M3** screw **(metric)** or **4-40** **UNC** and **4-48** **UNF** screws **(imperial)**.

![Mounting Hole Locations](./assets/mounting-guidelines/mounting-holes.jpg)

The 4 screws should be fastened in a specific order as illustrated in the sequence column of the following table.

|         | Torque($Nm$)                               | Sequence |
| ------- | ------------------------------------------ | -------- |
| Initial | $0.3\times$[Recommended Torque](#torquing) | 1-2-3-4  |
| Final   | [Recommended Torque](#torquing)            | 4-3-2-1  |

### Screws

Ensure the board is mounted and torqued in a manner that is perpendicular to the board.

We recommend the following screw type for effective mounting of the BMS(*for example only*):

- [M3xL Pan Head Philips Drive SEMS Screw](https://in.rsdelivers.com/product/rs-pro/din-125a-bs-4183/rs-pro-m3-x-6mm-zinc-plated-steel-pan-head-sems/0278821) - SEMS screws have washers fixed on the screw shank thus preventing the possibility of washers falling into your application.

<div className="alert alert-primary" role="alert">
<span>:information_source:</span>
The length (L) and material of the screws is dependant on your application and can be chosen as per your requirement.
</div>

#### Torquing

We recommend a torquing of **$1 N/m^2$**.

The board is FR4 4 layer PCB. The FR4 material can withstand the recommended torque for M3 screws ($1 N/m^2$) without any failures. It is ION's recommendation that **[pneumatic](https://www.ingersollrandproducts.com/en-in/power-tools/products/air-and-electric-screwdrivers/1-series-inline.html#tab-2-594ad5ed-66ad-495c-96ca-0e907ebf737f)/[electric torque drivers](https://www.ingersollrandproducts.com/en-in/power-tools/products/air-and-electric-screwdrivers/brushless-electric.html)** are used so as to carefully control the torque applied to each screw. **Magnetic** drive bits are recommended to ensure that the screws do not fall into your application.

:::warning
Failure to follow these guidelines can result in stress cracking of the surface mounted components and/or the board itself
:::

### Clearances

#### Printed circuit board

The board has components populated on both, the top face and bottom facethe top face and bottom face of the board which means it is important to ensure that the board has enough clearance from the component it is being mounted to, be it a sheet metal plate or a plastic enclosure box.

:::info
A tolerance stack up analysis is recommended to ensure this is taken care of at the design level.
:::

As the BMS dimensions above show, the tallest component on the bottom side of the BMS board is 2.64 mm in height. However, depending on the EMS the board is manufactured at, there is a rare possibility of through hole component pins being extended beyond 2.64 mm which is why we recommend a minimum of 6 mm clearance from the mounting location.

The clearance can be achieved by using the following methods:

- For **Sheet Metal**
  - Use [Self Clinch Standoffs](https://in.rsdelivers.com/product/rs-pro/16475/rs-pro-steel-zinc-plated-self-clinching-standoff/0827451)

- For **Plastics**
  - Design bosses with [inserts](https://in.rsdelivers.com/product/rs-pro/14591/rs-pro-m3-brass-threaded-insert-diameter-4mm/0278534) that raise the board at least 6 mm from the next plastic surface.

An example of the same is shown below:
![BMU Clearances](./assets/mounting-guidelines/mounting.jpg)

As observed in the image above, sufficient clearance has been provided to ensure that no mating screws or parts can cause a short on the board.

#### Wire harness

[COMMENT]: # (This section will be linked to the wire harness document once merged.)

The FS-XT BMU board has six different connectors mounted on it. They allow the user to wire their system to the BMS by the means of a wire harness. The connectors on the BMS are shown below:

![BMU Connectors](./assets/mounting-guidelines/connectors.jpg)

- [Cell Voltage Sensing (18S) Connector](https://www.molex.com/molex/products/datasheet.jsp?part=active/5051510601_CRIMP_HOUSINGS.xml) - 6 Pin Connector
- [Cell Voltage Sensing Connector](https://www.molex.com/molex/products/datasheet.jsp?part=active/5051511401_CRIMP_HOUSINGS.xml&channel=Products&Lang=en-US) - 14 Pin Connector
- [Temperature Sensor NTC Connector](https://www.molex.com/molex/products/datasheet.jsp?part=active/5051510601_CRIMP_HOUSINGS.xml) - 6 Pin Connector
- [2nd Protection Connector](https://www.molex.com/molex/products/datasheet.jsp?part=active/5051510201_CRIMP_HOUSINGS.xml) - 2 Pin Connector
- [Auxiliary Connector](https://www.molex.com/molex/products/datasheet.jsp?part=active/5051511001_CRIMP_HOUSINGS.xml) - 10 Pin Connector
- [iso. SPI Connector](https://www.molex.com/molex/products/datasheet.jsp?part=active/5051510201_CRIMP_HOUSINGS.xml) - 2 Pin Connector
- [S PIN Connector](https://katalog.we-online.de/en/em/MM_MALE_IDC_CONNECTOR_69015700XX72) - 20 Pin FRC Connector

All the above connectors are mated with wire to board to connectors containing **[22 AWG](http://www.farnell.com/datasheets/1855486.pdf)** cables. It is important to leave sufficient clearance around the board to ensure the wiring can be bent easily without stressing the wire crimps or the connectors on the board.

:::info
The wire harness required for most applications using BMU requires standard multi-core wires which need 8 times the overall wire diameter as bend clearance.
:::

## Assembly Instructions

Below are the steps and warnings that need to be added to the BMS manual as a part of assembly instruction.

1. The BMS board is designed to be mounted with four fasteners at the [indicated locations](#mounting) in the figure.
2. Fasteners must be tightened in the [denoted fashion](#mounting) to enable alignment and to reduce stress on the components while assembling.
3. Consider the space between the nearest component and the type of [fastener](#screws) being used. Kindly follow the standard instructions to remain compliant.
4. Care must be taken to not to damage the conformal coating on the board surface.

:::info
The connector series used is of a positive locking type. Ensure that the connectors are positively locked and look for a click on correct connection. This is a quality checkpoint.
:::

:::info
Mounting of the boards in the horizontal orientation is preferred.
:::

## Assembly Verification

The installation must be verified before configuring and powering up the board. Follow the instructions below.

1. Verify that all the fasteners are assembled.
2. Verify that the fasteners are torqued correctly. This can be done by manually trying to torque the screw by an additional quarter turn at max.
3. Verify the gap between the tallest component and interaction point in the assembly.
4. Verify whether all the signal connectors are completely snap locked.
5. Make sure that the cable routing in close proximity to the components in the board is avoided
