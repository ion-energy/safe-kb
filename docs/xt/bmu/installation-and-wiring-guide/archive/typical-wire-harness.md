---
layout: docs

group: BMU
title: BMU Wire harness
description : This document highlights the instructions for wire harness integration with FS-XT BMU board in the customer application.
---

The Wire Harness connects the Battery Management System (BMS)FS-XT slave board or Battery Monitoring Unit (BMU) to various applications for batteries with less than 18S combination for each board.

## Items in the kit

The items in the kit are split into six as below:

Table 1: Items in the Kit.

|       Item Name       |  Qty  |                    Notes                     |
| :-------------------: | :---: | :------------------------------------------: |
|   Iso. SPI BMU Bus    |   2   |                                              |
| Auxiliary Connections |   1   |   Pin description change with application    |
|   Second Protection   |   1   |     Optional (dependant on application)      |
|    NTC Connections    |   1   |                                              |
|   Cell Connections    |  1-2  | 1 for 12S Cell Module, 2 for 18S Cell Module |
|   S-Pin Connection    |   1   |     Optional (dependant on application)      |

The number of harness sets needed is dependent on the application.

[COMMENT]: #  (Add images of the kit once available.)

## Pre-installation

Before installation, proper selection of the BMU board is need to be done. The selection of the BMU board is done on the basis of the cell modules that can be connected to it. The BMU board is available in two variants.
Namely:

- 12S BMU Board
- 18S BMU Board

The 12S BMU board can be connected to a maxixmum of 12S cell confiurgation. Similarly, the 18S board can be connected to a maximum of 18S cell configuration. For additional connections, auxiliary connections and external balancing boards can be used.

### Optional pre-installation steps

For the BMU board, there are various connections that are highly application dependent. These include:

- Auxiliary connections
  
  - The connections like I2C, CAN bus, 3.3V, 5V, Pack+, Pack-, GPIO, etc. are added which can be application specific connections.
  - Additional cell voltage sensing connections can be provided if needed. Connections to heaters, fans, etc. can also be provided to this harness.
This harness increases the versatility of the board.

- Second Protection

  - The second protection connection is provided if the customer demands it. It will act as a signal producer at over-voltage scenarios which can be used for various applications.

- S-Pin connection
  
    - In the case when an external balancing board is used, it will be connected to the BMU board using this harness.

## Installation

To understand the installation, the wiring diagram must be studied. The steps to assemble the harness are referenced from the wiring diagram below.

### Wiring diagram

The below wiring diagram represents the interconnection between the BMS board and the rest of the battery pack.

<img src="./assets/BMU_Harness/BMU-harness.png" width="80%"></img>

Wiring Diagram for FS-XT BMU Board

Table 2: Wiring Diagram Reference

|       Item Name       | Reference | Quantity |
| :-------------------: | :-------: | :------: |
|    iso-SPI BMU Bus    |  **H1**   |    2     |
| Auxiliary Connections |  **H2**   |    1     |
|   Second Protection   |  **H3**   |    1     |
|    NTC Connections    |  **H4**   |    1     |
|   Cell Connections    |  **H5**   |   1-2    |
|   S-Pin Connection    |  **H6**   |    1     |

:::info
Since the cell voltage sensing harness <strong>H5</strong> will power up the board, we advise that it be connected as the last step in your setup to prevent unwanted BMS errors.    
:::


### Assembly instructions

1. Connect the **H4** harness to the **J4** connector making the NTC connections from the cell module to the Board.
2. If applicable, connect the **H3** harness to **J3** connector making the safety harness for the protection of the board.
3. Connect the **H2** harness to the **J2** connector making the Auxiliary connections.
4. Connect both **J1-A** and **J1-B** connectors making the daisy connections for the BMU bus (**H1**).
5. Connect the **H5** harness to the **J5** from the cell module. The connector **J6** will be used in the case of an 18S board. (shown by a dotted connection in the figure.)
6. If applicable, connect the balancing FRC cable **H6** to the **J7** connector.

## Connection verification

Verify the following to make sure that the harness is assembled properly

1. ION Lens shows no error after connection
2. All the connectors are locked properly in position
3. There is no strain on the cable while bending
4. The shunt switches are in position and configured properly