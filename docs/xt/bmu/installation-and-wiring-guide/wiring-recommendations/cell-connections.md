---
layout: docs

group: smu
title: Cell Connections
description: Cell Connections for BMU-12S, BMU-15S BMU-18S. 
---

This is a guide for harness configurations for 12S, 15S and 18S FS-XT-BMUs.Respective voltage masks need to be configured along with the harness configuration. More information about voltage masks can be found [here](/docs/fusion/configuration/voltage-masks.md).

:::info
Wire harnesses of the pin numbers with the same description need to be connected/shorted together and then connected to the respective cell terminal.
:::

## BMU-12S

The BMU-12S can be configured to connect to a minimum of 6 cells and a maximum of 12 cells in series.

<img src="./assets/wiring-diagram-bmu-12s.png" width="100%"></img>

### 6S battery cell harness with 12S BMU

A 6S battery has 7 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number   |    Description    |
| ------------ | :---------------: |
| 1            |   No Connection   |
| 2, 3, 4, 5   |      Cell 6+      |
| 6            | Cell 5+ / Cell 6- |
| 7            | Cell 4+ / Cell 5- |
| 8, 9, 10, 11 | Cell 3+ / Cell 4- |
| 12           | Cell 2+ / Cell 3- |
| 13           | Cell 1+ / Cell 2- |
| 14           |      Cell 1-      |

### 7S battery cell harness with 12S BMU

A 7S battery has 8 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          |   No Connection   |
| 2, 3, 4, 5 |      Cell 7+      |
| 6          | Cell 6+ / Cell 7- |
| 7          | Cell 5+ / Cell 6- |
| 8, 9, 10   | Cell 4+ / Cell 5- |
| 11         | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 8S battery cell harness with 12S BMU

A 8S battery has 9 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          |   No Connection   |
| 2, 3, 4    |      Cell 8+      |
| 5          | Cell 7+ / Cell 8- |
| 6          | Cell 6+ / Cell 7- |
| 7          | Cell 5+ / Cell 6- |
| 8, 9, 10   | Cell 4+ / Cell 5- |
| 11         | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 9S battery cell harness with 12S BMU

A 9S battery has 10 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          |   No Connection   |
| 2, 3, 4    |      Cell 9+      |
| 5          | Cell 8+/ Cell 9-  |
| 6          | Cell 7+ / Cell 8- |
| 7          | Cell 6+ / Cell 7- |
| 8,9        | Cell 5+ / Cell 6- |
| 10         | Cell 4+ / Cell 5- |
| 11         | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 10S battery cell harness with 12S BMU

A 10S battery has 11 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |    Description     |
| ---------- | :----------------: |
| 1          |   No Connection    |
| 2, 3       |      Cell 10+      |
| 4          | Cell 9+ / Cell 10- |
| 5          |  Cell 8+/ Cell 9-  |
| 6          | Cell 7+ / Cell 8-  |
| 7          | Cell 6+ / Cell 7-  |
| 8, 9       | Cell 5+ / Cell 6-  |
| 10         | Cell 4+ / Cell 5-  |
| 11         | Cell 3+ / Cell 4-  |
| 12         | Cell 2+ / Cell 3-  |
| 13         | Cell 1+ / Cell 2-  |
| 14         |      Cell 1-       |

### 11S battery cell harness with 12S BMU

A 11S battery has 12 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3       |      Cell 11+       |
| 4          | Cell 10+ / Cell 11- |
| 5          | Cell 9+ / Cell 10-  |
| 6          |  Cell 8+/ Cell 9-   |
| 7          |  Cell 7+ / Cell 8-  |
| 8          |  Cell 6+ / Cell 7-  |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 12S battery cell harness with 12S BMU

A 12S battery has 13 cell monitoring cables that connect with the J2 connector of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |      Cell 12+       |
| 3          | Cell 11+ / Cell 12- |
| 4          | Cell 10+ / Cell 11- |
| 5          | Cell 9+ / Cell 10-  |
| 6          |  Cell 8+/ Cell 9-   |
| 7          |  Cell 7+ / Cell 8-  |
| 8          |  Cell 6+ / Cell 7-  |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

## BMU-15S

The BMU-15S can be configured to connect to a minimum of 7 cells and a maximum of 15 cells in series.

### 7S battery cell harness with 15S BMU

A 7S battery has 8 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

<img src="./assets/wiring-diagram-bmu-15s.png" width="100%"></img>

#### J2 - Cell Monitoring Interface

| Pin Number |  Description  |
| ---------- | :-----------: |
| 1          | No Connection |
| 2          | No Connection |
| 3, 4, 5, 6 |    Cell 7+    |

#### J4 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          | Cell 6+ / Cell 7- |
| 2          |   No Connection   |
| 3, 4, 5, 6 | Cell 5+ / Cell 6- |
| 7          | Cell 4+ / Cell 5- |
| 8          |   No Connection   |
| 9, 10, 11  | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 8S battery cell harness with 15S BMU

A 8S battery has 9 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |  Description  |
| ---------- | :-----------: |
| 1          | No Connection |
| 2          | No Connection |
| 3, 4, 5, 6 |    Cell 8+    |

#### J4 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          | Cell 7+ / Cell 8- |
| 2          |   No Connection   |
| 3, 4, 5    | Cell 6+ / Cell 7- |
| 6          | Cell 5+ / Cell 6- |
| 7          | Cell 4+ / Cell 5- |
| 8          |   No Connection   |
| 9, 10, 11  | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 9S battery cell harness with 15S BMU

A 9S battery has 10 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          |   No Connection   |
| 2          |   No Connection   |
| 3, 4, 5    |      Cell 9+      |
| 6          | Cell 8+ / Cell 9- |

#### J4 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          | Cell 7+ / Cell 8- |
| 2          |   No Connection   |
| 3, 4, 5    | Cell 6+ / Cell 7- |
| 6          | Cell 5+ / Cell 6- |
| 7          | Cell 4+ / Cell 5- |
| 8          |   No Connection   |
| 9, 10, 11  | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 10S battery cell harness with 15S BMU

A 10S battery has 11 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |    No Connection    |
| 3, 4, 5    |      Cell 10+       |
| 6          | Cell 9+  / Cell 10- |

#### J4 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          | Cell 8+ / Cell 9- |
| 2          |   No Connection   |
| 3, 4, 5    | Cell 7+ / Cell 8- |
| 6          | Cell 6+ / Cell 7- |
| 7          | Cell 5+ / Cell 6- |
| 8          |   No Connection   |
| 9, 10      | Cell 4+ / Cell 5- |
| 11         | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 11S battery cell harness with 15S BMU

A 11S battery has 12 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |    No Connection    |
| 3, 4, 5    |      Cell 11+       |
| 6          | Cell 10+ / Cell 11- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 9+  / Cell 10- |
| 2          |    No Connection    |
| 3, 4       |  Cell 8+ / Cell 9-  |
| 5          |  Cell 7+ / Cell 8-  |
| 6          |  Cell 6+ / Cell 7-  |
| 7          |  Cell 5+ / Cell 6-  |
| 8          |    No Connection    |
| 9, 10      |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 12S battery cell harness with 15S BMU

A 12S battery has 13 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |    No Connection    |
| 3, 4       |      Cell 12+       |
| 5          | Cell 11+ / Cell 12- |
| 6          | Cell 10+ / Cell 11- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 9+  / Cell 10- |
| 2          |    No Connection    |
| 3, 4       | Cell 8+  / Cell 9-  |
| 5          |  Cell 7+ / Cell 8-  |
| 6          |  Cell 6+ / Cell 5-  |
| 7          |  Cell 5+ / Cell 6-  |
| 8          |    No Connection    |
| 9, 10      |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 13S battery cell harness with 15S BMU

A 13S battery has 14 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |    No Connection    |
| 3,4        |      Cell 13+       |
| 5          | Cell 12+ / Cell 13- |
| 6          | Cell 11+ / Cell 12- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 10+ / Cell 11- |
| 2          |    No Connection    |
| 3, 4       | Cell 9+  / Cell 10- |
| 5          |  Cell 8+ / Cell 9-  |
| 6          |  Cell 7+ / Cell 8-  |
| 7          |  Cell 6+ / Cell 7-  |
| 8          |    No Connection    |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 14S battery cell harness with 15S BMU

A 14S battery has 15 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |    No Connection    |
| 3, 4       |      Cell 14+       |
| 5          | Cell 13+ / Cell 14- |
| 6          | Cell 12+ / Cell 13- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 11+ / Cell 12- |
| 2          |    No Connection    |
| 3          | Cell 10+ / Cell 11- |
| 4          | Cell 9+  / Cell 10- |
| 5          |  Cell 8+ / Cell 9-  |
| 6          |  Cell 7+ / Cell 8-  |
| 7          |  Cell 6+ / Cell 7-  |
| 8          |    No Connection    |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 15S battery cell harness with 15S BMU

A 15S battery has 16 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |    No Connection    |
| 3          |      Cell 15+       |
| 4          | Cell 14+ / Cell 15- |
| 5          | Cell 13+ / Cell 14- |
| 6          | Cell 12+ / Cell 13- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 11+ / Cell 12- |
| 2          |    No Connection    |
| 3          | Cell 10+ / Cell 11- |
| 4          | Cell 9+  / Cell 10- |
| 5          |  Cell 8+ / Cell 9-  |
| 6          |  Cell 7+ / Cell 8-  |
| 7          |  Cell 6+ / Cell 7-  |
| 8          |    No Connection    |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

## BMU-18S

The BMU-18S can be configured to connect to a minimum of 8 cells and a maximum of 18 cells in series.

<img src="./assets/wiring-diagram-bmu-18s.png" width="100%"></img>

### 8S battery cell harness with 18S BMU

A 8S battery has 9 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number    |  Description  |
| ------------- | :-----------: |
| 1             | No Connection |
| 2, 3, 4, 5, 6 |    Cell 8+    |

#### J4 - Cell Monitoring Interface

| Pin Number   |    Description    |
| ------------ | :---------------: |
| 1            | Cell 7+ / Cell 8- |
| 2, 3, 4, 5   | Cell 6+ / Cell 7- |
| 6            | Cell 5+ / Cell 6- |
| 7            | Cell 4+ / Cell 5- |
| 8, 9, 10, 11 | Cell 3+ / Cell 4- |
| 12           | Cell 2+ / Cell 3- |
| 13           | Cell 1+ / Cell 2- |
| 14           |      Cell 1-      |

### 9S battery cell harness with 18S BMU

A 9S battery has 10 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          |   No Connection   |
| 2, 3, 4, 5 |      Cell 9+      |
| 6          | Cell 8+ / Cell 9- |

#### J4 - Cell Monitoring Interface

| Pin Number   |    Description    |
| ------------ | :---------------: |
| 1            | Cell 7+ / Cell 8- |
| 2, 3, 4, 5   | Cell 6+ / Cell 7- |
| 6            | Cell 5+ / Cell 6- |
| 7            | Cell 4+ / Cell 5- |
| 8, 9, 10, 11 | Cell 3+ / Cell 4- |
| 12           | Cell 2+ / Cell 3- |
| 13           | Cell 1+ / Cell 2- |
| 14           |      Cell 1-      |

### 10S battery cell harness with 18S BMU

A 10S battery has 11 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3, 4, 5 |      Cell 10+       |
| 6          | Cell 9+  / Cell 10- |

#### J4 - Cell Monitoring Interface

| Pin Number |    Description    |
| ---------- | :---------------: |
| 1          | Cell 8+ / Cell 9- |
| 2, 3, 4, 5 | Cell 7+ / Cell 8- |
| 6          | Cell 6+ / Cell 7- |
| 7          | Cell 5+ / Cell 6- |
| 8, 9, 10   | Cell 4+ / Cell 5- |
| 11         | Cell 3+ / Cell 4- |
| 12         | Cell 2+ / Cell 3- |
| 13         | Cell 1+ / Cell 2- |
| 14         |      Cell 1-      |

### 11S battery cell harness with 18S BMU

A 11S battery has 12 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3, 4, 5 |      Cell 11+       |
| 6          | Cell 10+ / Cell 11- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 9+  / Cell 10- |
| 2, 3, 4    |  Cell 8+ / Cell 9-  |
| 5          |  Cell 7+ / Cell 8-  |
| 6          |  Cell 6+ / Cell 7-  |
| 7          |  Cell 5+ / Cell 6-  |
| 8, 9, 10   |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 12S battery cell harness with 18S BMU

A 12S battery has 13 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3, 4    |      Cell 12+       |
| 5          | Cell 11+ / Cell 12- |
| 6          | Cell 10+ / Cell 11- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 9+  / Cell 10- |
| 2, 3, 4    |  Cell 8+ / Cell 9-  |
| 5          |  Cell 7+ / Cell 8-  |
| 6          |  Cell 6+ / Cell 7-  |
| 7          |  Cell 5+ / Cell 6-  |
| 8, 9, 10   |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 13S battery cell harness with 18S BMU

A 13S battery has 14 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3, 4    |      Cell 13+       |
| 5          | Cell 12+ / Cell 13- |
| 6          | Cell 11+ / Cell 12- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 10+ / Cell 11- |
| 2, 3, 4    | Cell 9+  / Cell 10- |
| 5          |  Cell 8+ / Cell 9-  |
| 6          |  Cell 7+ / Cell 8-  |
| 7          |  Cell 6+ / Cell 7-  |
| 8 , 9      |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 14S battery cell harness with 18S BMU

A 14S battery has 15 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3, 4    |      Cell 14+       |
| 5          | Cell 13+ / Cell 14- |
| 6          | Cell 12+ / Cell 13- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 11+ / Cell 12- |
| 2, 3       | Cell 10+ / Cell 11- |
| 4          | Cell 9+  / Cell 10- |
| 5          |  Cell 8+ / Cell 9-  |
| 6          |  Cell 7+ / Cell 8-  |
| 7          |  Cell 6+ / Cell 7-  |
| 8, 9       |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 15S battery cell harness with 18S BMU

A 15S battery has 16 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3       |      Cell 15+       |
| 4          | Cell 14+ / Cell 15- |
| 5          | Cell 13+ / Cell 14- |
| 6          | Cell 12+ / Cell 13- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 11+ / Cell 12- |
| 2, 3       | Cell 10+ / Cell 11- |
| 4          | Cell 9+  / Cell 10- |
| 5          |  Cell 8+ / Cell 9-  |
| 6          |  Cell 7+ / Cell 8-  |
| 7          |  Cell 6+ / Cell 7-  |
| 8, 9       |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 16S battery cell harness with 18S BMU

:::info
16S configuration is currently not supported in BMS firmware.
:::

A 16S battery has 17 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3       |      Cell 16+       |
| 4          | Cell 15+ / Cell 16- |
| 5          | Cell 14+ / Cell 15- |
| 6          | Cell 13+ / Cell 14- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 12+ / Cell 13- |
| 2, 3       | Cell 11+ / Cell 12- |
| 4          | Cell 10+ / Cell 11- |
| 5          | Cell 9+  / Cell 10- |
| 6          |  Cell 8+ / Cell 9-  |
| 7          |  Cell 7+ / Cell 8-  |
| 8          |  Cell 6+ / Cell 7-  |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 17S battery cell harness with 18S BMU

:::info
17S configuration is currently not supported in BMS firmware.
:::

A 17S battery has 18 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2, 3       |      Cell 17+       |
| 4          | Cell 16+ / Cell 17- |
| 5          | Cell 15+ / Cell 16- |
| 6          | Cell 14+ / Cell 15- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 13+ / Cell 14- |
| 2          | Cell 12+ / Cell 13- |
| 3          | Cell 11+ / Cell 12- |
| 4          | Cell 10+ / Cell 11- |
| 5          | Cell 9+  / Cell 10- |
| 6          |  Cell 8+ / Cell 9-  |
| 7          |  Cell 7+ / Cell 8-  |
| 8          |  Cell 6+ / Cell 7-  |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |

### 18S battery cell harness with 18S BMU

:::info
18S configuration is currently not supported in BMS firmware.
:::

A 18S battery has 19 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          |    No Connection    |
| 2          |      Cell 18+       |
| 3          | Cell 17+ / Cell 18- |
| 4          | Cell 16+ / Cell 17- |
| 5          | Cell 15+ / Cell 16- |
| 6          | Cell 14+ / Cell 15- |

#### J4 - Cell Monitoring Interface

| Pin Number |     Description     |
| ---------- | :-----------------: |
| 1          | Cell 13+ / Cell 14- |
| 2          | Cell 12+ / Cell 13- |
| 3          | Cell 11+ / Cell 12- |
| 4          | Cell 10+ / Cell 11- |
| 5          | Cell 9+  / Cell 10- |
| 6          |  Cell 8+ / Cell 9-  |
| 7          |  Cell 7+ / Cell 8-  |
| 8          |  Cell 6+ / Cell 7-  |
| 9          |  Cell 5+ / Cell 6-  |
| 10         |  Cell 4+ / Cell 5-  |
| 11         |  Cell 3+ / Cell 4-  |
| 12         |  Cell 2+ / Cell 3-  |
| 13         |  Cell 1+ / Cell 2-  |
| 14         |       Cell 1-       |
