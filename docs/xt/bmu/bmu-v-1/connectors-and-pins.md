---
layout: docs

group: xt
title: Connectors and pins 
description: Description of the connectors and pins 
---
[comment]: # (Connector overview diagram to be added)

## Voltage Sensing Connector (J2)

### Pin diagram

<img src="./assets/bmu-pins-and-labels-j2.png"></img>

### Pin description table
