---
layout: docs

group: XT
title: Connectors and pins
description: Overview of all the connectors and pins of the FS-XT-BMU-OTS board
design: FS-XT-BMU-OTS
---


{% include connector-templates/connector-overview.html design = page.design imageName="bmu-connector-overview.png" %}

{% include connector-templates/connector-template.html design=page.design connector='J2' title='Voltage sensing - port B' %}

:::info
J2 Connector is populated only in the BMU 18S Board and not in the BMU 12S board.
:::

{% include connector-templates/connector-template.html design=page.design connector='J3' title='isoSPI Out/channel A' %}
{% include connector-templates/connector-template.html design=page.design connector='J4' title='Voltage sensing - port A' %}
{% include connector-templates/connector-template.html design=page.design connector='J5' title='isoSPI In/channel B' %}
{% include connector-templates/connector-template.html design=page.design connector='J6' title='Auxiliary GPIOs' %}
{% include connector-templates/connector-template.html design=page.design connector='J7' title='Temperature sensing' %}
