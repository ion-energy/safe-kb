---
layout: docs

group: bmu
title: BMU harness guide
description: Harness configurations for BMU-12S, BMU-15S, BMU-18S. 
---

This is a guide for harness configurations for 12S, 15S and 18S FS-XT-BMUs.Respective voltage masks need to be configured along with the harness configuration. More information about voltage masks can be found [here](/docs/fusion/configuration/reference/definition/battery/bms.html#cell-masks).

:::info
Wire harnesses of the pin numbers with the same description need to be connected/shorted together and then connected to the respective cell terminal.
:::

## BMU-12S

The BMU-12S can be configured to connect to a minimum of 6 cells and a maximum of 12 cells in series.

<img src="assets\connectors-and-pins\bmu-connector-overview.png" width="100%"></img>

### 6S battery cell harness with 12S BMU

A 6S battery has 7 cell monitoring cables that connect with the J2 connector of the BMU.

#### J4 - Cell Monitoring Interface

| Pin Number   |        Description        |
| ------------ | :-----------------------: |
| 1            |       No Connection       |
| 2, 3, 4, 5   | Voltage sensing channel 6 |
| 6            | Voltage sensing channel 5 |
| 7            | Voltage sensing channel 4 |
| 8, 9, 10, 11 | Voltage sensing channel 3 |
| 12           | Voltage sensing channel 2 |
| 13           | Voltage sensing channel 1 |
| 14           |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 7S battery cell harness with 12S BMU

A 7S battery has 8 cell monitoring cables that connect with the J2 connector of the BMU.

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2, 3, 4, 5 | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8, 9, 10   | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 8S battery cell harness with 12S BMU

A 8S battery has 9 cell monitoring cables that connect with the J2 connector of the BMU.

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2, 3, 4    | Voltage sensing channel 8 |
| 5          | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8, 9, 10   | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 9S battery cell harness with 12S BMU

A 9S battery has 10 cell monitoring cables that connect with the J2 connector of the BMU.
#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2, 3, 4    | Voltage sensing channel 9 |
| 5          | Voltage sensing channel 8 |
| 6          | Voltage sensing channel 7 |
| 7          | Voltage sensing channel 6 |
| 8,9        | Voltage sensing channel 5 |
| 10         | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 10S battery cell harness with 12S BMU

A 10S battery has 11 cell monitoring cables that connect with the J2 connector of the BMU.

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3       | Voltage sensing channel 10 |
| 4          | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8, 9       | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 11S battery cell harness with 12S BMU

A 11S battery has 12 cell monitoring cables that connect with the J2 connector of the BMU.

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3       | Voltage sensing channel 11 |
| 4          | Voltage sensing channel 10 |
| 5          | Voltage sensing channel 9  |
| 6          | Voltage sensing channel 8  |
| 7          | Voltage sensing channel 7  |
| 8          | Voltage sensing channel 6  |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 12S battery cell harness with 12S BMU

A 12S battery has 13 cell monitoring cables that connect with the J2 connector of the BMU.

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          | Voltage sensing channel 12 |
| 3          | Voltage sensing channel 11 |
| 4          | Voltage sensing channel 10 |
| 5          | Voltage sensing channel 9  |
| 6          | Voltage sensing channel 8  |
| 7          | Voltage sensing channel 7  |
| 8          | Voltage sensing channel 6  |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

## BMU-15S

The BMU-15S can be configured to connect to a minimum of 7 cells and a maximum of 15 cells in series.

<!-- <img src="./assets/FS-XT_BMU-15S.png" width="100%"> //-->

### 7S battery cell harness with 15S BMU

A 7S battery has 8 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2          |       No Connection       |
| 3, 4, 5, 6 | Voltage sensing channel 7 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 6 |
| 2          |       No Connection       |
| 3, 4, 5, 6 | Voltage sensing channel 5 |
| 7          | Voltage sensing channel 4 |
| 8          |       No Connection       |
| 9, 10, 11  | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 8S battery cell harness with 15S BMU

A 8S battery has 9 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2          |       No Connection       |
| 3, 4, 5, 6 | Voltage sensing channel 8 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 7 |
| 2          |       No Connection       |
| 3, 4, 5    | Voltage sensing channel 6 |
| 6          | Voltage sensing channel 5 |
| 7          | Voltage sensing channel 4 |
| 8          |       No Connection       |
| 9, 10, 11  | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 9S battery cell harness with 15S BMU

A 9S battery has 10 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2          |       No Connection       |
| 3, 4, 5    | Voltage sensing channel 9 |
| 6          | Voltage sensing channel 8 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 7 |
| 2          |       No Connection       |
| 3, 4, 5    | Voltage sensing channel 6 |
| 6          | Voltage sensing channel 5 |
| 7          | Voltage sensing channel 4 |
| 8          |       No Connection       |
| 9, 10, 11  | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 10S battery cell harness with 15S BMU

A 10S battery has 11 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          |       No Connection        |
| 3, 4, 5    | Voltage sensing channel 10 |
| 6          | Voltage sensing channel 9  |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 8 |
| 2          |       No Connection       |
| 3, 4, 5    | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8          |       No Connection       |
| 9, 10      | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 11S battery cell harness with 15S BMU

A 11S battery has 12 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          |       No Connection        |
| 3, 4, 5    | Voltage sensing channel 11 |
| 6          | Voltage sensing channel 10 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 9 |
| 2          |       No Connection       |
| 3, 4       | Voltage sensing channel 8 |
| 5          | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8          |       No Connection       |
| 9, 10      | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 12S battery cell harness with 15S BMU

A 12S battery has 13 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          |       No Connection        |
| 3, 4       | Voltage sensing channel 12 |
| 5          | Voltage sensing channel 11 |
| 6          | Voltage sensing channel 10 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 9 |
| 2          |       No Connection       |
| 3, 4       | Voltage sensing channel 8 |
| 5          | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8          |       No Connection       |
| 9, 10      | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 13S battery cell harness with 15S BMU

A 13S battery has 14 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          |       No Connection        |
| 3,4        | Voltage sensing channel 13 |
| 5          | Voltage sensing channel 12 |
| 6          | Voltage sensing channel 11 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 10 |
| 2          |       No Connection        |
| 3, 4       | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8          |       No Connection        |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 14S battery cell harness with 15S BMU

A 14S battery has 15 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          |       No Connection        |
| 3, 4       | Voltage sensing channel 14 |
| 5          | Voltage sensing channel 13 |
| 6          | Voltage sensing channel 12 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 11 |
| 2          |       No Connection        |
| 3          | Voltage sensing channel 10 |
| 4          | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8          |       No Connection        |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 15S battery cell harness with 15S BMU

A 15S battery has 16 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          |       No Connection        |
| 3          | Voltage sensing channel 15 |
| 4          | Voltage sensing channel 14 |
| 5          | Voltage sensing channel 13 |
| 6          | Voltage sensing channel 12 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 11 |
| 2          |       No Connection        |
| 3          | Voltage sensing channel 10 |
| 4          | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8          |       No Connection        |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

## BMU-18S

The BMU-18S can be configured to connect to a minimum of 8 cells and a maximum of 18 cells in series.

<!--  <img src="./assets/FS-XT_BMU-18S.png" width="100%"> //-->

### 8S battery cell harness with 18S BMU

A 8S battery has 9 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number    |        Description        |
| ------------- | :-----------------------: |
| 1             |       No Connection       |
| 2, 3, 4, 5, 6 | Voltage sensing channel 8 |

#### J4 - Cell Monitoring Interface

| Pin Number   |        Description        |
| ------------ | :-----------------------: |
| 1            | Voltage sensing channel 7 |
| 2, 3, 4, 5   | Voltage sensing channel 6 |
| 6            | Voltage sensing channel 5 |
| 7            | Voltage sensing channel 4 |
| 8, 9, 10, 11 | Voltage sensing channel 3 |
| 12           | Voltage sensing channel 2 |
| 13           | Voltage sensing channel 1 |
| 14           |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 9S battery cell harness with 18S BMU

A 9S battery has 10 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          |       No Connection       |
| 2, 3, 4, 5 | Voltage sensing channel 9 |
| 6          | Voltage sensing channel 8 |

#### J4 - Cell Monitoring Interface

| Pin Number   |        Description        |
| ------------ | :-----------------------: |
| 1            | Voltage sensing channel 7 |
| 2, 3, 4, 5   | Voltage sensing channel 6 |
| 6            | Voltage sensing channel 5 |
| 7            | Voltage sensing channel 4 |
| 8, 9, 10, 11 | Voltage sensing channel 3 |
| 12           | Voltage sensing channel 2 |
| 13           | Voltage sensing channel 1 |
| 14           |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 10S battery cell harness with 18S BMU

A 10S battery has 11 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3, 4, 5 | Voltage sensing channel 10 |
| 6          | Voltage sensing channel 9  |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 8 |
| 2, 3, 4, 5 | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8, 9, 10   | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 11S battery cell harness with 18S BMU

A 11S battery has 12 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3, 4, 5 | Voltage sensing channel 11 |
| 6          | Voltage sensing channel 10 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 9 |
| 2, 3, 4    | Voltage sensing channel 8 |
| 5          | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8, 9, 10   | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 12S battery cell harness with 18S BMU

A 12S battery has 13 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3, 4    | Voltage sensing channel 12 |
| 5          | Voltage sensing channel 11 |
| 6          | Voltage sensing channel 10 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description        |
| ---------- | :-----------------------: |
| 1          | Voltage sensing channel 9 |
| 2, 3, 4    | Voltage sensing channel 8 |
| 5          | Voltage sensing channel 7 |
| 6          | Voltage sensing channel 6 |
| 7          | Voltage sensing channel 5 |
| 8, 9, 10   | Voltage sensing channel 4 |
| 11         | Voltage sensing channel 3 |
| 12         | Voltage sensing channel 2 |
| 13         | Voltage sensing channel 1 |
| 14         |          Cell 1-          |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 13S battery cell harness with 18S BMU

A 13S battery has 14 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3, 4    | Voltage sensing channel 13 |
| 5          | Voltage sensing channel 12 |
| 6          | Voltage sensing channel 11 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 10 |
| 2, 3, 4    | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8 , 9      | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 14S battery cell harness with 18S BMU

A 14S battery has 15 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3, 4    | Voltage sensing channel 14 |
| 5          | Voltage sensing channel 13 |
| 6          | Voltage sensing channel 12 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 11 |
| 2, 3       | Voltage sensing channel 10 |
| 4          | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8, 9       | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 15S battery cell harness with 18S BMU

A 15S battery has 16 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3       | Voltage sensing channel 15 |
| 4          | Voltage sensing channel 14 |
| 5          | Voltage sensing channel 13 |
| 6          | Voltage sensing channel 12 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 11 |
| 2, 3       | Voltage sensing channel 10 |
| 4          | Voltage sensing channel 9  |
| 5          | Voltage sensing channel 8  |
| 6          | Voltage sensing channel 7  |
| 7          | Voltage sensing channel 6  |
| 8, 9       | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 16S battery cell harness with 18S BMU

:::info
16S configuration is currently not supported in BMS firmware.
:::

A 16S battery has 17 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3       | Voltage sensing channel 16 |
| 4          | Voltage sensing channel 15 |
| 5          | Voltage sensing channel 14 |
| 6          | Voltage sensing channel 13 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 12 |
| 2, 3       | Voltage sensing channel 11 |
| 4          | Voltage sensing channel 10 |
| 5          | Voltage sensing channel 9  |
| 6          | Voltage sensing channel 8  |
| 7          | Voltage sensing channel 7  |
| 8          | Voltage sensing channel 6  |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 17S battery cell harness with 18S BMU

:::info
17S configuration is currently not supported in BMS firmware.
:::

A 17S battery has 18 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2, 3       | Voltage sensing channel 17 |
| 4          | Voltage sensing channel 16 |
| 5          | Voltage sensing channel 15 |
| 6          | Voltage sensing channel 14 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 13 |
| 2          | Voltage sensing channel 12 |
| 3          | Voltage sensing channel 11 |
| 4          | Voltage sensing channel 10 |
| 5          | Voltage sensing channel 9  |
| 6          | Voltage sensing channel 8  |
| 7          | Voltage sensing channel 7  |
| 8          | Voltage sensing channel 6  |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)

### 18S battery cell harness with 18S BMU

:::info
18S configuration is currently not supported in BMS firmware.
:::

A 18S battery has 19 cell monitoring cables that connect with the J2 and J4 connectors of the BMU.

#### J2 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          |       No Connection        |
| 2          | Voltage sensing channel 18 |
| 3          | Voltage sensing channel 17 |
| 4          | Voltage sensing channel 16 |
| 5          | Voltage sensing channel 15 |
| 6          | Voltage sensing channel 14 |

#### J4 - Cell Monitoring Interface

| Pin Number |        Description         |
| ---------- | :------------------------: |
| 1          | Voltage sensing channel 13 |
| 2          | Voltage sensing channel 12 |
| 3          | Voltage sensing channel 11 |
| 4          | Voltage sensing channel 10 |
| 5          | Voltage sensing channel 9  |
| 6          | Voltage sensing channel 8  |
| 7          | Voltage sensing channel 7  |
| 8          | Voltage sensing channel 6  |
| 9          | Voltage sensing channel 5  |
| 10         | Voltage sensing channel 4  |
| 11         | Voltage sensing channel 3  |
| 12         | Voltage sensing channel 2  |
| 13         | Voltage sensing channel 1  |
| 14         |          Cell 1-           |

To return to the Wiring Guide, click [here](/docs/safe/xt/integration-guide/wiring-guide#wiring-guides)
