---
layout: docs

group: FS-LT
title: FS-LT wiring harness
description: Instructions to electrically interface FS-LT with a battery pack.
---

The Wire Harness connects the Battery Management System (BMS) FS-LT to various applications for batteries ranging from 6S to 25S depending on the variant selected.

## Items in the kit

The items in the kit are split into five items as detailed below:

Table 1: Items in the Kit

|     Harness Name     |  Qty  |
| :------------------: | :---: |
| Cell Voltage Sensing |   1   |
|     CAN bus Port     |   1   |
|   NTC Connections    |   1   |
|     Custom GPIO      |   1   |
|    PDU Connector     |   1   |


## Variants

For every application, proper selection of the FS-LT BMS variant has to be done. The selection of the variant is done on the basis of the cell modules that are connected in series. The FS-LT board is available in three variants. Namely:

Table 2: Variants

|  Variant  | # of cells in series |
| :-------: | :------------------: |
| FS-LT 10S |        6S-10S        |
| FS-LT 15S |        9S-15S        |
| FS-LT 25S |       15S-25S        |

## Installation

To understand the installation, the wiring diagram must be studied. The steps to assemble the harness are referenced from the wiring diagram.

### Wiring diagram

The below wiring diagram represents the interconnection between the BMS and the rest of the battery pack.

![FS-LT Wiring Diagram](./assets/wiring-harness/fs-lt-wiring-diagram-v2.png)

The above image represents a FS-LT 15S wiring diagram.

FS-LT can be used with both, ION's Solid State Relay Board(FSB-B) as well as an interface board(FSB-PR-I) which can interface with electromechanical contactors, relays and hall effect sensors.


Wiring Diagram for FS-LT BMS Board

<div className="alert alert-info" role="alert">
<span>:information_source:</span>
This image is for representative purposes only. Please consult ION before finalising the wiring harness for your application.
</div>

Table 3: Wiring Diagram Reference

|            Item Name             | Reference |  Qty  |
| :------------------------------: | :-------: | :---: |
|       Cell Voltage Sensing       |  **H1**   |   1   |
|          PDU Connector           |  **H2**   |   1   |
|           NTC Harness            |  **H3**   |   1   |
|         CAN bus Harness          |  **H4**   |   1   |
|      Custom GPIO & Ignition      |  **H5**   |   1   |
|    Voltage Sensing Connector     |  **J1**   |   1   |
|       BMS<->PDU Connector        |  **J2**   |   1   |
|       NTC Cable Connector        |  **J3**   |   1   |
|        CAN bus Connector         |  **J4**   |   1   |
| Custom GPIO & Ignition Connector |  **J5**   |   1   |

A complete list of the connectors and pin descriptions can be seen in the -[Connector Pin Description](connector-pin-description) document.

### Assembly instructions

<div  className="alert alert-info"  role="alert">
<span>:information_source:</span>  
Since the cell voltage sensing harness (H1) will power up the board, we advise that it be connected as the last step in your setup to prevent unwanted BMS errors.
</div>

1. Connect the NTC Harness **H3** to the FS-LT BMS Connector **J3** and the lug termination to be connected to the intended location as per the battery design.
2. Connect the GPIO & Ignition connector **H5** to the FS-LT BMS connector **J5** and the other free end of the cable to your application as required.
3. Connect the CAN bus connector **H4** to the FS-LT BMS connector **J4** and the other free end of the cable to the application connector.
4. Connect the PDU connector **H2** to the FS-LT BMS connector **J2** and the other end to the PDU board or the PR-I board that is being used with your application.
5. Connect the Cell Voltage Sensing Harness **H1** to the FS-LT BMS Connector **J1** and the lug termination to be connected to the intended location on the cell modules.

## Connection verification

Verify the following to make sure that the harness is assembled properly:

1. Freeview shows no errors after connection
2. All the connectors are locked properly in position
3. There is no strain on the cables while bending and routing
4. The shunt switches are in position and configured properly
