---
layout: docs

group: FS-LT
title: FS-LT connector pin description
description: Instructions to identify and understand the pin functionality.
---

The pin description explains the functionality of each pin on the FS-LT board.

## BMS Connectors general description

![FS-LT Connectors Top View](./assets/lt-connector-pin-description/fs-lt-connectors-top-view.png)

The above image represents a FS-LT 15S board.

FS-LT can be used with both, ION's Solid State Relay Board(FSB-B) as well as an interface board(FSB-PR-I) which can interface with electromechanical contactors, relays and hall effect sensors.

Table 1: FS-LT Connector Description

| Connector Number |         Connector         | Pins  |                           Description                            |
| :--------------: | :-----------------------: | :---: | :--------------------------------------------------------------: |
|        1         |       Isolated CAN        |   8   | Isolated CAN bus(1500V) for communication with external devices  |
|        2         | Voltage Sensing Connector |  26   |               Connection to battery cell terminals               |
|        3         |   Thermistor Connector    |  10   |       Connect to 10k NTC resistors for temperature sensing       |
|        4         |      GPIO Connector       |  16   |         Digital, analog, isolated and non-isolated I/O's         |
|        5         |       PDU Connector       |  10   | Connect to PDU. Drive the power switch and measure power current |
|        6a        |       Micro SD Card       |   -   |                                -                                 |
|        6b        |     SD Card Extension     |   7   |             Used to connect an external SD Connector             |
|     7a & 7b      |   Programming Connector   |   5   |      Connect to programming tool for loading a new firmware      |
|        8         |  External isolated reset  |   2   |                    Used to force a BMS reset                     |
|        9         |    Extension and debug    |  15   |                             Reserved                             |

All connectors needed for the BMS implantation in its system (1 to 5) are from the JST PUD family. See here for a
complete datasheet of these connectors: [JST Connectors](http://www.jst-mfg.com/product/pdf/eng/ePUD.pdf)

All the needed references are described in the table below:

| Onboard Connector |              |              | Recommended Connector |                            |
| ----------------- | ------------ | ------------ | --------------------- | -------------------------- |
| Connector Number  | Manufacturer | P/N          | Manufacturer          | P/N                        |
| 1                 | JST          | S08B-PUDSS-1 | JST                   | PUDP-08V-S                 |
|                   |              |              | JST                   | SPUD-001T-P0.5 with AWG 24 |
| 2                 | JST          | S26B-PUDSS-1 | JST                   | PUDP-26V-S                 |
|                   |              |              | JST                   | SPUD-001T-P0.5 with AWG 24 |
| 3                 | JST          | S10B-PUDSS-1 | JST                   | PUDP-10V-S                 |
|                   |              |              | JST                   | SPUD-001T-P0.5 with AWG 24 |
| 4                 | JST          | S16B-PUDSS-1 | JST                   | PUDP-16V-S                 |
|                   |              |              | JST                   | SPUD-001T-P0.5 with AWG 24 |
| 5                 | JST          | S10B-PUDSS-1 | JST                   | PUDP-10V-S                 |
|                   |              |              | JST                   | SPUD-001T-P0.5 with AWG 24 |

### Connector 1 - Isolated CAN

| Connector Reference on PCB | Complementary Reference | Crimp Reference | Recommended Wire Size |
| -------------------------- | ----------------------- | --------------- | --------------------- |
| S08B-PUDSS-1               | PUDP-08V-S              | SPUD-001T-P0.5  | 24 AWG                |

<img src="./assets/lt-connector-pin-description/isolated-can-connector.png" width="80%"></img>

| Pin No. | Type   | Description   |
| ------- | ------ | ------------- |
| 1       | Supply | GND CAN       |
| 2       | CAN    | CAN L         |
| 3       | CAN    | CAN H         |
| 4       | Supply | 5V CAN        |
| 5-8     | NC     | Not Connected |

### Connector 2 - Cell Voltage Sensing

| Connector Reference on PCB | Complementary Reference | Crimp Reference | Recommended Wire Size |
| -------------------------- | ----------------------- | --------------- | --------------------- |
| S26B-PUDSS-1               | PUDP-26V-S              | SPUD-001T-P0.5  | 24 AWG                |

![FS-LT Connectors Top View](./assets/lt-connector-pin-description/voltage-sensing-connector.png)

| Pin No. | Type         | Description                   |
| ------- | ------------ | ----------------------------- |
| 1       | Analog Input | Cell 25+<sup>1</sup>          |
| 2       | Analog Input | Cell 24+/Cell 25-<sup>1</sup> |
| 3       | Analog Input | Cell 23+/Cell 24-<sup>1</sup> |
| 4       | Analog Input | Cell 22+/Cell 23-<sup>1</sup> |
| 5       | Analog Input | Cell 21+/Cell 22-<sup>1</sup> |
| 6       | Analog Input | Cell 20+/Cell 21-<sup>1</sup> |
| 7       | Analog Input | Cell 19+/Cell 20-<sup>1</sup> |
| 8       | Analog Input | Cell 18+/Cell 19-<sup>1</sup> |
| 9       | Analog Input | Cell 17+/Cell 18-<sup>1</sup> |
| 10      | Analog Input | Cell 16+/Cell 17-<sup>1</sup> |
| 11      | Analog Input | Cell 15+/Cell 16-<sup>1</sup> |
| 12      | Analog Input | Cell 14+/Cell 15-<sup>2</sup> |
| 13      | Analog Input | Cell 13+/Cell 14-<sup>2</sup> |
| 14      | Analog Input | Cell 12+/Cell 13-<sup>2</sup> |
| 15      | Analog Input | Cell 11+/Cell 12-<sup>2</sup> |
| 16      | Analog Input | Cell 10+/Cell 11-<sup>2</sup> |
| 17      | Analog Input | Cell 9+/Cell 10-              |
| 18      | Analog Input | Cell 8+/Cell 9-               |
| 19      | Analog Input | Cell 7+/Cell 8-               |
| 20      | Analog Input | Cell 6+/Cell 7-               |
| 21      | Analog Input | Cell 5+/Cell 6-               |
| 22      | Analog Input | Cell 4+/Cell 5-               |
| 23      | Analog Input | Cell 3+/Cell 4-               |
| 24      | Analog Input | Cell 2+/Cell 3-               |
| 25      | Analog Input | Cell 1+/Cell 2-               |
| 26      | Analog Input | Cell 1-                       |

- Note<sup>1</sup> - Pins not used on the 15S and 10S version, only connected on the 25S version.

- Note<sup>2</sup> - Pins not used on the 10S version, only connected on the 15S and 25S version.

### Connector 3 - NTC Connector

| Connector Reference on PCB | Complementary Reference | Crimp Reference | Recommended Wire Size |
| -------------------------- | ----------------------- | --------------- | --------------------- |
| S10B-PUDSS-1               | PUDP-10V-S              | SPUD-001T-P0.5  | 24 AWG                |

<img src="./assets/lt-connector-pin-description/ntc-connector.png" width="80%"></img>

| Pin No. | Type  | Description        |
| ------- | ----- | ------------------ |
| 1       | Input | NTC 5+<sup>1</sup> |
| 2       | Input | NTC 5-<sup>1</sup> |
| 3       | Input | NTC 4+<sup>1</sup> |
| 4       | Input | NTC 4-<sup>1</sup> |
| 5       | Input | NTC 3+<sup>2</sup> |
| 6       | Input | NTC 3-<sup>2</sup> |
| 7       | Input | NTC 2+             |
| 8       | Input | NTC 2-             |
| 9       | Input | NTC 1+             |
| 10      | Input | NTC 1-             |

- Note<sup>1</sup> - Pins not used on the 15S and 10S version, only connected on the 25S version.

- Note<sup>2</sup> - Pins not used on the 10S version, only connected on the 15S and 25S version.

### Connector 4 - GPIO Connector

| Connector Reference on PCB | Complementary Reference | Crimp Reference | Recommended Wire Size |
| -------------------------- | ----------------------- | --------------- | --------------------- |
| S16B-PUDSS-1               | PUDP-16V-S              | SPUD-001T-P0.5  | 24 AWG                |

![FS-LT Connectors Top View](./assets/lt-connector-pin-description/gpio-connector.png)

| Pin No. | Type                     | Description      |
| ------- | ------------------------ | ---------------- |
| 1       | Supply                   | 3.3V BMS         |
| 2       | Supply                   | GND BMS          |
| 3       | Digital Output           | Open drain no. 0 |
| 4       | Digital Output           | Open drain no. 1 |
| 5       | Insulated Digital Output | OUT0+            |
| 6       | Insulated Digital Output | OUT0-            |
| 7       | Insulated Digital Input  | IN0+             |
| 8       | Insulated Digital Input  | IN0-             |
| 9       | Digital I/O              | GPIO 2           |
| 10      | Digital I/O              | GPIO 4           |
| 11      | Digital I/O              | GPIO 1           |
| 12      | Digital I/O              | GPIO 3           |
| 13      | Supply                   | GND BMS          |
| 14      | CAN                      | CAN L            |
| 15      | CAN                      | CAN H            |
| 16      | Supply                   | 5V BMS           |

### Connector 5 - PDU Connector

| Connector Reference on PCB | Complementary Reference | Crimp Reference | Recommended Wire Size |
| -------------------------- | ----------------------- | --------------- | --------------------- |
| S10B-PUDSS-1               | PUDP-10V-S              | SPUD-001T-P0.5  | 24 AWG                |

<img src="./assets/lt-connector-pin-description/ntc-connector.png" width="80%"></img>

| Pin No. | Type           | Description               |
| ------- | -------------- | ------------------------- |
| 1       | Digital Input  | Short Circuit Clear       |
| 2       | NC             | Not Connected             |
| 3       | Digital Input  | Precharge Circuit Command |
| 4       | Digital Output | Short Circuit Detection   |
| 5       | Digital Input  | Charge Circuit Command    |
| 6       | Analog Input   | Current Measurement       |
| 7       | Digital Input  | Discharge Circuit Command |
| 8       | Analog Input   | PDU Board Temperature     |
| 9       | Supply         | GND BMS                   |
| 10      | Supply         | 3.3V BMS                  |

### Connector 6b - SD Card Extension

Connector reference on PCB: B7B-ZR

Complementary connector reference: ZRH-7

Crimp contact reference: SZH-002T-P0.5

<img src="./assets/lt-connector-pin-description/sd-card-extension.png" width="80%"></img>

| Pin No. | Type          | Description |
| ------- | ------------- | ----------- |
| 1       | Supply        | GND BMS     |
| 2       | Supply        | 3.3V BMS    |
| 3       | SPI           | SDI         |
| 4       | SPI           | SCK         |
| 5       | SPI           | SDO         |
| 6       | SPI           | CS          |
| 7       | Digital Input | Card Detect |

### Connector 7a & 7b - Programming

<img src="./assets/lt-connector-pin-description/programming-connector.png" width="80%"></img>

7a directly printed on the PCB (reference: AVX 009159010061916)

7b is a standard 2.54mm pitch connector in parallel of 7a.

### Connector 8 - External Isolated Reset

2.54mm pitch connector. It is an insulated input used to reset the microcontroller. It must be driven with a voltage between 3.3V and 12V.

<img src="./assets/lt-connector-pin-description/bms-reset.png" width="80%"></img>

| Pin No. | Type                    | Description |
| ------- | ----------------------- | ----------- |
| 1       | Insulated Digital Input | ResetISO+   |
| 2       | Insulated Digital Input | ResetISO-   |
