---
layout: docs

group: lt
title: Wiring guide
description: Wiring guide for the FS-LT BMS with the FSB-PR-I.
---

## System integration diagram

[FS-LT-PR-I-integration-wd]:assets/fsb_pri_wiring_2.png
![FS-LT-PR-I-integration-wd][FS-LT-PR-I-integration-wd]
[<i className="fas fa-search-plus"></i>][FS-LT-PR-I-integration-wd]

:::info
This is an example integration diagram and is for references only.
:::

## Wiring guide

### FS-LT BMS

The details of all the connectors of the FS-LT BMS can be found [here](/docs/safe/lt/fs-lt/connectors-and-pins)

Once you are familiar with the connectors and pins of the FS-LT, refer to the section below for connection order.

#### Cell connections

The number of series-connected-cells that the FS-LT BMS can connect to is configurable. Configuring the FS-LT for a particular number of cells involves the following changes:

- Changes in the wiring of "Volt. Sensing" harness that interfaces the BMS with the battery module.
  A battery module with N cells in series will have N+1 cell sensing lines that interface with FS-LT via the "Volt. Sensing" harness. If N is less than the maximum number of cells that the FS-LT variant can connect to, then certain lines of the "Volt. Sensing" harness should not be connected.
  
- Changes in the DIP switches that are preset on the FS-LT.
  The position of the following DIP switches needs to be changed in accordance with the number of series-connected-cells that are interfaced with the FS-LT variant.

[FS-LT-integration-ds]:assets/FS-LT-integration-ds.png
![FS-LT-integration-ds][FS-LT-integration-ds]
[<i className="fas fa-search-plus"></i>][FS-LT-integration-ds]

- Changes in voltage mask of BMS Configuration.
  More information about voltage masks can be found [here](/docs/fusion/configuration/reference/definition/battery/bms#cell-masks).

A detailed description of changes in the wiring of "Volt. Sensing" harness and changes in the DIP switches is presented below.

##### FS-LT-10S

FS-LT-10S can be configured to connect to a minimum of 6 cells and a maximum of 10 cells in series.

- [6S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#6s-battery-cell-harness-with-fs-lt-10s)

- [7S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#7s-battery-cell-harness-with-fs-lt-10s)

- [8S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#8s-battery-cell-harness-with-fs-lt-10s)

- [9S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#9s-battery-cell-harness-with-fs-lt-10s)

- [10S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#10s-battery-cell-harness-with-fs-lt-10s)

##### FS-LT-15S

FS-LT-15S can be configured to connect to a minimum of 9 cells and a maximum of 15 cells in series.

- [9S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#9s-battery-cell-harness-with-fs-lt-15s)

- [10S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#10s-battery-cell-harness-with-fs-lt-15s)

- [11S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#11s-battery-cell-harness-with-fs-lt-15s)

- [12S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#12s-battery-cell-harness-with-fs-lt-15s)

- [13S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#13s-battery-cell-harness-with-fs-lt-15s)

- [14S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#14s-battery-cell-harness-with-fs-lt-15s)

- [15S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#15s-battery-cell-harness-with-fs-lt-15s)

##### FS-LT-25S

FS-LT-25S can be configured to connect to a minimum of 16 cells and a maximum of 25 cells in series.

- [15S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#15s-battery-cell-harness-with-fs-lt-25s)

- [16S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#16s-battery-cell-harness-with-fs-lt-25s)

- [17S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#17s-battery-cell-harness-with-fs-lt-25s)

- [18S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#18s-battery-cell-harness-with-fs-lt-25s)

- [19S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#19s-battery-cell-harness-with-fs-lt-25s)

- [20S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#20s-battery-cell-harness-with-fs-lt-25s)

- [21S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#21s-battery-cell-harness-with-fs-lt-25s)

- [22S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#22s-battery-cell-harness-with-fs-lt-25s)

- [23S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#23s-battery-cell-harness-with-fs-lt-25s)

- [24S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#24s-battery-cell-harness-with-fs-lt-25s)

- [25S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#25s-battery-cell-harness-with-fs-lt-25s)

:::warning
  The cell connector should not be connected to the FS-LT BMS at this point.
:::

#### NTC connections

The NTCs should be placed inside the battery pack to monitor cell temperature.

FS-LT boards have NTC thermistors on connector J4. The number of thermistors that the BMS variant supports is as explained in the table below.

| FS-LT variant | No of NTCs |
| ------------- | ---------- |
| FS-LT-10S     | 2          |
| FS-LT-15S     | 3          |
| FS-LT-25S     | 5          |

##### Recommendation of placement of NTCs

The optimal way to place the NTC’s on the battery pack is to perform thermal analysis, identify the hotspots, and place the NTC’s on the hotspots.

If the battery pack temperature is uniformly distributed, the best practice is to cover the maximum volume of the pack. On this basis, the following are the recommended placements.

1. For BMS consisting of 3 NTC’s, the recommended arrangement is triangular, with 2 NTC’s on one side and the other one has to be placed on the opposite side as shown in the figure.

<img src="./assets/3_NTC.jpg" width="450" height="180"></img>

1. For BMS consisting of 5 NTC’s, the recommended arrangement is  rhomboidal, with 3 NTC’s on one side and the other two have to be placed on the opposite side as shown in the figure.

<img src="./assets/5_NTC.jpg" width="550" height="200"></img>

After the placement, connect the NTCs to the BMU.

#### PDU Interface connection

Connect the PDU Interface connector to the BMS.

#### GPIO connection

Connect the GPIO connector to the BMS. This connector comprises of the Ignition button and the status LED. The Ignition button is used to give the command to the BMS to connect/disconnect the contactors. The state of the BMS can be determined by the blinking pattern of the Status LED.

:::warning
Contactors should always be connected on the high side (between BATT+ and LOAD+) to avoid GPIO faults.
:::

#### CAN bus

Connect the Isolated CAN connector to the BMS. The CAN lines form this connector connects to a DB9 connector at the other end. A 120 ohm termination resistor is already present inside the DB9 connector. The two open-ended wires from the DB9 connector are CAN lines. This is to facilitate connection to another device such as a motor controller etc.

:::info
The Ignition button, DB9 CAN connector, and the status LED should be accessible from outside the battery pack.
:::

### FSB-PR-I

The details of all the connectors of the FSB-PR-I can be found [here](/docs/safe/lt/fsb-pr-i/connectors-and-pins)

Once you are familiar with the connectors and pins of the FSB-PR-I, refer to the section below for connection order.

#### BMS to PR-I connection

Connect the BMS Interface connector to the connector 'J2' of FSB-PR-I. This will connect the PDU Interface and the GPIO connector (J6) of the BMS to the PR-I board.

#### Weld check and current sensor 1

Connect the current sensor 1 (low range current sensor) and the auxiliary wires of the contactor to the PR-I board on connector J4. You can skip the connection of the current sensor_1 if your system has only one current sensor.

:::info
Please make sure you have the contactors with auxiliary pins in order to use the Weld Check feature. If your system does not require the Weld check feature, you can skip the connection for the auxiliary wires of the contactors to the PR-I board.
:::

:::warning
If your system requires the use of weld check feature with two contactors (i.e. charge and discharge) connected in the system, then the contactors must be mandatorily selected with "NO" contacts. A detailed reason for this is described below.
:::

Both the weld check channels are logically ORed on the HW. The output of the OR gate is fed through a single pin on "Pin 6 of J2 connector" on the FSB-PR-I board. This pin further gets connected to "Pin 4 of J10 connector on the FSLT board. The Pin 4 of the J10 connector on the FSLT board needs to get an Active HIGH from the PDU for signalling the "Contact Weld" detection. Hence, if the contactors are selected with NO contacts && whenever there is a weld contact detection, the auxiliary contacts will transition from NO to NC whilst giving an Active HIGH output to the BMS. Thus, if the system is using the weld check feature, the contactors must always be selected with "NO" contacts.


#### Power distribution and current sensor 2

Connect the coils of the contactors and the current sensor_2 (high range) to the PR-I board on connector J1.

### Powering up the BMS

After you have completed all the above connections, connect the cell harness to the J9-connector FS-LT BMS after verifying the connections. For verification, measure the voltage on each pin of the cell connector. The voltage between any adjacent two pins should be equal to the corresponding cell voltage to which those pins are connected.

### Powering up the contactors

Connect the auxiliary power supply for the power distribution channels to the PR-I board on connector J3. The contactors coils will be powered from this source.

:::info
This power supply is for only for the coil supply of the contactors. The voltage and current specifications of the power supply being used should be decided based on the coil supply requirements of the contactors used.
:::
