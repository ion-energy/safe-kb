---
layout: docs

group: lt
title: Integration BOM
description: A guide on essential components needed before starting BMS integration.
---
 
## Description

This section describes the basic components required to integrate the BMS with the Battery. Please have these components in hand, before starting the integration. Additionally, other components could be needed as per your application.

:::info
Before starting the integration, ensure that all individual components are thoroughly tested and are in working condition.
:::

## List of components

| Component                            | Quantity           | Description                                                                                                                                   |
|--------------------------------------|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| FS-LT                                | 1                  | Provided by ION                                                                                                                               |
| FSB-PR-I                             | 1                  | Provided by ION                                                                                                                               |
| HK-FS-LT                             | 1                  | Provided by ION                                                                                                                               |
| HK-FSB-PR-I                          | 1                  | Provided by ION                                                                                                                               |
| Hall-effect Current sensor           | 1-2                | To be procured as per the applications’ specifications. The number of current sensors used depends on the current requirements of the system. Refer to the Hall effect current sensor's [selection guide](./assets/Hall%20Effect%20Current%20Sensor%20.pdf) |
| Connector/harness for Current Sensor | 1                  | To be procured as per current sensor                                                                                                          |
| Bi-directional Contactors            | 3                  | To be procured for pre-charge, discharge, and charge. Refer to the Contactor's [selection guide](./assets/High%20Voltage%20Contactors.pdf)                                         |
| Connector for contactor coils        | 3                  | To be procured as per contactor                                                                                                               |
| Precharge Resistor                   | 1                  | To be procured as per the application's specifications. Refer to the Precharge Resistor's [selection guide](./assets/Precharge%20Resistor.pdf)                                                           |
| Fuse/MCB                             | As per requirement | To be procured as per the application's specifications. Refer to the Fuse's [selection guide](./assets/Fuse.pdf)                                            |
| Power supply                         | 1                  | 9-70 V DC for powering  contactor's coils                                                                                                     |
| Android phone or Tab                 | 1                  | To be procured as per the application's specifications                                                                                        |
| PCAN-USB Adapter                     | 1                  | Provided by ION                                                                                                                               |
| Test Load                            | 1                  | To be procured as per the application's specifications                                                                                        |
| Charger                              | 1                  | To be procured as per the application's specifications                                                                                        |

:::warning
The selection guide document is only for a reference purpose, and the user can choose any vendor/manufacturer's component for their application. But the selected component must follow the selection criteria, as mentioned in the selection guide for each of the components in order to make it compatible with the ION BMS.
:::

:::info
The required power supply's voltage and wattage specifications depend on the specifications of the contactor's actuation/coil voltage and wattage. For example, if the contactor's actuating voltage is 24V and 8W then a 24V power supply of at least 8W is required.
:::
