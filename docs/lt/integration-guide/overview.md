---
layout: docs

group: lt
title: Overview 
description: Overview of the integration guide and its different sections.
---
 
## Introduction

This section is a guide to integrating the FS-LT BMS with the battery pack. It aims to provide an overview of the steps recommended for the hardware installation of the BMS. Furthermore, it provides information about the bill of materials required for integration, system-level integration diagram, wiring guides, connectors and pins, connection order recommendations, NTC placements, etc.
The section is divided into two main parts based on the PDU used in the system.

- FS-LT BMS with FSB-B PDU
- FS-LT BMS with the FSB-PR-I board

## Breakdown of the process

The integration process is divided into the following sections.

### Integration BOM

This section of the guide lists all the components necessary for the integration of the BMS. As these components are from multiple vendors, the BOM helps in procurement planning. This section also helps in familiarizing with the components required for integration.

- Click [here](/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/integration-bom) to navigate to FS-LT BMS with FSB-B PDU Integration BOM
- Click [here](/docs/safe/lt/integration-guide/fs-lt-with-fsb-b-pr-i/integration-bom) to navigate to FS-LT BMS with FSB-PR-I PDU Integration BOM

### Wiring guide

This section of the guide gives detailed guidance on the system level wiring. Refer to this section for wiring and powering the BMS. For making the integration easy, the ION’s sample harness kit comes with labels on each wire for easy reference.

- Click [here](/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/wiring-guide) to navigate to FS-LT BMS with FSB-B PDU wiring guide.
- Click [here](/docs/safe/lt/integration-guide/fs-lt-with-fsb-b-pr-i/wiring-guide) to navigate to FS-LT BMS with FSB-PR-I PDU wiring guide.

This section discusses the following topics.

#### FS-LT with FSB-B PDU

- System-level integration diagram for FS-LT BMS with FSB-B PDU.
- Wiring guide for FS-LT BMS with FSB-B PDU.

#### FS-LT with FSB-PR-I board

- System-level integration diagram for FSB-PR-I PDU
- Wiring guide for FS-LT BMS with FSB-PR-I PDU
