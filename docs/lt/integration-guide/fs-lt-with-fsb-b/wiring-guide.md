---
layout: docs

group: lt
title: Wiring guide
description: Wiring guide for the FS-LT BMS with the FSB-B PDU.
---

## System integration diagram

[FS-LT-integration-wd]:assets/FS-LT-integration-wd.png
![FS-LT-integration-wd][FS-LT-integration-wd]
[<i className="fas fa-search-plus"></i>][FS-LT-integration-wd]

:::info
This is an example integration diagram and is for references only.
:::

## Wiring guide

### FS-LT BMS

The details of all the connectors of the FS-LT BMS can be found [here](/docs/safe/lt/fs-lt/connectors-and-pins)

Once you are familiar with the connectors and pins of the FS-LT, refer to the section below for connection order.

#### Cell connections

The number of series-connected-cells that the FS-LT BMS can connect to is configurable. Configuring the FS-LT for a particular number of cells involves the following changes:

- Changes in the wiring of "Volt. Sensing" harness that interfaces the BMS with the battery module.
  A battery module with N cells in series will have N+1 cell sensing lines that interface with FS-LT via the "Volt. Sensing" harness. If N is less than the maximum number of cells that the FS-LT variant can connect to, then certain lines of the "Volt. Sensing" harness should not be connected.
  
- Changes in the DIP switches that are preset on the FS-LT.
  The position of the following DIP switches needs to be changed in accordance with the number of series-connected-cells that are interfaced with the FS-LT variant.

[FS-LT-integration-ds]:assets/FS-LT-integration-ds.png
![FS-LT-integration-ds][FS-LT-integration-ds]
[<i className="fas fa-search-plus"></i>][FS-LT-integration-ds]

- Changes in voltage mask of BMS Configuration.
  More information about voltage masks can be found [here](fusion/configuration/voltage-masks.md).

A detailed description of changes in the wiring of "Volt. Sensing" harness and changes in the DIP switches is presented below.

##### FS-LT-10S

FS-LT-10S can be configured to connect to a minimum of 6 cells and a maximum of 10 cells in series.

- [6S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#6s-battery-cell-harness-with-fs-lt-10s)

- [7S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#7s-battery-cell-harness-with-fs-lt-10s)

- [8S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#8s-battery-cell-harness-with-fs-lt-10s)

- [9S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#9s-battery-cell-harness-with-fs-lt-10s)

- [10S battery with FS-LT-10S](/docs/safe/lt/fs-lt/lt-harness-guide#10s-battery-cell-harness-with-fs-lt-10s)

##### FS-LT-15S

FS-LT-15S can be configured to connect to a minimum of 9 cells and a maximum of 15 cells in series.

- [9S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#9s-battery-cell-harness-with-fs-lt-15s)

- [10S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#10s-battery-cell-harness-with-fs-lt-15s)

- [11S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#11s-battery-cell-harness-with-fs-lt-15s)

- [12S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#12s-battery-cell-harness-with-fs-lt-15s)

- [13S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#13s-battery-cell-harness-with-fs-lt-15s)

- [14S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#14s-battery-cell-harness-with-fs-lt-15s)

- [15S battery with FS-LT-15S](/docs/safe/lt/fs-lt/lt-harness-guide#15s-battery-cell-harness-with-fs-lt-15s)

##### FS-LT-25S

FS-LT-25S can be configured to connect to a minimum of 16 cells and a maximum of 25 cells in series.

- [15S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#15s-battery-cell-harness-with-fs-lt-25s)

- [16S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#16s-battery-cell-harness-with-fs-lt-25s)

- [17S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#17s-battery-cell-harness-with-fs-lt-25s)

- [18S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#18s-battery-cell-harness-with-fs-lt-25s)

- [19S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#19s-battery-cell-harness-with-fs-lt-25s)

- [20S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#20s-battery-cell-harness-with-fs-lt-25s)

- [21S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#21s-battery-cell-harness-with-fs-lt-25s)

- [22S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#22s-battery-cell-harness-with-fs-lt-25s)

- [23S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#23s-battery-cell-harness-with-fs-lt-25s)

- [24S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#24s-battery-cell-harness-with-fs-lt-25s)

- [25S battery with FS-LT-25S](/docs/safe/lt/fs-lt/lt-harness-guide#25s-battery-cell-harness-with-fs-lt-25s)

:::warning
  The cell connector should not be connected to the FS-LT BMS at this point.
:::

#### NTC connections

The NTCs should be placed inside the battery pack to monitor cell temperature.

FS-LT boards have NTC thermistors on connector J4. The number of thermistors that the BMS variant supports is as explained in the table below.

| FS-LT variant | No of NTCs |
| ------------- | ---------- |
| FS-LT-10S     | 2          |
| FS-LT-15S     | 3          |
| FS-LT-25S     | 5          |

##### Recommendation of placement of NTCs

The optimal way to place the NTC’s on the battery pack is to perform thermal analysis, identify the hotspots, and place the NTC’s on the hotspots.

If the battery pack temperature is uniformly distributed, the best practice is to cover the maximum volume of the pack. On this basis, the following are the recommended placements.

1. For BMS consisting of 3 NTC’s, the recommended arrangement is triangular, with 2 NTC’s on one side and the other one has to be placed on the opposite side as shown in the figure.

<img src="./assets/3_NTC.jpg" width="450" height="180"></img>

1. For BMS consisting of 5 NTC’s, the recommended arrangement is  rhomboidal, with 3 NTC’s on one side and the other two have to be placed on the opposite side as shown in the figure.

<img src="./assets/5_NTC.jpg" width="550" height="200"></img>

After the placement, connect the NTCs to the BMU.

#### PDU Interface connection

Connect the PDU Interface connector to the BMS.

#### GPIO connection

Connect the GPIO connector to the BMS. This connector comprises of the Ignition button and the status LED. The Ignition button is used to give the command to the BMS to connect/disconnect the contactors. The state of the BMS can be determined by the blinking pattern of the Status LED.

:::warning
All devices connected to the non-isolated pins of GPIO shall never refer to LOAD- or any other potential than BATT-
:::

#### GPIO integration fault scenarios

##### Fault scenario 1

FSB-B (PDU) connected to load and the GPIO of the FS-LT connected to Interface device with a LOAD- reference

[fslt_pdu_gpio_1]:assets/fslt_gpio_groundfault_1.png
![fslt_pdu_gpio_1][fslt_pdu_gpio_1]
[<i className="fas fa-search-plus"></i>][fslt_pdu_gpio_1]

**Fault analysis:**

1. An interface device with ground reference of LOAD- and a supply derived from BATT+ (PSU of FS-LT), is connected to a GPIO of the FS-LT.
2. FSB-B (PDU) is in an open state, disconnecting LOAD- from BATT-.
3. Voltage potential of all pins on the device will be tied to BATT+ as no current flows after the opening of the PDU.
4. An overvoltage condition propagates to the power rails of the FS-LT.
5. This causes damage to the entire FS-LT board (including the PSU and the MCU).

**Solution:**

GND of interface device shall be connected to BATT-.

[fslt_pdu_sol1]:assets/fslt_gpio_sol1.png
![fslt_pdu_sol1][fslt_pdu_sol1]
[<i className="fas fa-search-plus"></i>][fslt_pdu_sol1]

##### Fault scenario 2

Interface device is interfaced with the load without isolation.

[fslt_pdu_gpio_2]:assets/fslt_gpio_groundfault_2.png
![fslt_pdu_gpio_2][fslt_pdu_gpio_2]
[<i className="fas fa-search-plus"></i>][fslt_pdu_gpio_2]

**Fault analysis:**

1. An interface device with BATT- reference is interfaced with the load device without any isolation.
2. FSB-B (PDU) is in an open state, disconnecting LOAD- from BATT-.
3. Voltage potential of all pins on the load will be tied to BATT+ as no current flows after the opening of the PDU.
4. An overvoltage condition propagates via the interface board to the power rails of the FS-LT.
5. Damage to Interface device and entire FS-LT board.

**Solution:**

Isolation between interface board and load is necessary.

[fslt_gpio_sol2]:assets/fslt_gpio_sol2.png
![fslt_gpio_sol2][fslt_gpio_sol2]
[<i className="fas fa-search-plus"></i>][fslt_gpio_sol2]

#### CAN bus

Connect the Isolated CAN connector to the BMS. The CAN lines form this connector connects to a DB9 connector at the other end. A 120 ohm termination resistor is already present inside the DB9 connector. The two open-ended wires from the DB9 connector are CAN lines. This is to facilitate connection to another device such as a motor controller etc.

:::info
The Ignition button, DB9 CAN connector, and the status LED should be accessible from outside the battery pack.
:::

:::warning
CAN bus connection between FS-LT and load shall always be isolated.
:::

#### CAN bus integration fault scenarios

##### Fault scenario

Non-isolated CAN connection between FS-LT and load.

[can_fault]:assets/can_fault.png
![can_fault][can_fault]
[<i className="fas fa-search-plus"></i>][can_fault]

**Fault analysis:**

1. Non-isolated CAN communication between FS-LT and load exists due to lack of isolated power supply.
2. FSB-B (PDU) is in an open state, disconnecting LOAD- from BATT-.
3. Voltage potential of CAN lines will be tied to BATT+ as no current flows after the opening of the PDU.
4. An overvoltage condition propagates via the CAN transceiver to the power rails of the FS-LT.
5. Damage to CAN transceiver and entire FS-LT board.

**Solution:**

Isolation between CAN with isolated power supply   is necessary.

[can_fault_sol]:assets/can_fault_sol.png
![can_fault_sol][can_fault_sol]
[<i className="fas fa-search-plus"></i>][can_fault_sol]

### FSB-B PDU

The details of all the connectors of the BMU can be found [here](/docs/safe/lt/fsb-b/connectors-and-pins).

Once you are familiar with the connectors and pins of the FSB-B, refer to the section below for connection order.

#### BMS Interface

Connect the BMS Interface connector to the PDU.

#### Battery Negative

Connect the Battery negative connector to the PDU.

#### PDU Power Supply and Snubber capacitor

The FSB-B harness comes with a snubber capacitor and it's precharge circuit. The capacitor needs to be precharged in the following sequence.

1. Keep the toggle switch pole in the middle position (called pre-charge mode).
2. Connect the PDU power supply to the battery positive terminal.
3. After 1 second, turn ON the switch by moving the pole of the switch to position 3.
4. Now the capacitor is pre-charged. Leave the switch in the same position.

<img src="./assets/snubber-cap1.png" width="550" height="200"></img>

#### Load Negative terminal

Connect the Load Negative terminal to the PDU.

### Powering up the BMS

After you have completed all the above connections, connect the cell harness to the FS-LT BMS after verifying the connections. For verification, measure the voltage on each pin of the cell connector. The voltage between any adjacent two pins should be equal to the corresponding cell voltage to which those pins are connected.

[Back to onboarding guide](/docs/safe/guides/getting-started)
