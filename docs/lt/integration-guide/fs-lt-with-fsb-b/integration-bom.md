---
layout: docs

group: lt
title: Integration BOM
description: A guide on essential components needed before starting BMS integration.
---
 
## Description

This section describes the basic components required to integrate the BMS with the Battery. Please have these components in hand, before starting the integration. Additionally, other components could be needed as per your application.

:::info
Before starting the integration, ensure that all individual components are thoroughly tested and are in working condition.
:::

## List of components

| Component            | Quantity           | Description                                            |
|----------------------|--------------------|--------------------------------------------------------|
| FS-LT                | 1                  | Provided by ION                                        |
| FSB-B                | 1                  | Provided by ION                                        |
| HK-FS-LT             | 1                  | Harness kit provided by ION                            |
| HK-FSB-B             | 1                  | Harness kit provided by ION                            |
| Fuse/MCB             | As per requirement | To be procured as per the application's specifications. Refer to the Fuse's [selection guide](./assets/Fuse.pdf)|
| Android phone or Tab | 1                  | To be procured as per the application's specifications |
| PCAN-USB Adapter     | 1                  | Provided by ION                                        |
| Test Load            | 1                  | To be procured as per the application's specifications |
| Charger              | 1                  | To be procured as per the application's specifications |

:::warning
The selection guide document is only for a reference purpose, and the user can choose any vendor/manufacturer's component for their application. But the selected component must follow the selection criteria, as mentioned in the selection guide.
:::
