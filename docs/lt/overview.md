---
layout: docs

group: lt
title: FS-LT Overview
description: An overview of FS-LT specifications & features.
---

![FS-LT board](./assets/product-overview/lt.png)

## Applications

- Light electric and hybrid vehicles, motorcycles, scooters, 3 wheelers
- Stationary Industrial and home storage
- Backup battery systems

## Features

- Stackable architecture based on slave boards,
- Support multiple battery chemistries and supercapacitors
- State of Charge (SOC) and State of Health (SOH) estimations based on advanced algorithms
- Watchdogs and self-diagnostics safety systems
- CAN bus 2.0 A&B interface for charger control and system interfacing
- Bluetooth 4.1 BLE monitoring capabilities with Android app ION Lens
- Compatible with CANView utility software.

## Cutting-edge, safe, compact

- Each board manages from 6 to 25 cells in series
- Scalable Up to 1080V battery pack (Up to 340 cells in series)
- Detection of 17 errors and 18 events on the battery and the BMS
- Real-time monitoring and logging of more than 61 variables
- Stores up to 20 years of data history on 8GB micro SD card
- More than 200 configurable parameters to tune the behavior of the BMS
- 200µA supply current in power saving mode
- Embedded passive balancing up to 210mA per cell
- 1 onboard temperature sensors and 3 thermistors (NTC) inputs for external sensing

## General specifications

| Parameters                     | Description                                                                   |
| ------------------------------ | ----------------------------------------------------------------------------- |
| Battery voltage                | 12 – 1080 VDC                                                                 |
| Cell configuration (per board) | 6-10 cells (FSLT-10S)                                                         |
|                                | 9-15 cells (FSLT-15S)                                                         |
|                                | 15-25 cells (FSLT-25S)                                                        |
| Number of cell per System      | 6 – 340 cells in series                                                       |
| Capacity                       | 200Ah max recommended                                                         |
| Balancing current per cell     | 210 mA @ 4.2V                                                                 |
| Input Voltage                  | 6 to 100 V                                                                    |
| Max cell voltage               | 5V                                                                            |
| Current consumption on battery | 15mA typ in normal mode                                                       |
|                                | 200µA typ in sleep mode                                                       |
| Temperature sensors            | 2 on the board + 3 external sensors (10kΩ NTC)                                |
| Control IO                     | PDU I/O : charge, load, precharge control, current and temperature monitoring |
|                                | 8 GPIO : HV interlock, ignition key, buzzer control                           |
| Communication                  | CAN 2.0 A&B for system integration                                            |
|                                | Bluetooth 4.1 for Android dashboard                                           |
| Temperature                    | -40°C to 85°C                                                                 |
| Dimensions                     | 110 x 62 x 15 mm                                                              |
| Weight                         | 40 g                                                                          |

## Typical 48V application

<img src="./assets/product-overview/fsb-b-diagram.svg" width="100%"></img>
