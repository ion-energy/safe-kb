---
layout: docs
title: Mounting Guidelines
group: LT
description: This document highlights the method by which the FSB-PR-I should be mounted in the customer application


---

The objective of this document is to provide the user/customer of the ION Energy FS-LT BMS with an overview of the BMS installation guidelines including the environmental conditions it is most suitable to operate in.

## Handling

All ION Energy BMS boards are shipped in cartons containing either 5, 20 or 50 boards based on the size of the order. All the boards are shipped individually wrapped in antistatic bags which are further encapsulated in foam sheets. When handling the boards, please ensure the following measures are taken:

<img src="./assets/mounting-guidelines/antistatic-cover.png" width="50%"></img>

- Ensure that all personnel receive education and training on ESD preventive measures in general.
- The operator/handler of the board must wear a grounded antistatic wrist-strap in contact with their skin when handling the boards outside the antistatic bags.
- The boards are unboxed on an Electro-Static Discharge(ESD) mat to prevent any unwanted static current from damaging the board.

<img src="./assets/mounting-guidelines/esd-mat.png" width="50%"></img>

## Storage

- The BMS boards must be stored at temperature and humidity levels that do not exceed specified operating conditions that are shown below.
- If the BMS is going to be stored for longer periods of time, it is ION Energy's recommendation to store the boards at temperatures below $25°C$ and humidity levels below **70% RH**.

### Environmental conditions

| Parameters        | Units   | Standard operating conditions |
| ----------------- | ------- | ----------------------------- |
| Temperature       | $°C$    | $15-35$                       |
| Relative Humidity | $RH$(%) | $25-75$                       |
| Pressure          | $mBar$  | $860-1060$                    |

## BMS Dimensions

The ION Energy BMS is a highly configurable system that can manage multiple lithium ion chemistries and packages. The Mechanical Dimensions of the board are shown below:

| Parameter | Units | Value                         |
| --------- | ----- | ----------------------------- |
| Dimension | *mm*  | *100 x 80 x 12.24(L x W x H)* |
| Weight    | *g*   | *<150*                        |

![FS_LT_2D_Diagram](./assets/mounting-guidelines/fs-lt-2d.png)

## Mounting

<img src="./assets/mounting-guidelines/torque-direction.png" width="50%"></img>

The board provides four Ø4mm holes symmetric mounting holes for mounting that accept **M3** and **M3.5** screws **(metric)** or **5-44** and **6-40 UNF** screws **(imperial)**.

![Mounting Hole Locations](./assets/mounting-guidelines/mounting-holes.png)

The 4 screws should be fastened in a specific order as illustrated in the sequence column of the following table.

|         | Torque($Nm$)                        | Sequence |
| ------- | ----------------------------------- | -------- |
| Initial | 0.3*[Recommended Torque](#torquing) | 1-2-3-4  |
| Final   | [Recommended Torque](#torquing)     | 4-3-2-1  |

### Screws

Ensure the board is mounted and torqued in a manner that is perpendicular to the board.

We recommend the following screw type for effective mounting of the BMS(*for example only*):

- [M3xL Pan Head Philips Drive SEMS Screw](https://in.rsdelivers.com/product/rs-pro/din-125a-bs-4183/rs-pro-m3-x-6mm-zinc-plated-steel-pan-head-sems/0278821) - SEMS screws have washers fixed on the screw shank thus preventing the possibility of washers falling into your application.

:::info
The length (L) and material of the screws is dependant on your application and can be chosen as per your requirement.
:::

#### Torquing

We recommend a torquing of **$1 N/m^2$**.

The board is FR4 4 layers PCB. The FR4 material can withstand the recommended torque for M3 screws ($1 N/m^2$) without any failures. It is ION's recommendation that **[pneumatic](https://www.ingersollrandproducts.com/en-in/power-tools/products/air-and-electric-screwdrivers/1-series-inline.html#tab-2-594ad5ed-66ad-495c-96ca-0e907ebf737f)/[electric torque drivers](https://www.ingersollrandproducts.com/en-in/power-tools/products/air-and-electric-screwdrivers/brushless-electric.html)** are used so as to carefully control the torque applied to each screw. **Magnetic** drive bits are recommended to ensure that the screws do not fall into your application.

:::info
Failure to follow these guidelines can result in stress cracking of the surface mounted components and/or the board itself
:::

### Clearances

#### Printed circuit board

The board has components populated on both sides of the board which means it is important to ensure that the board has enough clearance from the component it is being mounted to, be it a sheet metal plate or a plastic enclosure box.

:::info
A tolerance stack up analysis is recommended to ensure this is taken care of at the design level.
:::

As the BMS dimensions above show, the tallest component on the bottom side of the BMS board is 4.20 mm in height. However, depending on the EMS the board is manufactured at, there is a rare possibility of through hole component pins being extended beyond 4.20 mm which is why we recommend a minimum of 6 mm clearance from the mounting location.

The clearance can be achieved by using the following methods:

- For **Sheet Metal**
  - Use [Self Clinch Standoffs](https://catalog.pemnet.com/item/blind-threaded-standoffs-type-bso4/installation-into-stainless-steel-type-bso4-metric/bso4-m3-8)

- For **Plastics**
  - Design bosses with [inserts](https://catalog.pemnet.com/item/knurled-spacers-types-stka-stkb-stkc/d-in-threaded-inserts-thru-threaded-knurled-metric/22600) that raise the board at least 6 mm from the next plastic surface.

An example of the same is shown below:
![FS-LT Clearances](./assets/mounting-guidelines/fs-lt-clearances.png)

As observed in the image above, sufficient clearance has been provided in the case of board to board stacking to ensure that no mating screws or parts can cause a short on the board.

#### Wire harness

The FS-LT BMS has five different connectors mounted on it. They allow the user to wire their system to the BMS by the means of a wire harness. The connectors on the BMS are shown below:

![FS-LT Connectors](./assets/mounting-guidelines/connectors.png)

- [Cell Voltage Sensing Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/S26B-PUDSS-1-LF-SN/455-2494-ND/1989483) - 26 Pin Connector
- [Temperature Sensor NTC Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/S10B-PUDSS-1-LF-SN/455-2486-ND/1989475) - 10 Pin Connector
- [BMS to PDU/PR-I Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/S10B-PUDSS-1-LF-SN/455-2486-ND/1989475) - 10 Pin Connector
- [CAN bus Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/S08B-PUDSS-1-LF-SN/455-2485-ND/1989474) - 8 Pin Connector
- [GPIO Connector](https://www.digikey.in/product-detail/en/jst-sales-america-inc/S16B-PUDSS-1-LF-SN/455-2489-ND/1989478) - 16 Pin Connector

All the above connectors are mated with wire to board to connectors containing **[24 AWG](http://www.farnell.com/datasheets/1855483.pdf)** cables. It is important to leave sufficient clearance around the board to ensure the wiring can be bent easily without stressing the wire crimps or the connectors on the board.

:::info
The wire harness required for most applications using FS-LT requires standard multi-core wires which need 8 times the overall wire diameter as bend clearance.
:::

![Wire Harness Clearance](./assets/mounting-guidelines/bend-radius.png)

#### BLE module

A typical clearance of 20 mm & 25mm should be provided to enable proper operation of the BLE module.

[comment]: # (Add diagram here)

The BLE Module is located on the bottom side of the board and hence needs some clearance from metal surfaces to ensure a strong connection to the mobile application.

![BLE Module](./assets/mounting-guidelines/ble-module.png)

An example of the clearance given to the BLE module is shown below:

![BLE Clearance from Board](./assets/mounting-guidelines/ble-module-clearances.png)

:::warning
Ensuring clerance alone does not guarantee BLE operations.
BLE connectivity depends on the integration of the BMS in the battery & the battery in the application.:::



#### SD-Card connector

We recommend a clearance of **25 mm** as illustrated in the following diagram to facilitate maintenance of the BMS.
