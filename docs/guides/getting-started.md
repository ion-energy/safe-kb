---
layout: docs
title: Getting started
group: guides
description: How to integrate and use ION's BMS with the Battery Pack.


---
## Objective

The objective of this document is to provide an overview of the stages involved in the BMS activation process.

## Introduction to activation stages

The below flowchart gives a high-level understanding of all the stages involved in activating the BMS with a small description for each stage.

![BMS activation](./assets/BMS-activation-flow.png)

## Edison Orientation

### Prerequisites

Before getting started with this stage there are some prerequisites which must be fulfilled as listed below:

1. A valid and reviewed RFI sheet must be available.
2. The user must have an Edison account with necessary credentials (Username and Password)
   <div className="alert alert--warning" role="alert">
   <p className="first" className="alert-title"> Warning </p>
   <p>
   If you don't have an Edison account then get in touch with your point of contact at ION energy and make sure you have your account created before proceeding with this step.
   </p>
   </div>

3. The latest version of the ION Trace android application must be downloaded on your android mobile phone or tablet. Click [here](https://edison.ionenergy.co/core/doc/docs/edison/companion-apps/ion-trace) to read about ION Trace in detail.

### Steps to be followed in this stage

The below-mentioned steps must be followed as per the chronology in which they are listed. Relevant links are provided in each step for guidance.

1. Creating a battery model.
   - Refer to the [Creating battery model](https://edison.ionenergy.co/core/doc/docs/edison/guides/getting-started/creating-battery-model) page and make a battery model.

2. Creating a configuration file.
   - Refer to the [Creating configuration](https://edison.ionenergy.co/core/doc/docs/edison/guides/getting-started/creating-configuration) page and make a configuration file using the Edison configuration wizard.

3. Assembling the battery.
    - Generate QR codes.
      - Refer to the [Generate QR codes](https://edison.ionenergy.co/core/doc/docs/edison/guides/getting-started/generating-qr-codes) page and generate the QR codes for the Battery as demonstrated.
    - Minimal production pipeline.
      - Refer to the [Minimal production pipeline](https://edison.ionenergy.co/core/doc/docs/edison/guides/getting-started/minimal-production-pipeline) page to associate the BMS serial number (QR code) and PDU serial number (QR code) with the Battery serial number (QR code) that you generated in the above step.

### Checklist for Edison Orientation

This stage can be concluded only if all the below-mentioned points are **"Completed"**.

- The battery model must be present on Edison.
- The BMS is configured with the right parameters in the battery model.
- The QR codes for the battery model are generated.
- The generated battery QR code must be printed and pasted on the battery pack.
- The battery QR code must be associated with the BMS QR code and the PDU QR code using the ION trace application.
- The parent (battery) and the associated child (BMS and PDU) components must be present on the ION trace application, once the battery QR code is scanned.

## BMS Integration

### Prerequisites

Before getting started with this stage there are some prerequisites which must be fulfilled as listed below:

1. The checklist section of the [Edison Orientation](#checklist-for-edison-orientation) must be completed.
2. All the components along with the mating connectors must be procured as mentioned in the BOM. The BOM for different BMS variants are mentioned below:
   - Click [here](https://edison.ionenergy.co/core/doc/docs/safe/xt/integration-guide/integration-bom) for FS-XT integration BOM.
   - Click [here](https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/fs-lt-with-fsb-pr-i/integration-bom) for FS-LT with FSB-PR-I integration BOM.
   - Click [here](https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/integration-bom) for FS-LT with FSB-B integration BOM.

   <div className="alert alert--warning" role="alert">
   <p className="first" className="alert-title"> Warning </p>
   <p>
   The components must be individually tested and verified before the integration.
   </p>
   </div>

### Steps to be followed in this stage

The below-mentioned steps must be followed. Relevant links are provided in each step for guidance.

1. For FS-XT BMS:
   - Refer to [This guide](https://edison.ionenergy.co/core/doc/docs/safe/xt/integration-guide/overview) and wire up the BMS with the battery.

2. For FS-LT BMS:
   - Refer to [This guide](https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/overview) and wire up the BMS with the battery.

### Checklist for BMS integration

This stage can be concluded only if all the below-mentioned points are **"Completed"**.

- The cell voltage harness is wired as per the wiring guide.
- Cell voltages on the pins of the **_Voltage sensing connector_** are checked and verified.
- All the relevant components are connected as per the wiring guide.

:::warning
All the relevant connectors of the BMS must be connected except the Voltage sensing connector.
:::

## Operating the BMS

### Prerequisites

Before getting started with this stage there are some prerequisites which must be fulfilled as listed below:

1. The checklist section of the [BMS integration](#checklist-for-bms-integration) must be completed.
2. The latest version of the ION Lens android application must be downloaded on your android mobile phone or tablet. Click [here](https://play.google.com/store/apps/details?id=centreprise.freeview&hl=en&pageId=none) to download.
3. The latest version of the CANView utility tool must be downloaded and installed on your computer. Click [here](https://edison.ionenergy.co/core/doc/docs/fusion/operations/operating-the-battery/using-canview#prerequisites) to see the different hardware and software requirements for the CANView tool.

### Steps to be followed in this stage

The below-mentioned steps must be followed as per the chronology in which they are listed. Relevant links are provided in each step for guidance.

1. Connect the Voltage sensing connector of the BMS at this moment.
2. Interface the BMS with the ION Lens android application.
   - Refer to the [Using ION Lens](https://edison.ionenergy.co/core/doc/docs/fusion/operations/operating-the-battery/using-ionlens) page to interface the BMS with the ION Lens application and complete this step.

3. Interface the BMS with the CANView tool.
   - Refer to the [Using CANView](https://edison.ionenergy.co/core/doc/docs/fusion/operations/operating-the-battery/using-canview#prerequisites) page to interface the BMS with the CANView tool and complete this step.

4. Give the ignition signal to the BMS.

### Checklist for operating the BMS

This stage can be concluded only if all the below-mentioned points are **"Completed"**.

- The Voltage sensing connector is connected to the BMS.
- The BMS must be interfaced with the ION Lens application and the BMS data must be broadcasted on the ION Lens application without any errors.
- The BMS must be interfaced with the CANView Tool and the BMS data must be broadcasted on the CANView tool without any errors.

## BMS Testing and Configuration Validation

### Prerequisites

Before getting started with this stage there are some prerequisites which must be fulfilled as listed below:

1. All the components of the BMS must be integrated with the battery pack that is to be tested.
2. Appropriate load and charger must be connected as per the parameters filled in the configuration file.
   <div className="alert alert--warning" role="alert">
   <p className="first" className="alert-title"> Warning </p>
   <p>
   Do not use a load and charger having more or less rated values compared to the one mentioned in the configuration file as this may damage the battery pack and result in improper functioning of BMS.
   </p>
   </div>

### Steps to be followed in this stage

The below-mentioned steps must be followed as per the chronology in which they are listed. Relevant links are provided in each step for guidance.

1. A set of tests must be performed on the BMS to validate its functionality.
   - Refer to the [Tests for the BMS activation](./assets/Tests-for-BMS-activation.pdf) PDF file to get detailed information about the tests to be performed on the BMS.
      <div className="alert alert--warning" role="alert">
      <p className="first" className="alert-title"> Warning </p>
      <p>
      Unless the ignition signal is given, by default an "Undervoltage error" will be triggered that can be seen on the ION Lens application. Don't stop at this point and instead provide ignition to the BMS and the error will get cleared automatically.
      </p>
      </div>

2. If you face any error while testing the BMS then troubleshoot the error before moving forward.
   - Refer to the [Errors and Warnings](https://edison.ionenergy.co/core/doc/docs/fusion/safety/errors-and-warnings) page to see the different errors thrown by the BMS.

3. Finalize the configuration file.

   <div className='alert alert--info' role='alert'>
   <p className='first' className='alert-title'> Info
   </p>
   <p>
   When the BMS throws an error then try to fine-tune the relevant configuration parameters associated with that error to fix it. Finalize the configuration file only after making the necessary changes to the configuration parameters.
   </p>
   </div>

### Checklist for BMS testing and configuration validation

This stage can be concluded only if all the below-mentioned points are **"Completed"**.

- BMS is in error-free operation.
- The configuration file is finalized.

:::info
If you face any issues after integrating the BMS and the battery pack with the end application (Example: Electric vehicle, Energy storage system), then you can submit your issue to ION energy's <a href="https://ionenergyhelp.freshdesk.com/support/tickets/new">Customer support forum</a> or email to  <a href="mailto:support@ionenergy.co">support@ionenergy.co</a>
:::
