---
layout: docs
title: Master KB
group: guides
description: Guide to various pages on ION knowledge base

---


## Introduction

The ION Energy knowledge base is a centralized repository that stores information related to the ION Energy battery management system. The knowledge base helps to learn  about the ION Energy core product range. The knowledge base addresses all aspects of the BMS lifecycle which involves setting the virtual battery model, handling the inventory and operations, operating the battery and system integration guidelines, and also troubleshooting. It also gives an exclusive glance at the advanced visualization tools to visualize battery system data.

The information on the knowledge base can be split mainly into 3 sections:

- Edison (Software Platform)
- Fusion (Firmware Platform)
- Safe (Hardware Platform)

By clicking on hyperlinks under each section, the page will redirect to various pages on the knowledge base. One can get easy access and on-demand information by clicking the links.

## Knowledge base architectural layout

This chart drafts the various repositories of the knowledge base and the sub-topics associated with each repository. The chart will help navigate and identify the target sub-topic under each repository.

![master kb](./assets/master-kb.png)

## Edison

Edison  repository  helps explain and navigates each section on the Edison Analytics platform. Relevant links to pages on managing admins, creating battery models, adding stock to the virtual inventory, visualizing battery data are mentioned in this section.

### Guides

This sub section contains various guides for setting up and operate different sections of the Edison Analytics platform.
#### Edison orientation

#### Designer

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/designer/specification">Specifications</a>: Details on defining the specifications of the battery model being developed.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/designer/system-architecture">System Architecture</a>: Steps on associating the components of the battery model. The components mainly include the BMS and PDU model, apart from this other battery components models can also be attached.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/designer/sensors">Sensors</a>: A guide on sensors to be assigned to the target battery model.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/designer/ion-bms-configuration">ION BMS Configuration</a>: A guide on  <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/designer/example/creating-battery-model">Creating battery model</a> and <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/designer/example/creating-configuration">Creating configuration</a> wizard on Edison.

#### Asset Manager

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/asset-manager/track-your-assets">Track your assets</a>: Description of keeping a track of the battery model and its components on Edison.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/asset-manager/generating-qr-codes">Generating QR Codes</a>: A guide on generating QR codes for the target battery model developed and configured for testing/production.

#### Dashboards

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/dashboards/create-dashboards">Create Dashboards</a>: A guide to set up and use dashboards on Edison.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/dashboards/mathematical-expression">Mathematical Expressions</a>: 

#### Alerts

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/alerts/alert-rules">Alert Rules</a>: A guide to set up and use alerts.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/alerts/warranty-conditions">Warranty Conditions</a>: A guide to setup and use warranty conditions.

#### System Monitoring

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/system-monitoring/connect-your-systems">Connect your systems</a>: 
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/system-monitoring/visualization-tool">Visualization tools</a>: A guide to handle the Edison Visualization tool.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/system-monitoring/ion-sentinel">ION Sentinal</a>:

#### User Management

#### Security

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/security">Security</a>:
  
#### Uploader role tutorial

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/guides/uploader-role-tutorial">Uploader role tutorial</a>:  A guide to uploading data files from the BMS to the Edison Analytics platform.

### Companion Apps

This sub section give a glance of the various android applications developed by ION Energy.

- <a href="https://edison.ionenergy.co/core/doc/docs/edison/companion-apps/ion-trace">ION Trace</a>: User guide and description on the ION Trace application.
- <a href="https://edison.ionenergy.co/core/doc/docs/edison/companion-apps/ion-lens">ION Lens</a>: User guide and description on the ION Lens application.

## Fusion

The Fusion is a stack that deals with the functioning of ION BMS firmware. The Fusion is structured in different subsections to understand the BMS firmware and guides to configure its tools.

### Operations

The operations subsection involves setting up and operate ION visualization tools, describes the CAN protocol on which the BMS communicates, explains the BMS feature development and implementation.

#### Communication

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/communication/can-connection">CAN Connection</a>: The page defines the CAN protocol on which the BMS connects to the CAN based peripherals. 
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/communication/can-datalogging">CAN Datalogging</a>: The page describes the CAN Bus frame and protocol on which the BMS data logging process takes place.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/communication/simple-can-bus">Simple CAN bus</a>: The page explains the CAN message IDs and the details of each message broadcasted over CAN. A CAN DBC file attached to page give details on each CAN messaage ID in detail.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/communication/bulk-can-bus">Bulk CAN bus</a>: This page explains about CANBus frames list and implementation details about CAN-TP and the state machines.

#### Runtime

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/runtime/balancing-in-ltc-based-slaves">Balancing in LTC based slaves</a>: Description on cell balancing in LTC chips based BMS is provided.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/runtime/current-sensing-and-processing">Current sensing and processing</a>: The page explains the current sensing and processing by the BMS. 
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/runtime/sensing">Sensing</a>: The page gives a lens into how the BMS sense and measures cell voltages and temperatures.

#### Setup

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/setup/configuration-over-can">Configuration over CAN</a>: CANBus frames list and implementation details for read/write BMS configuration.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/setup/pdu-control-modes">PDU control modes</a>: The page describes the different PDU modes for power distribution to load. 
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/setup/special-power-control-features">Special power control features</a>: The page explains about the KL15 & Power Sustain features.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/setup/post-pdu-paralleling">Post PDU paralleling</a>: POST PDU paralleling algorithm of ION BMS platform.

#### Operating the battery

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/operating-the-battery/using-ionlens">Using ION Lens</a>: A guide to operate and navigate the ION Lens mobile application.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/operating-the-battery/using-canview">Using CANView</a>: A guide to operate and navigate the CANView utility tool.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/operating-the-battery/basic-troubleshooting">Basic troubleshooting</a>: A guide to understand and troubleshoot the BMS for error-free operation.

#### Security

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/security/security-in-fusion">Security in Fusion</a>: Details on security features in the ION BMS system.

#### Reference

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/operations/reference/logging-variables">Logging variables</a>: Description of xids logged and stored in the SD Card.

### Safety

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/safety/errors-and-warnings">Errors and warnings</a>: Definition and explanation of each error state and its mitigation strategy occurring on the BMS.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/safety/safety-fuse-trigger-and-limp-mode">Safety fuse trigger and Limp mode</a>: The page explains about the limp mode operation. 

### Release

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/release/fw-upgrade">FW upgrade</a>: A guide to flash the BMS firmware using the ION BMS Programmer through  CANbus.

### Reference

#### PDU

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/pdu/power-lines">Power lines</a>: 
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/pdu/precharge">Precharge</a>: Description of ION BMS precharge process.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/pdu/pre-pdu-serialization">Pre PDU serialization</a>: Pre PDU serialization algorithm of ION BMS plaform.

#### Current

This section discusses about the <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/current/current-sensing">Current sensing</a> and <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/current/current-processing">Current processing</a> done by the BMS.

#### Bootloader

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/bootloader/bootloader-pc-app-implementation">Bootloader PC App implementation</a>: Implementation of bootloader for flashing the firmware on the BMS.

#### State Estimators

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/state-estimators/soh">State of Health</a>: Guidelines and developer design notes on the state of health algorithm implementation.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/state-estimators/soc">State of charge</a>: Guidelines and developer design notes on the state of charge algorithm implementation.
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/state-estimators/state-of-power">State of Power</a>: Guidelines and developer design notes on the state of power algorithm implementation.

#### Low- level drivers

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/low-level-drivers/adc/adc-fs-lt">ADC FS-LT</a>: Implementation of ADC driver for input from the current sensor in FS-LT BMS.

#### Master-Slave

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/master-slave/bmu-allocation">BMU Allocation</a>: Firmware implementation of BMU arrangement on communication channels
- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/master-slave/stack-arrangement">Stack arrangement</a>: Arrangement of stacks for master-slave FS-XT platform and the corresponding guidelines on configuring the stack arrangement in the configuration wizard.

#### Configuration

- <a href="https://edison.ionenergy.co/core/doc/docs/fusion/reference/configuration/adding-new-configuration-parameters">Adding new configuration parameters</a>: Details on adding new configuration parameters to the configuration wizard.
  
## Safe

### Guides

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/guides/getting-started">Getting started</a>: This getting started page gives a step by step process of activating the BMS and links relevant configuration pages to have direct information on the topics encountered during activation.

### LT

#### FS-LT

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fs-lt/product-overview">Product overview</a>: Introduction, system block diagram ,and functional overview of ION FS-LT BMS.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fs-lt/connectors-and-pins">Connectors and pins</a>: Connector designation and pin description available on the FS-LT board. Also, the manufacturing part numbers of the connectors and mating connectors are listed here.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fs-lt/specifications">Specifications</a>: Hardware specifications of FS-LT.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fs-lt/mounting-guidelines">Mounting guidelines</a>: Mounting and handling guidelines for storage and integration.

#### FSB-B

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-b/product-overview">Product overview</a>: Introduction, system block diagram ,and functional overview of ION FSB-B PDU.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-b/connectors-and-pins">Connectors and pins</a>: Connector designation and pin description available on the FSB-B board. Also, the manufacturing part numbers of the connectors and mating connectors are listed here.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-b/specifications">Specifications</a>: Hardware specifications of FSB-B.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-b/mounting-guidelines">Mounting guidelines</a>: Mounting and handling guidelines for storage and integration.
  
#### FSB-PR-I

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-pr-i/product-overview">Product overview</a>: Introduction, system block diagram ,and functional overview of ION FSB-PR-I PDU.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-pr-i/connectors-and-pins">Connectors and pins</a>: Connector designation and pin description available on the FSB-PR-I board. Also, the manufacturing part numbers of the connectors and mating connectors are listed here.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-pr-i/specifications">Specifications</a>: Hardware specifications of FSB-PR-I.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/fsb-pr-i/mounting-guidelines">Mounting guidelines</a>: Mounting and handling guidelines for storage and integration.

#### Integration guide

#### FS-LT with FSB-B

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/integration-bom">Integration BOM</a>: The page defines each component and its selection guide that will be required for integration of the battery pack with ION FS-LT with FSB-B.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/wiring-guide">Wiring guide</a>: A step by step guide for activating the BMS.

#### FS-LT with FSB-PR-I

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/integration-bom">Integration BOM</a>: The page defines each component and its selection guide that will be required for integration of the battery pack with ION FS-LT with FSB-PR-I. 
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/lt/integration-guide/fs-lt-with-fsb-b/wiring-guide">Wiring guide</a>: A step by step guide for activating the BMS.

### XT

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/overview">Overview</a>: This page explains about the master-slave architecture of the BMS.

#### SMU

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/smu/xt-smu-f/product-overview">Product overview</a>: Introduction, system block diagram ,and functional overview of ION FS-XT-SMU.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/smu/xt-smu-f/connectors-and-pins">Connectors and pins</a>: Connector designation and pin description available on the FS-XT-SMU board. Also, the manufacturing part numbers of the connectors and mating connectors are listed here.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/smu/xt-smu-f/specifications">Specifications</a>:  Hardware specifications of FS-XT-SMU
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/smu/xt-smu-f/mounting-guidelines">Mounting guidelines</a>: Mounting and handling guidelines for storage and integration.

#### BMU

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/bmu/xt-bmu-ots/product-overview">Product overview</a>: Introduction, system block diagram and functional overview of ION FS-XT-SMU.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/bmu/xt-bmu-ots/connectors-and-pins">Connectors and pins</a>: Connector designation and pin description available on the FS-XT-SMU board. Also, the manufacturing part numbers of the connectors and mating connectors are listed here.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/bmu/xt-bmu-ots/specifications">Specifications</a>: Hardware specifications of FS-XT-SMU.
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/bmu/xt-bmu-ots/bmu-harness-guide">BMU harness guide</a>: Cell wiring harness connection guide to the FS-XT-BMU 12S/18S. 
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/bmu/xt-bmu-ots/mounting-guidelines">Mounting guidelines</a>:  Mounting and handling guidelines for storage and integration.

#### Integration guide

- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/integration-guide/integration-bom">Integration BOM</a>:  The page defines each component ,and its selection guide that will be required for integration of the battery pack with FS-XT-SMU and FS-XT-BMU. 
- <a href="https://edison.ionenergy.co/core/doc/docs/safe/xt/integration-guide/wiring-guide">Wiring guide</a>:  A step by step guide for activating the BMS.