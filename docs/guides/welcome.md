---
layout: docs
title: Architecture Overview
group: Guides
description: Architecture and typical application of the Safe BMS family


---

![Safe](assets/welcome/safe-architecture.png)

## Battery monitoring Unit (BMU)

![BMU](assets/welcome/bmu.png)

## System Management Unit (SMU)

![SMU](assets/welcome/smu.png)