---
layout: docs
title: Onboarding Guide
group: guides
description: How to implement & use ION BMS for the first time


---

![timeline](./assets/onboarding/timeline.png)

Our technology blends software, embedded system, and hardware into a comprehensive solution and gives you the ability and clarity for your electric application deployment.

## Edison Account creation

Edison is our online platform for battery management. Getting started with Edison is the first step to become familiar with ION products.
Pre-deployment Edison will enable you to model your battery and attach specific configurations.

<img src="./assets/onboarding/edison_banner.jpg" width="50%"></img>

- Create a user account
- Add a company to your profile
- [Create your first battery model](/docs/edison/guides/getting-started/creating-battery-model)

Go to the [getting started with Edison][edisonSetup] tutorial to explore additional options.

## BMS configuration

Every battery has a specific set of constraints and requirements that must be specified within the BMS configuration. This information is derived from Application requirements down to the chemistry details of the cell.

To accelerate the onboarding process, we've come up with a minimal set of parameters that must be tuned coupled with a configuration wizard to get started.

- Gather information about your cell, battery, and application.
- [Add a custom BMS configuration](/docs/edison/guides/getting-started/creating-configuration)
- Configure all the mandatory parameters following the [basic configuration tutorial](/docs/fusion/configuration/basics).

## BMS Software interfaces

Now that everything *software* has been taken care of, it's time to build a bridge between Edison and the physical world.

First, assign a serial number to your battery. If you don't have a serial number ready, you can [generate a QR code](/docs/edison/guides/getting-started/generating-qr-codes) with Edison.

- Put the serial number or the QR code on the battery
- [Register the battery][registerTrace] using ION Lens.

The BMS will already be registered from the test & validation process phase if we shipped it to you.

In complex battery assembly, many components can be tracked and assembled. For this onboarding, we will only assemble the battery and the BMS.

- [Assemble the battery and the BMS][assembleTrace] using ION Lens.

Now the physical & the digital world are connected together, you can integrate the BMS in the battery pack.

## BMS Hardware interfaces

For this step, the instructions will be specific for each BMS family.

- Interface Mechanically the BMS with the battery
   - [CT][ctMounting]  \| [LT][ltMounting] \| [XT][xtMounting]
- Connect the BMS electrically battery
   - [CT][ctWiring] \| [LT][ltWiring] \| [XT][xtWiring]

At the end of this section, your BMS should be installed and powered-up. Some activity is visible, but the battery is not yet operational because the BMS needs to be initialized.

## BMS Initialization

The initialization is required to provide an accurate DateTime to the firmware, as well as the latest configuration specified on Edison. This process ensures that all data collected after the first connection is relevant.

- Load the configuration with [ION Lens][uploadConf] \| ION Desk
- Authenticate the BMS with [ION Lens][activateBMS] \| ION Desk

After the authentification is done, all error flags should disappear. The battery is ready to be used.

## Operating the battery

- Operations overview
- Error-modes
- Real-time monitoring

## Analytics & management

- Accessing Real-time data
- Reviewing logged data

## Basic Maintenance

- [FW upgrade][fwUpgrade]

[edisonSetup]:/docs/edison/guides/getting-started
[registerTrace]: /docs/edison/companion-apps/ion-trace#add-new-component-in-your-company-inventory
[assembleTrace]: /docs/edison/companion-apps/ion-trace#assemble-a-component

[xtWiring]:/docs/safe/xt/smu/mechanical/typical-wire-harness
[xtMounting]:/docs/safe/xt/bmu/mechanical/mounting-guidelines

[ltMounting]:/docs/safe/lt/mechanical/mounting-guidelines
[ltWiring]:/docs/safe/lt/mechanical/typical-wire-harness

[ctWiring]:/docs/safe/ct/mechanical/typical-wire-harness
[ctMounting]:/docs/safe/ct/mechanical/mounting-guidelines

[uploadConf]:/docs/edison/companion-apps/ion-lens#update-bms-configuration

[activateBMS]:/docs/edison/companion-apps/ion-lens#activate-bms
[fwUpgrade]:/docs/safe/guides/fw-upgrade
